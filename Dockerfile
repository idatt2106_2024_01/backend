# Use an OpenJDK runtime as the base image
FROM openjdk:21-jdk-slim

# Set a directory for the app
WORKDIR /usr/src/app

# Copy the JAR file into the container
COPY ./target/sparesti-backend-0.0.1-SNAPSHOT.jar /usr/src/app/app.jar

# Copy the static images into the container
COPY ./src/main/resources/static/images /usr/src/app/src/main/resources/static/images

# Command to run the application
CMD ["java", "-Dspring.profiles.active=prod", "-jar", "app.jar"]

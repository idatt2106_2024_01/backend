package no.ntnu.sparestibackend.model;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

/**
 * Test class for the BankAccount model.
 */
public class BankAccountTest {

    private BankAccount bankAccount;

    @BeforeEach
    public void setup() {
        bankAccount = new BankAccount();
    }

    @Test
    public void testSetAndGetTransactionId() {
        bankAccount.setAccountNumber(123L);
        assertEquals(123L, bankAccount.getAccountNumber(),
                "Transaction ID should be set and retrieved correctly.");
    }

    @Test
    public void testSetAndGetName() {
        String testName = "Savings Account";
        bankAccount.setName(testName);
        assertEquals(testName, bankAccount.getName(), "Name should be set and retrieved correctly.");
    }

    @Test
    public void testSetAndGetAccountType() {
        AccountType testType = AccountType.SAVINGS;
        bankAccount.setAccountType(testType);
        assertEquals(testType, bankAccount.getAccountType(), "Account type should be set and retrieved correctly.");
    }

    @Test
    public void testToString() {
        bankAccount.setAccountNumber(123L);
        bankAccount.setName("Checking Account");
        bankAccount.setAccountType(AccountType.CHECKING);
        String expectedString = "BankAccount(accountNumber=123, name=Checking Account, accountType=CHECKING)";
        assertEquals(expectedString, bankAccount.toString(), "ToString should return the correct representation.");
    }

    @Test
    public void testNonNullName() {
        Exception exception = assertThrows(NullPointerException.class, () -> {
            bankAccount.setName(null);
        });
        assertTrue(exception.getMessage().contains("name is marked non-null but is null"),
                "Setting null name should throw NullPointerException.");
    }

    @Test
    public void testNoArgsConstructor() {
        BankAccount bankAccount = new BankAccount();
        assertNotNull(bankAccount, "BankAccount should be instantiated.");
    }

    @Test
    public void testAllArgsConstructor() {
        BankAccount bankAccount = new BankAccount();
        bankAccount.setAccountNumber(123L);
        bankAccount.setName("Savings Account");
        bankAccount.setAccountType(AccountType.SAVINGS);
        bankAccount.setUser(new User());
        assertEquals(123L, bankAccount.getAccountNumber(), "Transaction ID should be set correctly.");
        assertEquals("Savings Account", bankAccount.getName(), "Name should be set correctly.");
        assertEquals(AccountType.SAVINGS, bankAccount.getAccountType(), "Account type should be set correctly.");
    }

    @Test
    public void testBuilder() {
        BankAccount bankAccount = BankAccount.builder()
                .accountNumber(123L)
                .name("My Bank Account")
                .accountType(AccountType.SAVINGS)
                .build();
        assertEquals(123L, bankAccount.getAccountNumber(),
                "Builder should set the transaction ID correctly.");
        assertEquals("My Bank Account", bankAccount.getName(),
                "Builder should set the name correctly.");
        assertEquals(AccountType.SAVINGS, bankAccount.getAccountType(),
                "Builder should set the account type correctly.");
    }
}

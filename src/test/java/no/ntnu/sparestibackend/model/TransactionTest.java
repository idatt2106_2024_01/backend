package no.ntnu.sparestibackend.model;

import jakarta.persistence.EntityManager;
import jakarta.persistence.EntityTransaction;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Date;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

/**
 * Test class for Transaction.
 * Ensures proper handling and functionality of Transaction entities.
 */
public class TransactionTest {

    private EntityManager entityManager;
    private EntityTransaction transaction;
    private Transaction testTransaction;
    private Date transactionDate;
    private Category category;
    private BankAccount mockedBankAccount;

    @BeforeEach
    public void setUp() {
        entityManager = mock(EntityManager.class);
        transaction = mock(EntityTransaction.class);
        mockedBankAccount = mock(BankAccount.class);
        when(entityManager.getTransaction()).thenReturn(transaction);

        transactionDate = new Date();
        category = new Category();
        category.setName("Groceries");
        testTransaction = Transaction.builder()
                .bankAccount(mockedBankAccount)
                .category(category)
                .date(transactionDate)
                .description("Weekly grocery shopping")
                .sum(150.00)
                .build();
        testTransaction.setTransactionId(1L);
    }

    @Test
    public void testTransactionPersistence() {
        assertNotNull(testTransaction);
        assertEquals(1L, testTransaction.getTransactionId());
        assertEquals(150.00, testTransaction.getSum());
        assertEquals("Weekly grocery shopping", testTransaction.getDescription());
        assertEquals(transactionDate, testTransaction.getDate());
        assertEquals(category, testTransaction.getCategory());

        when(entityManager.find(Transaction.class, 1L)).thenReturn(testTransaction);

        entityManager.persist(testTransaction);
        verify(entityManager).persist(any(Transaction.class));

        Transaction foundTransaction = entityManager.find(Transaction.class, 1L);
        assertNotNull(foundTransaction);
        assertEquals("Weekly grocery shopping", foundTransaction.getDescription());
    }

    @Test
    public void testSettingNegativeSum() {
        double negativeSum = -100.00;
        testTransaction.setSum(negativeSum);
        assertEquals(negativeSum, testTransaction.getSum(), "The sum should be set to the negative value");
    }


    @Test
    public void testTransactionUpdate() {
        testTransaction.setDescription("Changed description");
        assertEquals("Changed description", testTransaction.getDescription());

        entityManager.merge(testTransaction);
        verify(entityManager).merge(testTransaction);

        when(entityManager.find(Transaction.class, testTransaction.getTransactionId())).thenReturn(testTransaction);
        Transaction updatedTransaction = entityManager.find(Transaction.class, testTransaction.getTransactionId());
        assertNotNull(updatedTransaction);
        assertEquals("Changed description", updatedTransaction.getDescription());
    }

    @Test
    public void testTransactionDeletion() {
        entityManager.remove(testTransaction);
        verify(entityManager).remove(testTransaction);
    }

    @Test
    public void testNoArgsConstructor() {
        Transaction emptyTransaction = new Transaction();
        assertNotNull(emptyTransaction);
    }

    @Test
    public void testAllArgsConstructor() {
        Transaction fullTransaction = new Transaction(1L, mockedBankAccount, category, transactionDate,
                "Full constructor", 200.00, null);
        assertNotNull(fullTransaction);
        assertEquals(1L, fullTransaction.getTransactionId());
        assertEquals(category, fullTransaction.getCategory());
        assertEquals(transactionDate, fullTransaction.getDate());
        assertEquals("Full constructor", fullTransaction.getDescription());
        assertEquals(200.00, fullTransaction.getSum());
    }

    @Test
    public void testBuilderPattern() {
        Transaction builtTransaction = Transaction.builder()
                .transactionId(2L)
                .category(category)
                .date(transactionDate)
                .description("Built transaction")
                .sum(250.00)
                .build();
        assertNotNull(builtTransaction);
        assertEquals(2L, builtTransaction.getTransactionId());
        assertEquals(category, builtTransaction.getCategory());
        assertEquals(transactionDate, builtTransaction.getDate());
        assertEquals("Built transaction", builtTransaction.getDescription());
        assertEquals(250.00, builtTransaction.getSum());
    }

    @Test
    public void testSettingInvalidDate() {
        Exception exception = assertThrows(NullPointerException.class, () -> {
            testTransaction.setDate(null);
        });
        assertTrue(exception.getMessage().contains("null"));
    }


    @Test
    public void testToStringMethod() {
        String transactionString = testTransaction.toString();
        assertNotNull(transactionString);
        assertTrue(transactionString.contains("Weekly grocery shopping"));
    }

}
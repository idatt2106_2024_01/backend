package no.ntnu.sparestibackend.model;

import jakarta.persistence.EntityManager;
import jakarta.persistence.EntityTransaction;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

/**
 * Test class for Category.
 * Verifies the functionality and handling of Category entities.
 */
public class CategoryTest {

    private EntityManager entityManager;
    private EntityTransaction transaction;
    private Category category;
    private User user;

    @BeforeEach
    public void setUp() {
        entityManager = mock(EntityManager.class);
        transaction = mock(EntityTransaction.class);
        when(entityManager.getTransaction()).thenReturn(transaction);

        category = new Category();
        category.setName("Utilities");
        category.setCategoryId(1L);
    }

    @Test
    public void testCategoryPersistence() {
        assertNotNull(category);
        assertEquals(1L, category.getCategoryId());
        assertEquals("Utilities", category.getName());

        entityManager.persist(category);
        verify(entityManager).persist(any(Category.class));
        verify(transaction, never()).begin();

        Category foundCategory = new Category();
        foundCategory.setName("Utilities");
        when(entityManager.find(Category.class, 1L)).thenReturn(foundCategory);

        Category queriedCategory = entityManager.find(Category.class, 1L);
        assertNotNull(queriedCategory);
        assertEquals("Utilities", queriedCategory.getName());
    }

    @Test
    public void testCategoryUpdates() {
        assertEquals("Utilities", category.getName());
        category.setName("Entertainment");
        assertEquals("Entertainment", category.getName());

        entityManager.merge(category);
        verify(entityManager).merge(category);
        verify(transaction, never()).commit();

        when(entityManager.find(Category.class, category.getCategoryId())).thenReturn(category);
        Category updatedCategory = entityManager.find(Category.class, category.getCategoryId());
        assertNotNull(updatedCategory);
        assertEquals("Entertainment", updatedCategory.getName());
    }

    @Test
    public void testCategoryDeletion() {
        entityManager.remove(category);
        verify(entityManager).remove(category);
    }

    @Test
    public void testNoArgsConstructor() {
        Category emptyCategory = new Category();
        assertNotNull(emptyCategory);
        assertNull(emptyCategory.getName());
    }

    @Test
    public void testRequiredArgsConstructor() {
        Category requiredCategory = new Category();
        requiredCategory.setName("Essentials");
        assertNotNull(requiredCategory);
        assertEquals("Essentials", requiredCategory.getName());
    }

    @Test
    public void testAllArgsConstructor() {
        Category fullCategory = new Category();
        fullCategory.setCategoryId(2L);
        fullCategory.setName("Groceries");
        fullCategory.setUser(user);
        assertNotNull(fullCategory);
        assertEquals(2L, fullCategory.getCategoryId());
        assertEquals("Groceries", fullCategory.getName());
    }

    @Test
    public void testBuilder() {
        Category builtCategory = Category.builder()
                .categoryId(3L)
                .name("Rent")
                .img("/img/rent.png")
                .build();
        assertNotNull(builtCategory);
        assertEquals(3L, builtCategory.getCategoryId());
        assertEquals("Rent", builtCategory.getName());
    }

    @Test
    public void testToStringMethod() {
        String categoryString = category.toString();
        assertNotNull(categoryString);
        assertTrue(categoryString.contains("Utilities"));
    }
}

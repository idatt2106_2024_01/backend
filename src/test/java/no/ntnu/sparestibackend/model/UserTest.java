package no.ntnu.sparestibackend.model;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class UserTest {
    @Test
    public void test_getters() {
        User user = User.builder()
                .userId(1L)
                .email("test@test.no")
                .firstName("test")
                .lastName("test")
                .build();

        assertEquals(1L, user.getUserId());
        assertEquals("test@test.no", user.getEmail());
        assertEquals("test", user.getFirstName());
        assertEquals("test", user.getLastName());
    }

    @Test
    public void test_setters() {
        User user = new User();
        user.setUserId(1L);
        user.setEmail("test@test.no");
        user.setFirstName("test");
        user.setLastName("test");

        assertEquals(1L, user.getUserId());
        assertEquals("test@test.no", user.getEmail());
        assertEquals("test", user.getFirstName());
        assertEquals("test", user.getLastName());
    }

    @Test
    public void testBankAccountsHandling() {
        User user = new User();
        BankAccount account = new BankAccount();
        user.getBankAccounts().add(account);

        assertTrue(user.getBankAccounts().contains(account));
    }

    @Test
    public void testUserBadgesHandling() {
        User user = new User();
        UserBadge badge = new UserBadge();
        user.getUserBadges().add(badge);

        assertTrue(user.getUserBadges().contains(badge));
    }

    @Test
    public void testProfileAvatar() {
        User user = User.builder()
                .firstName("test")
                .lastName("test")
                .profileAvatar("http://example.com/avatar.png")
                .build();
        assertEquals("http://example.com/avatar.png", user.getProfileAvatar());
    }

    @Test
    public void testNewsletterConsent() {
        User user = new User();
        user.setNewsletterConsent(true);

        assertTrue(user.isNewsletterConsent());
    }
}

package no.ntnu.sparestibackend.model;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Date;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

public class SavingChallengeTest {
    private SavingChallenge savingChallenge;
    private User user;
    private Category category;
    private Date testStartDate = new Date();
    private Date testEndDate = new Date();

    @BeforeEach
    void setUp() {
        user = User.builder()
                .userId(1L)
                .firstName("Test")
                .lastName("User")
                .build();
        category = Category.builder()
                .categoryId(1L)
                .name("Category Name")
                .img("/path/to/image")
                .build();
        savingChallenge = SavingChallenge.builder()
                .savingChallengeId(1L)
                .description("Challenge Description")
                .targetSpending(1000)
                .currentSpending(5000)
                .startDate(testStartDate)
                .endDate(testEndDate)
                .status(Status.ACTIVE)
                .difficulty(Difficulty.EASY)
                .category(category)
                .user(user)
                .build();
    }

    @Test
    public void testSetSavingChallengeId() {
        savingChallenge.setSavingChallengeId(2L);
        assertEquals(2L, savingChallenge.getSavingChallengeId());
    }

    @Test
    public void testSetDescription() {
        savingChallenge.setDescription("New Challenge Description");
        assertEquals("New Challenge Description", savingChallenge.getDescription());
    }

    @Test
    public void testSetCategory() {
        Category newCategory = new Category();
        newCategory.setCategoryId(2L);
        newCategory.setName("New Category");
        savingChallenge.setCategory(newCategory);
        assertEquals(newCategory, savingChallenge.getCategory());
    }

    @Test
    public void testSetCurrentSpending() {
        savingChallenge.setCurrentSpending(2000);
        assertEquals(2000, savingChallenge.getCurrentSpending());
    }

    @Test
    public void testSetTargetSpending() {
        savingChallenge.setTargetSpending(10000);
        assertEquals(10000, savingChallenge.getTargetSpending());
    }

    @Test
    public void testSetStartDate() {
        Date newStartDate = new Date();
        savingChallenge.setStartDate(newStartDate);
        assertEquals(newStartDate, savingChallenge.getStartDate());
    }

    @Test
    public void testSetEndDate() {
        Date newEndDate = new Date();
        savingChallenge.setEndDate(newEndDate);
        assertEquals(newEndDate, savingChallenge.getEndDate());
    }

    @Test
    public void testSetStatus() {
        savingChallenge.setStatus(Status.COMPLETED);
        assertEquals(Status.COMPLETED, savingChallenge.getStatus());
    }

    @Test
    public void testSetDifficulty() {
        savingChallenge.setDifficulty(Difficulty.MEDIUM);
        assertEquals(Difficulty.MEDIUM, savingChallenge.getDifficulty());
    }

    @Test
    public void testSetSavingGoal() {
        SavingGoal newSavingGoal = new SavingGoal();
        savingChallenge.setSavingGoal(newSavingGoal);
        assertEquals(newSavingGoal, savingChallenge.getSavingGoal());
    }

    @Test
    public void testSetUser() {
        User newUser = new User();
        newUser.setUserId(2L);
        savingChallenge.setUser(newUser);
        assertEquals(newUser, savingChallenge.getUser());
    }

    @Test
    void testSavingChallengeBuilder() {
        Date startDate = new Date();
        Date endDate = new Date(startDate.getTime() + 1000 * 60 * 60 * 24); // One day later
        Long categoryId = 1L;
        Long userId = 1L;
        Long goalId = 1L;

        Category category = new Category();
        category.setCategoryId(categoryId);

        User user = new User();
        user.setUserId(userId);

        SavingGoal savingGoal = new SavingGoal();
        savingGoal.setSavingGoalId(goalId);

        SavingChallenge challenge = SavingChallenge.builder()
                .savingChallengeId(1L)
                .description("Save money on coffee")
                .category(category)
                .currentSpending(50)
                .targetSpending(25)
                .startDate(startDate)
                .endDate(endDate)
                .status(Status.ACTIVE)
                .difficulty(Difficulty.EASY)
                .savingGoal(savingGoal)
                .user(user)
                .build();

        assertNotNull(challenge);
        assertEquals(Long.valueOf(1L), challenge.getSavingChallengeId());
        assertEquals("Save money on coffee", challenge.getDescription());
        assertEquals(category, challenge.getCategory());
        assertEquals(50, challenge.getCurrentSpending());
        assertEquals(25, challenge.getTargetSpending());
        assertEquals(startDate, challenge.getStartDate());
        assertEquals(endDate, challenge.getEndDate());
        assertEquals(Status.ACTIVE, challenge.getStatus());
        assertEquals(Difficulty.EASY, challenge.getDifficulty());
        assertEquals(savingGoal, challenge.getSavingGoal());
        assertEquals(user, challenge.getUser());
    }
}

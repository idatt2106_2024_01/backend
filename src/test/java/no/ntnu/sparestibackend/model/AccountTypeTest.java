package no.ntnu.sparestibackend.model;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

/**
 * Test class for the AccountType enum
 */
public class AccountTypeTest {

    @Test
    public void testEnumValuesExist() {
        assertNotNull(AccountType.valueOf("SAVINGS"), "SAVINGS should exist.");
        assertNotNull(AccountType.valueOf("CHECKING"), "CHECKING should exist.");
    }

    @Test
    public void testCorrectNumberOfValues() {
        assertEquals(2, AccountType.values().length, "There should be exactly two enum values.");
    }
}

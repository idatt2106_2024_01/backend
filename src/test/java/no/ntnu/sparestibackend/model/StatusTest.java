package no.ntnu.sparestibackend.model;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

/**
 * Test class for the Status enum.
 */
public class StatusTest {

    @Test
    public void testEnumValuesExist() {
        assertNotNull(Status.valueOf("ACTIVE"), "ACTIVE should exist.");
        assertNotNull(Status.valueOf("COMPLETED"), "COMPLETED should exist.");
        assertNotNull(Status.valueOf("FAILED"), "FAILED should exist.");
    }

    @Test
    public void testCorrectNumberOfValues() {
        assertEquals(3, Status.values().length, "There should be exactly three enum values.");
    }
}

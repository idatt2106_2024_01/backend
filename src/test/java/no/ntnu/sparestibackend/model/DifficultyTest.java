package no.ntnu.sparestibackend.model;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

/**
 * Test class for the Difficulty enum.
 */
public class DifficultyTest {

    @Test
    public void testEnumValuesExist() {
        assertNotNull(Difficulty.valueOf("EASY"), "EASY should exist.");
        assertNotNull(Difficulty.valueOf("MEDIUM"), "MEDIUM should exist.");
        assertNotNull(Difficulty.valueOf("HARD"), "HARD should exist.");
    }

    @Test
    public void testCorrectNumberOfValues() {
        assertEquals(3, Difficulty.values().length, "There should be exactly three enum values.");
    }
}

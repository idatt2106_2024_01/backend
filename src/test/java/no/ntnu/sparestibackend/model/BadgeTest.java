package no.ntnu.sparestibackend.model;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

/**
 * Test class for the Badge entity.
 * This class tests the functionality of getters, setters, and constructors
 * to ensure proper handling of Badge attributes.
 */
public class BadgeTest {
    private Badge badge;

    @BeforeEach
    void setUp() {
        badge = new Badge();
        badge.setName("100kr");
        badge.setBadgeId(1L);
        badge.setDescription("Awarded for saving more than 100kr");
        badge.setUrl("https://example.com/badge.png");
    }

    @Test
    void testGetBadgeId() {
        assertEquals(1L, badge.getBadgeId(),
                "Badge ID should be correctly retrieved.");
    }

    @Test
    void testSetBadgeId() {
        badge.setBadgeId(2L);
        assertEquals(2L, badge.getBadgeId(), "Badge ID should be correctly set.");
    }

    @Test
    void testGetName() {
        assertEquals("100kr", badge.getName(),
                "Badge name should be correctly retrieved.");
    }

    @Test
    void testSetName() {
        badge.setName("Explorer");
        assertEquals("Explorer", badge.getName(), "Badge name should be correctly set.");
    }

    @Test
    void testGetDescription() {
        assertEquals("Awarded for saving more than 100kr", badge.getDescription(),
                "Description should be correctly retrieved.");
    }

    @Test
    void testSetDescription() {
        badge.setDescription("Awarded for visiting new places");
        assertEquals("Awarded for visiting new places", badge.getDescription(),
                "Description should be correctly set.");
    }

    @Test
    void testGetUrl() {
        assertEquals("https://example.com/badge.png", badge.getUrl(),
                "URL should be correctly retrieved.");
    }

    @Test
    void testSetUrl() {
        badge.setUrl("https://example.com/newbadge.png");
        assertEquals("https://example.com/newbadge.png", badge.getUrl(),
                "URL should be correctly set.");
    }

    @Test
    void testToString() {
        assertNotNull(badge.toString(),
                "toString should return a non-null string representation of the badge.");
    }
}

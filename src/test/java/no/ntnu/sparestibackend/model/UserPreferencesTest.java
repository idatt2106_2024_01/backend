package no.ntnu.sparestibackend.model;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.HashSet;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

public class UserPreferencesTest {

    private UserPreferences userPreferences;
    private User user;
    private Category category1;
    private Category category2;

    @BeforeEach
    public void setUp() {
        user = new User();
        userPreferences = UserPreferences.builder()
                .userPreferencesId(1L)
                .user(user)
                .savingLevel(IntensityLevel.SOME)
                .willingnessToInvest(IntensityLevel.A_LOT)
                .theme("Dark")
                .savingCategories(new HashSet<>())
                .build();

        category1 = new Category();
        category2 = new Category();
        userPreferences.getSavingCategories().add(category1);
        userPreferences.getSavingCategories().add(category2);
    }

    @Test
    public void testDefaultValues() {
        assertNotNull(userPreferences);
        assertEquals(1L, userPreferences.getUserPreferencesId());
        assertEquals(user, userPreferences.getUser());
        assertEquals(IntensityLevel.SOME, userPreferences.getSavingLevel());
        assertEquals(IntensityLevel.A_LOT, userPreferences.getWillingnessToInvest());
        assertEquals("Dark", userPreferences.getTheme());
        assertEquals(2, userPreferences.getSavingCategories().size());
    }

    @Test
    public void testToString() {
        userPreferences.setNickname("NicknameExample");

        String expectedString = "UserPreferences(userPreferencesId=1, user=" + user +
                ", savingLevel=SOME, willingnessToInvest=A_LOT, theme=Dark, " +
                "savingCategories=[" + category1 + ", " + category2 + "], nickname=NicknameExample)";

        assertEquals(expectedString, userPreferences.toString(), "toString output should match expected");
    }


    @Test
    public void testBuilder() {
        UserPreferences builtPreferences = UserPreferences.builder()
                .userPreferencesId(2L)
                .user(user)
                .savingLevel(IntensityLevel.A_LITTLE)
                .willingnessToInvest(IntensityLevel.SOME)
                .theme("Light")
                .savingCategories(new HashSet<>())
                .build();

        assertNotNull(builtPreferences);
        assertEquals(2L, builtPreferences.getUserPreferencesId());
        assertEquals("Light", builtPreferences.getTheme());
    }
}

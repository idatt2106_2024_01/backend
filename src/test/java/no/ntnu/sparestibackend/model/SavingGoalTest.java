package no.ntnu.sparestibackend.model;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Date;
import java.util.HashSet;

import static org.junit.jupiter.api.Assertions.*;

public class SavingGoalTest {
    private SavingGoal savingGoal;
    private final Date deadline = new Date();
    private User testUser;

    @BeforeEach
    void setUp() {
        testUser = new User();
        savingGoal = new SavingGoal();

        savingGoal.setSavingGoalId(1l);
        savingGoal.setTitle("Holiday");
        savingGoal.setGoal(5000);
        savingGoal.setDeadline(deadline);
        savingGoal.setUser(testUser);
        savingGoal.setSavingChallenges(new HashSet<>());
        savingGoal.setStatus(Status.ACTIVE);
    }

    @Test
    public void testGetTitle() {
        assertEquals("Holiday", savingGoal.getTitle(), "getTitle should return the correct title.");
    }

    @Test
    public void testSetTitle() {
        savingGoal.setTitle("Vacation Fund");
        assertEquals("Vacation Fund", savingGoal.getTitle(), "setTitle should update the title.");
    }

    @Test
    public void testGetGoal() {
        assertEquals(5000, savingGoal.getGoal(), "getGoal should return the correct goal amount.");
    }

    @Test
    public void testSetGoal() {
        savingGoal.setGoal(10000);
        assertEquals(10000, savingGoal.getGoal(), "setGoal should update the goal amount.");
    }

    @Test
    public void testGetDeadline() {
        assertEquals(deadline, savingGoal.getDeadline(), "getDeadline should return the correct deadline.");
    }

    @Test
    public void testSetDeadline() {
        Date newDeadline = new Date();
        savingGoal.setDeadline(newDeadline);
        assertEquals(newDeadline, savingGoal.getDeadline(), "setDeadline should update the deadline.");
    }

    @Test
    public void testToString() {
        String expected = "SavingGoal(savingGoalId=1, title=Holiday, goal=5000, deadline=" + deadline + "," +
                " savingChallenges=[], status=ACTIVE)";
        assertEquals(expected, savingGoal.toString(), "toString should return the correct string representation.");
    }

    @Test
    public void testNoArgsConstructor() {
        SavingGoal emptyGoal = new SavingGoal();
        assertNotNull(emptyGoal, "NoArgsConstructor should create a non-null SavingGoal object.");
        assertNull(emptyGoal.getTitle(),
                "Newly created SavingGoal should have null title when using NoArgsConstructor.");
    }

    @Test
    public void testAllArgsConstructor() {
        SavingGoal fullGoal = new SavingGoal();
        fullGoal.setSavingGoalId(1L);
        fullGoal.setTitle("House Fund");
        fullGoal.setGoal(20000);
        fullGoal.setDeadline(deadline);
        fullGoal.setUser(testUser);
        fullGoal.setSavingChallenges(new HashSet<>());
        fullGoal.setStatus(Status.ACTIVE);

        assertNotNull(fullGoal, "AllArgsConstructor should create a non-null SavingGoal object.");
        assertEquals("House Fund", fullGoal
                .getTitle(), "AllArgsConstructor should set the title correctly.");
        assertEquals(20000, fullGoal.getGoal(), "AllArgsConstructor should set the goal correctly.");
        assertEquals(deadline, fullGoal.getDeadline(), "AllArgsConstructor should set the deadline correctly.");
    }

    @Test
    public void testBuilderPattern() {
        SavingGoal builtGoal = SavingGoal.builder()
                .savingGoalId(1L)
                .title("Retirement Fund")
                .goal(100000)
                .deadline(deadline)
                .build();
        assertEquals("Retirement Fund", builtGoal.getTitle(), "Builder should set the title correctly.");
        assertEquals(100000, builtGoal.getGoal(), "Builder should set the goal amount correctly.");
        assertEquals(deadline, builtGoal.getDeadline(), "Builder should set the deadline correctly.");
    }

    @Test
    public void testNullTitle() {
        Exception exception = assertThrows(NullPointerException.class, () -> {
            savingGoal.setTitle(null);
        });
        assertNotNull(exception, "setTitle should throw NullPointerException when setting null.");
    }
}

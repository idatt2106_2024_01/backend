package no.ntnu.sparestibackend.model;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Date;

import static org.junit.jupiter.api.Assertions.*;


/**
 * Test class for the UserBadge entity.
 * Verifies the proper functioning of getters, setters, and interactions
 * between User, Badge, and UserBadge entities.
 */
public class UserBadgeTest {
    private UserBadge userBadge;
    private Date dateNow;

    @BeforeEach
    void setUp() {
        User user = new User();
        user.setUserId(1L);
        Badge badge = new Badge();
        badge.setBadgeId(1L);

        dateNow = new Date();

        userBadge = new UserBadge();
        userBadge.setId(1L);
        userBadge.setUser(user);
        userBadge.setBadge(badge);
        userBadge.setAchieved(true);
        userBadge.setDateAchieved(dateNow);
    }

    @Test
    void getId() {
        assertEquals(1L, userBadge.getId(), "Id should be correctly retrieved.");
    }

    @Test
    void getUser() {
        assertNotNull(userBadge.getUser(), "User should be correctly retrieved.");
        assertEquals(1L, userBadge.getUser().getUserId(), "User ID should match.");
    }

    @Test
    void getBadge() {
        assertNotNull(userBadge.getBadge(), "Badge should be correctly retrieved.");
        assertEquals(1L, userBadge.getBadge().getBadgeId(), "Badge ID should match.");
    }

    @Test
    void isAchieved() {
        assertTrue(userBadge.isAchieved(), "Achieved should be true.");
    }

    @Test
    void getDateAchieved() {
        assertEquals(dateNow, userBadge.getDateAchieved(), "Date achieved should be correctly retrieved.");
    }

    @Test
    void testSetAchieved() {
        userBadge.setAchieved(false);
        assertFalse(userBadge.isAchieved(), "Achieved should be false after set to false.");
    }

    @Test
    void testSetDateAchieved() {
        Date newDate = new Date();
        userBadge.setDateAchieved(newDate);
        assertEquals(newDate, userBadge.getDateAchieved(), "Date achieved should be correctly updated.");
    }

    @Test
    void noArgsConstructorTest() {
        UserBadge newUserBadge = new UserBadge();
        assertNull(newUserBadge.getId(), "ID should be null");
        assertNull(newUserBadge.getUser(), "User should be null");
        assertNull(newUserBadge.getBadge(), "Badge should be null");
        assertFalse(newUserBadge.isAchieved(), "Achieved should be false");
        assertNull(newUserBadge.getDateAchieved(), "DateAchieved should be null");
    }

    @Test
    void allArgsConstructorTest() {
        User user = new User();
        user.setUserId(1L);
        Badge badge = new Badge();
        badge.setBadgeId(1L);
        Date date = new Date();
        UserBadge userBadge = new UserBadge(1L, user, badge, true, date);

        assertEquals(1L, userBadge.getId(), "ID should be correctly initialized");
        assertEquals(user, userBadge.getUser(), "User should be correctly initialized");
        assertEquals(badge, userBadge.getBadge(), "Badge should be correctly initialized");
        assertTrue(userBadge.isAchieved(), "Achieved should be true");
        assertEquals(date, userBadge.getDateAchieved(), "DateAchieved should be correctly initialized");
    }

}

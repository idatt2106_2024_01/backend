package no.ntnu.sparestibackend.dto;

import org.junit.jupiter.api.Test;

import java.util.Collections;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

public class AuthResponseDTOTest {

    @Test
    public void testAuthResponseDTOCreation() {
        String token = "some-jwt-token";
        UserInfoDTO user = UserInfoDTO.builder()
                .userId(1L)
                .email("user@example.com")
                .firstName("John")
                .lastName("Doe")
                .bankAccounts(Collections.emptyList())
                .build();

        AuthResponseDTO authResponse = AuthResponseDTO.builder()
                .token(token)
                .user(user)
                .build();

        assertNotNull(authResponse);
        assertEquals(token, authResponse.token());
        assertEquals(user, authResponse.user());
    }


    @Test
    public void testEquality() {
        String token = "some-jwt-token";
        UserInfoDTO user = UserInfoDTO.builder()
                .userId(1L)
                .email("user@example.com")
                .firstName("John")
                .lastName("Doe")
                .bankAccounts(Collections.emptyList())
                .build();

        AuthResponseDTO authResponse1 = AuthResponseDTO.builder()
                .token(token)
                .user(user)
                .build();

        AuthResponseDTO authResponse2 = AuthResponseDTO.builder()
                .token(token)
                .user(user)
                .build();

        assertEquals(authResponse1, authResponse2);
    }
}

package no.ntnu.sparestibackend.dto;

import no.ntnu.sparestibackend.model.IntensityLevel;
import org.junit.jupiter.api.Test;

import java.util.HashSet;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.*;

public class UserPreferencesDTOTest {

    @Test
    public void testUserPreferencesDTOCreation() {
        IntensityLevel savingLevel = IntensityLevel.A_LOT;
        IntensityLevel willingnessToInvest = IntensityLevel.SOME;
        String theme = "Dark";
        Set<Long> savingCategoryIds = new HashSet<>();
        savingCategoryIds.add(1L);
        savingCategoryIds.add(2L);

        UserPreferencesDTO userPreferences = UserPreferencesDTO.builder()
                .savingLevel(savingLevel)
                .willingnessToInvest(willingnessToInvest)
                .theme(theme)
                .savingCategoryIds(savingCategoryIds)
                .build();

        assertNotNull(userPreferences);
        assertEquals(savingLevel, userPreferences.savingLevel());
        assertEquals(willingnessToInvest, userPreferences.willingnessToInvest());
        assertEquals(theme, userPreferences.theme());
        assertEquals(savingCategoryIds, userPreferences.savingCategoryIds());
    }

    @Test
    public void testNullValues() {
        Set<Long> savingCategoryIds = null;

        UserPreferencesDTO userPreferences = UserPreferencesDTO.builder()
                .savingLevel(null)
                .willingnessToInvest(null)
                .theme(null)
                .savingCategoryIds(savingCategoryIds)
                .build();

        assertNotNull(userPreferences);
        assertNull(userPreferences.savingLevel());
        assertNull(userPreferences.willingnessToInvest());
        assertNull(userPreferences.theme());
        assertNull(userPreferences.savingCategoryIds());
    }

    @Test
    public void testEquality() {
        IntensityLevel savingLevel = IntensityLevel.A_LOT;
        IntensityLevel willingnessToInvest = IntensityLevel.SOME;
        String theme = "Dark";
        Set<Long> savingCategoryIds = new HashSet<>();
        savingCategoryIds.add(1L);
        savingCategoryIds.add(2L);

        UserPreferencesDTO userPreferences1 = UserPreferencesDTO.builder()
                .savingLevel(savingLevel)
                .willingnessToInvest(willingnessToInvest)
                .theme(theme)
                .savingCategoryIds(savingCategoryIds)
                .build();

        UserPreferencesDTO userPreferences2 = UserPreferencesDTO.builder()
                .savingLevel(savingLevel)
                .willingnessToInvest(willingnessToInvest)
                .theme(theme)
                .savingCategoryIds(savingCategoryIds)
                .build();

        assertEquals(userPreferences1, userPreferences2);
    }
}

package no.ntnu.sparestibackend.dto;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertEquals;

class CategoryDTOTest {
    @Test
    void testBuilder() {
        String categoryName = "Electronics";

        CategoryDTO categoryDTO = CategoryDTO.builder()
                .name(categoryName)
                .build();

        assertAll("DTO should correctly store and return all values",
                () -> assertEquals(categoryName, categoryDTO.name(), "Category name should match")
        );
    }
}

package no.ntnu.sparestibackend.dto;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class UserRegisterDTOTest {

    @Test
    public void testUserRegisterDTOCreation() {
        String email = "user@example.com";
        String password = "securepassword";
        String firstName = "John";
        String lastName = "Doe";

        UserRegisterDTO userRegister = UserRegisterDTO.builder()
                .email(email)
                .password(password)
                .firstName(firstName)
                .lastName(lastName)
                .build();

        assertNotNull(userRegister);
        assertEquals(email, userRegister.email());
        assertEquals(password, userRegister.password());
        assertEquals(firstName, userRegister.firstName());
        assertEquals(lastName, userRegister.lastName());
    }

    @Test
    public void testNullValues() {
        UserRegisterDTO userRegister = UserRegisterDTO.builder()
                .email("user@example.com")
                .password(null)
                .firstName(null)
                .lastName(null)
                .build();

        assertNotNull(userRegister);
        assertEquals("user@example.com", userRegister.email());
        assertNull(userRegister.password());
        assertNull(userRegister.firstName());
        assertNull(userRegister.lastName());
    }

    @Test
    public void testEquality() {
        String email = "user@example.com";
        String password = "securepassword";
        String firstName = "John";
        String lastName = "Doe";

        UserRegisterDTO userRegister1 = UserRegisterDTO.builder()
                .email(email)
                .password(password)
                .firstName(firstName)
                .lastName(lastName)
                .build();

        UserRegisterDTO userRegister2 = UserRegisterDTO.builder()
                .email(email)
                .password(password)
                .firstName(firstName)
                .lastName(lastName)
                .build();

        assertEquals(userRegister1, userRegister2);
    }
}

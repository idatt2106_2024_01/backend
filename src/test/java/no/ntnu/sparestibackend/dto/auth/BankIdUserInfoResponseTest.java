package no.ntnu.sparestibackend.dto.auth;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

/**
 * Unit tests for the {@link BankIdUserInfoResponse} class.
 */
public class BankIdUserInfoResponseTest {

    /**
     * Tests the creation of a BankIdUserInfoResponse instance and verifies that all fields are set correctly.
     */
    @Test
    public void testBankIdUserInfoResponseCreation() {
        String idpId = "idp123";
        String givenName = "John";
        String familyName = "Doe";

        BankIdUserInfoResponse response = BankIdUserInfoResponse.builder()
                .idpId(idpId)
                .givenName(givenName)
                .familyName(familyName)
                .build();

        assertNotNull(response);
        assertEquals(idpId, response.idpId());
        assertEquals(givenName, response.givenName());
        assertEquals(familyName, response.familyName());
    }

    /**
     * Tests the equality of two BankIdUserInfoResponse instances created with the same data.
     */
    @Test
    public void testEquality() {
        String idpId = "idp123";
        String givenName = "John";
        String familyName = "Doe";

        BankIdUserInfoResponse response1 = BankIdUserInfoResponse.builder()
                .idpId(idpId)
                .givenName(givenName)
                .familyName(familyName)
                .build();

        BankIdUserInfoResponse response2 = BankIdUserInfoResponse.builder()
                .idpId(idpId)
                .givenName(givenName)
                .familyName(familyName)
                .build();

        assertEquals(response1, response2);
    }
}

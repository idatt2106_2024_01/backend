package no.ntnu.sparestibackend.dto.auth;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

/**
 * Unit tests for the {@link BankIdTokenResponse} class.
 */
public class BankIdTokenResponseTest {

    /**
     * Tests the creation of a BankIdTokenResponse instance and verifies that all fields are set correctly.
     */
    @Test
    public void testBankIdTokenResponseCreation() {
        String idToken = "idTokenValue";
        String accessToken = "accessTokenValue";
        String refreshToken = "refreshTokenValue";
        String expiresIn = "3600";
        String tokenType = "Bearer";
        String scope = "openid";

        BankIdTokenResponse response = BankIdTokenResponse.builder()
                .idToken(idToken)
                .accessToken(accessToken)
                .refreshToken(refreshToken)
                .expiresIn(expiresIn)
                .tokenType(tokenType)
                .scope(scope)
                .build();

        assertNotNull(response);
        assertEquals(idToken, response.idToken());
        assertEquals(accessToken, response.accessToken());
        assertEquals(refreshToken, response.refreshToken());
        assertEquals(expiresIn, response.expiresIn());
        assertEquals(tokenType, response.tokenType());
        assertEquals(scope, response.scope());
    }

    /**
     * Tests the equality of two BankIdTokenResponse instances created with the same data.
     */
    @Test
    public void testEquality() {
        String idToken = "idTokenValue";
        String accessToken = "accessTokenValue";
        String refreshToken = "refreshTokenValue";
        String expiresIn = "3600";
        String tokenType = "Bearer";
        String scope = "openid";

        BankIdTokenResponse response1 = BankIdTokenResponse.builder()
                .idToken(idToken)
                .accessToken(accessToken)
                .refreshToken(refreshToken)
                .expiresIn(expiresIn)
                .tokenType(tokenType)
                .scope(scope)
                .build();

        BankIdTokenResponse response2 = BankIdTokenResponse.builder()
                .idToken(idToken)
                .accessToken(accessToken)
                .refreshToken(refreshToken)
                .expiresIn(expiresIn)
                .tokenType(tokenType)
                .scope(scope)
                .build();

        assertEquals(response1, response2);
    }
}

package no.ntnu.sparestibackend.dto;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class UserStatsDTOTest {

    @Test
    public void testUserStatsDTOCreation() {
        Double totalSavings = 5000.0;
        Integer hardChallengesCompleted = 5;
        Integer successfulStreak = 10;

        UserStatsDTO stats = UserStatsDTO.builder()
                .totalSavings(totalSavings)
                .hardChallengesCompleted(hardChallengesCompleted)
                .successfulStreak(successfulStreak)
                .build();

        assertNotNull(stats);
        assertEquals(totalSavings, stats.totalSavings());
        assertEquals(hardChallengesCompleted, stats.hardChallengesCompleted());
        assertEquals(successfulStreak, stats.successfulStreak());
    }

    @Test
    public void testNullValues() {
        UserStatsDTO stats = UserStatsDTO.builder()
                .totalSavings(null)
                .hardChallengesCompleted(null)
                .successfulStreak(null)
                .build();

        assertNotNull(stats);
        assertNull(stats.totalSavings());
        assertNull(stats.hardChallengesCompleted());
        assertNull(stats.successfulStreak());
    }

    @Test
    public void testEquality() {
        Double totalSavings = 2000.0;
        Integer hardChallengesCompleted = 3;
        Integer successfulStreak = 7;

        UserStatsDTO stats1 = UserStatsDTO.builder()
                .totalSavings(totalSavings)
                .hardChallengesCompleted(hardChallengesCompleted)
                .successfulStreak(successfulStreak)
                .build();

        UserStatsDTO stats2 = UserStatsDTO.builder()
                .totalSavings(totalSavings)
                .hardChallengesCompleted(hardChallengesCompleted)
                .successfulStreak(successfulStreak)
                .build();

        assertEquals(stats1, stats2);
    }
}


package no.ntnu.sparestibackend.dto;

import no.ntnu.sparestibackend.model.AccountType;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class BankAccountDTOTest {

    @Test
    public void testBankAccountDTOCreation() {
        String name = "Main Account";
        Long accountNumber = 123456789L;
        AccountType accountType = AccountType.SAVINGS;

        BankAccountDTO bankAccount = BankAccountDTO.builder()
                .name(name)
                .accountNumber(accountNumber)
                .accountType(accountType)
                .build();

        assertNotNull(bankAccount);
        assertEquals(name, bankAccount.name());
        assertEquals(accountNumber, bankAccount.accountNumber());
        assertEquals(accountType, bankAccount.accountType());
    }

    @Test
    public void testNonNullFields() {
        assertThrows(NullPointerException.class, () -> {
            BankAccountDTO.builder()
                    .name(null)
                    .accountNumber(123456789L)
                    .accountType(AccountType.SAVINGS)
                    .build();
        });
    }

    @Test
    public void testEquality() {
        String name = "Main Account";
        Long accountNumber = 123456789L;
        AccountType accountType = AccountType.CHECKING;

        BankAccountDTO bankAccount1 = BankAccountDTO.builder()
                .name(name)
                .accountNumber(accountNumber)
                .accountType(accountType)
                .build();

        BankAccountDTO bankAccount2 = BankAccountDTO.builder()
                .name(name)
                .accountNumber(accountNumber)
                .accountType(accountType)
                .build();

        assertEquals(bankAccount1, bankAccount2);
    }
}

package no.ntnu.sparestibackend.dto;

import org.junit.jupiter.api.Test;

import java.util.Date;

import static org.junit.jupiter.api.Assertions.assertEquals;

/**
 * Test class for {@link SavingGoalDTO}.
 */
public class SavingGoalDTOTest {

    @Test
    public void testBuilder() {
        String expectedTitle = "Vacation Fund";
        Integer expectedGoal = 5000;
        Date expectedDeadline = new Date();

        SavingGoalDTO savingGoalDTO = SavingGoalDTO.builder()
                .title(expectedTitle)
                .goal(expectedGoal)
                .deadline(expectedDeadline)
                .build();

        assertEquals(expectedTitle, savingGoalDTO.title(), "The title should match the input value.");
        assertEquals(expectedGoal, savingGoalDTO.goal(), "The goal should match the input value.");
        assertEquals(expectedDeadline, savingGoalDTO.deadline(), "The deadline should match the input value.");
    }
}

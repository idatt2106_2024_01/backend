package no.ntnu.sparestibackend.dto;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;

import java.util.Date;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertEquals;

/**
 * Test class for TransactionDTO.
 */
class TransactionDTOTest {

    /**
     * Test that the builder correctly stores and returns all values.
     */
    @Test
    void testBuilder() {
        Long transactionId = 1L;
        Long categoryId = 2L;
        Date date = new Date();
        String description = "Transaction for groceries";
        double sum = 59.99;
        Long bankAccountId = 3L;

        TransactionDTO transactionDTO = TransactionDTO.builder()
                .transactionId(transactionId)
                .categoryId(categoryId)
                .date(date)
                .description(description)
                .sum(sum)
                .accountNumber(bankAccountId)
                .build();

        assertAll("DTO should correctly store and return all values",
                () -> assertEquals(transactionId, transactionDTO.transactionId(), "Transaction ID should match"),
                () -> assertEquals(categoryId, transactionDTO.categoryId(), "Category ID should match"),
                () -> assertEquals(date, transactionDTO.date(), "Date should match"),
                () -> assertEquals(description, transactionDTO.description(), "Description should match"),
                () -> assertEquals(sum, transactionDTO.sum(), "Sum should match"),
                () -> assertEquals(bankAccountId, transactionDTO.accountNumber(), "Bank Account ID should match")
        );
    }

    @Test
    public void testSerializeTransactionDTO() throws Exception {
        TransactionDTO dto = TransactionDTO.builder()
                .transactionId(1L)
                .categoryId(1L)
                .date(new Date())
                .description("Test Transaction")
                .sum(100.0)
                .accountNumber(1L)
                .savingGoalId(1L)
                .build();

        ObjectMapper objectMapper = new ObjectMapper();
        String json = objectMapper.writeValueAsString(dto);
        System.out.println(json);
    }

    @Test
    public void testSerializePageOfTransactionDTO() {
        ObjectMapper objectMapper = new ObjectMapper();

        List<TransactionDTO> list = List.of(new TransactionDTO(1L, 1L, new Date(), "Sample Transaction", 100.0, 1L, 1L));
        Page<TransactionDTO> page = new PageImpl<>(list);
        try {
            String json = objectMapper.writeValueAsString(page);
            System.out.println(json);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
    }

}

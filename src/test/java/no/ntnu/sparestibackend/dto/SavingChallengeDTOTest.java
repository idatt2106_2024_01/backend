package no.ntnu.sparestibackend.dto;

import no.ntnu.sparestibackend.model.Difficulty;
import no.ntnu.sparestibackend.model.Status;
import org.junit.jupiter.api.Test;

import java.util.Date;

import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertEquals;

class SavingChallengeDTOTest {
    @Test
    void testBuilder() {
        Date startDate = new Date();
        Date endDate = new Date();
        String description = "New Challenge";
        Double currentSpending = 100.0;
        Double targetSpending = 500.0;
        Status status = Status.ACTIVE;
        Difficulty difficulty = Difficulty.MEDIUM;

        SavingChallengeDTO dto = SavingChallengeDTO.builder()
                .description(description)
                .currentSpending(currentSpending)
                .targetSpending(targetSpending)
                .startDate(startDate)
                .endDate(endDate)
                .status(status)
                .difficulty(difficulty)
                .build();

        assertAll("DTO should correctly store values",
                () -> assertEquals(description, dto.description()),
                () -> assertEquals(currentSpending, dto.currentSpending()),
                () -> assertEquals(targetSpending, dto.targetSpending()),
                () -> assertEquals(startDate, dto.startDate()),
                () -> assertEquals(endDate, dto.endDate()),
                () -> assertEquals(status, dto.status()),
                () -> assertEquals(difficulty, dto.difficulty())
        );
    }
}

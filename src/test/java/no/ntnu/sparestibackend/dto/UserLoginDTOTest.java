package no.ntnu.sparestibackend.dto;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class UserLoginDTOTest {

    @Test
    public void testUserLoginDTOCreation() {
        String email = "user@example.com";
        String password = "password123";

        UserLoginDTO userLogin = UserLoginDTO.builder()
                .email(email)
                .password(password)
                .build();

        assertNotNull(userLogin);
        assertEquals(email, userLogin.email());
        assertEquals(password, userLogin.password());
    }

    @Test
    public void testNullValues() {
        UserLoginDTO userLogin = UserLoginDTO.builder()
                .email("null@example.com")
                .password(null)
                .build();

        assertNotNull(userLogin);
        assertEquals("null@example.com", userLogin.email());
        assertNull(userLogin.password());
    }

    @Test
    public void testEquality() {
        String email = "user@example.com";
        String password = "password123";

        UserLoginDTO userLogin1 = UserLoginDTO.builder()
                .email(email)
                .password(password)
                .build();

        UserLoginDTO userLogin2 = UserLoginDTO.builder()
                .email(email)
                .password(password)
                .build();

        assertEquals(userLogin1, userLogin2);
    }
}

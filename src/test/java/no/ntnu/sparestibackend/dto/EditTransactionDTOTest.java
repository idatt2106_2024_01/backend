package no.ntnu.sparestibackend.dto;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertEquals;

/**
 * Test class for EditTransactionDTO.
 */
class EditTransactionDTOTest {

    /**
     * Test that the builder correctly stores and returns all values.
     */
    @Test
    void testBuilder() {
        Long categoryId = 5L;
        String description = "Updated transaction description";

        EditTransactionDTO editTransactionDTO = EditTransactionDTO.builder()
                .categoryId(categoryId)
                .description(description)
                .build();

        assertAll("DTO should correctly store and return all values",
                () -> assertEquals(categoryId, editTransactionDTO.categoryId(), "Category ID should match"),
                () -> assertEquals(description, editTransactionDTO.description(), "Description should match")
        );
    }
}

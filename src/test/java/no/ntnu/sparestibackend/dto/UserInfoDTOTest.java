package no.ntnu.sparestibackend.dto;

import no.ntnu.sparestibackend.model.BankAccount;
import org.junit.jupiter.api.Test;

import java.util.Collections;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

public class UserInfoDTOTest {

    @Test
    public void testUserInfoDTOCreation() {
        Long userId = 1L;
        String email = "test@example.com";
        String firstName = "John";
        String lastName = "Doe";
        List<BankAccount> bankAccounts = Collections.emptyList();

        UserInfoDTO userInfo = UserInfoDTO.builder()
                .userId(userId)
                .email(email)
                .firstName(firstName)
                .lastName(lastName)
                .bankAccounts(bankAccounts)
                .build();

        assertNotNull(userInfo);
        assertEquals(userId, userInfo.userId());
        assertEquals(email, userInfo.email());
        assertEquals(firstName, userInfo.firstName());
        assertEquals(lastName, userInfo.lastName());
        assertEquals(bankAccounts, userInfo.bankAccounts());
    }

    @Test
    public void testNullValues() {
        List<BankAccount> bankAccounts = null;

        UserInfoDTO userInfo = UserInfoDTO.builder()
                .userId(2L)
                .email("null@example.com")
                .firstName(null)
                .lastName(null)
                .bankAccounts(bankAccounts)
                .build();

        assertNotNull(userInfo);
        assertNull(userInfo.firstName());
        assertNull(userInfo.lastName());
        assertNull(userInfo.bankAccounts());
    }

    @Test
    public void testEquality() {
        Long userId = 1L;
        String email = "test@example.com";
        String firstName = "John";
        String lastName = "Doe";

        UserInfoDTO userInfo1 = UserInfoDTO.builder()
                .userId(userId)
                .email(email)
                .firstName(firstName)
                .lastName(lastName)
                .build();

        UserInfoDTO userInfo2 = UserInfoDTO.builder()
                .userId(userId)
                .email(email)
                .firstName(firstName)
                .lastName(lastName)
                .build();

        assertEquals(userInfo1, userInfo2);
    }
}

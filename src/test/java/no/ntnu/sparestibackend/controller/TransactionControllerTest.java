package no.ntnu.sparestibackend.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import no.ntnu.sparestibackend.dto.EditTransactionDTO;
import no.ntnu.sparestibackend.dto.TransactionDTO;
import no.ntnu.sparestibackend.model.BankAccount;
import no.ntnu.sparestibackend.model.Category;
import no.ntnu.sparestibackend.model.Transaction;
import no.ntnu.sparestibackend.service.TransactionService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.security.core.Authentication;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.util.Date;

import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * Tests for the TransactionController.
 * This class contains unit tests that verify the functionality of adding, retrieving, and updating transactions
 * through the TransactionController. It utilizes the Mockito framework to mock dependencies and assert interactions.
 */
@ExtendWith(MockitoExtension.class)
public class TransactionControllerTest {
    @Mock
    private TransactionService transactionService;

    @Mock
    private Authentication authentication;

    @InjectMocks
    private TransactionController transactionController;

    private MockMvc mockMvc;

    /**
     * Set up the MockMvc object for testing the TransactionController.
     */
    @BeforeEach
    void setUp() {
        mockMvc = MockMvcBuilders
                .standaloneSetup(transactionController)
                .setCustomArgumentResolvers(new PageableHandlerMethodArgumentResolver())
                .build();
        when(authentication.getPrincipal()).thenReturn("1");
    }

    /**
     * Tests adding a transaction with valid authorization.
     * This method verifies that a transaction can be added by an authenticated and authorized user.
     * It ensures that the transaction is created and the appropriate status is returned.
     *
     * @throws Exception if the test encounters an unexpected error during execution
     */
    @Test
    void testAddTransactionWithValidAuthorization() throws Exception {
        TransactionDTO transactionDTO = new TransactionDTO(null, 2L, new Date(), "Test description", 150.0, 1L, 1L);
        Transaction createdTransaction = new Transaction(1L, new BankAccount(), new Category(), new Date(), "Test description", 150.0, null);

        when(transactionService.userOwnsBankAccount(1L, 1L)).thenReturn(true);
        when(transactionService.createTransaction(eq(1L), any(TransactionDTO.class))).thenReturn(createdTransaction);

        mockMvc.perform(post("/bank-accounts/1/transactions")
                        .principal(authentication)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(new ObjectMapper().writeValueAsString(transactionDTO)))
                .andExpect(status().isCreated())
                .andExpect(jsonPath("$.description").value("Test description"));
    }

    /**
     * Tests that the server correctly handles unauthorized transaction addition attempts by returning a 403 Forbidden status.
     */
    @Test
    void testAddTransactionWithInvalidAuthorization() throws Exception {
        TransactionDTO transactionDTO = new TransactionDTO(null, 2L, new Date(), "Test description", 150.0, 1L, 1L);

        when(transactionService.userOwnsBankAccount(1L, 1L)).thenReturn(false);

        mockMvc.perform(post("/bank-accounts/1/transactions")
                        .principal(authentication)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(new ObjectMapper().writeValueAsString(transactionDTO)))
                .andExpect(status().isForbidden());
    }

    /**
     * Tests the server's response to unauthorized access attempts when retrieving transactions, expecting a 403 Forbidden status.
     */
    @Test
    void testGetTransactionsWithInvalidAuthorization() throws Exception {
        when(transactionService.userOwnsBankAccount(1L, 1L)).thenReturn(false);

        mockMvc.perform(get("/bank-accounts/1/transactions")
                        .principal(authentication))
                .andExpect(status().isForbidden());
    }

    /**
     * Tests updating a transaction with valid authorization.
     * Verifies that the transaction details can be updated by an authorized user and checks that the updated details are correctly reflected in the response.
     */
    @Test
    void testUpdateTransactionWithValidAuthorization() throws Exception {
        EditTransactionDTO editTransactionDTO = new EditTransactionDTO(1L, "Updated description");
        Transaction updatedTransaction = Transaction.builder()
                .transactionId(1L)
                .date(new Date())
                .description("Updated description")
                .build();

        when(transactionService.userOwnsBankAccount(1L, 1L)).thenReturn(true);
        when(transactionService.updateTransaction(eq(1L), any(EditTransactionDTO.class))).thenReturn(updatedTransaction);

        mockMvc.perform(put("/bank-accounts/1/transactions/1")
                        .principal(authentication)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(new ObjectMapper().writeValueAsString(editTransactionDTO)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.description").value("Updated description"));
    }

    /**
     * Tests that updating a transaction without proper authorization results in a 403 Forbidden status.
     */
    @Test
    void testUpdateTransactionWithInvalidAuthorization() throws Exception {
        EditTransactionDTO editTransactionDTO = new EditTransactionDTO(1L, "Updated description");

        when(transactionService.userOwnsBankAccount(1L, 1L)).thenReturn(false);

        mockMvc.perform(put("/bank-accounts/1/transactions/1")
                        .principal(authentication)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(new ObjectMapper().writeValueAsString(editTransactionDTO)))
                .andExpect(status().isForbidden());
    }
}



package no.ntnu.sparestibackend.controller;

import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.*;

import com.fasterxml.jackson.databind.ObjectMapper;
import no.ntnu.sparestibackend.dto.BankAccountDTO;
import no.ntnu.sparestibackend.model.AccountType;
import no.ntnu.sparestibackend.service.BankAccountService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.MediaType;
import org.springframework.security.core.Authentication;
import org.springframework.test.web.servlet.MockMvc;

/**
 * Tests for {@link BankAccountController}. These tests ensure that the
 * web layer responds correctly to HTTP requests, mocking away all the business logic.
 */
@ExtendWith(MockitoExtension.class)
public class BankAccountControllerTest {
    @Mock
    private BankAccountService bankAccountService;

    @Mock
    private Authentication authentication;

    @InjectMocks
    private BankAccountController bankAccountController;

    private MockMvc mockMvc;

    /**
     * Sets up the environment before each test. Initializes the mock environment for Spring MVC tests.
     */
    @BeforeEach
    void setUp() {
        mockMvc = standaloneSetup(bankAccountController).build();
    }

    /**
     * Tests the updating of an existing bank account through the PUT endpoint.
     * Verifies that the server responds appropriately to a valid update request by checking the returned status
     * and content.
     */
    @Test
    void testUpdateBankAccount() throws Exception {
        BankAccountDTO bankAccountDTO = new BankAccountDTO("UpdatedAccount", 123L, AccountType.CHECKING);
        when(bankAccountService.updateBankAccount(eq(123L), any(BankAccountDTO.class))).thenReturn(bankAccountDTO);

        mockMvc.perform(put("/bank-accounts/{accountNumber}", 123)
                        .principal(authentication)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(new ObjectMapper().writeValueAsString(bankAccountDTO)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.name").value("UpdatedAccount"));
    }
}


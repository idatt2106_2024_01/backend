package no.ntnu.sparestibackend.controller;

import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.hasSize;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static org.mockito.Mockito.verify;
import static org.mockito.BDDMockito.given;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.standaloneSetup;

import com.fasterxml.jackson.databind.ObjectMapper;
import no.ntnu.sparestibackend.dto.SavingGoalDTO;
import no.ntnu.sparestibackend.model.SavingChallenge;
import no.ntnu.sparestibackend.model.SavingGoal;
import no.ntnu.sparestibackend.model.Status;
import no.ntnu.sparestibackend.model.User;
import no.ntnu.sparestibackend.service.SavingGoalService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.MediaType;
import org.springframework.security.core.Authentication;
import org.springframework.test.web.servlet.MockMvc;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Provides unit tests for the SavingGoalController class to ensure that the web layer behaves as expected.
 * This class tests the REST API endpoints related to managing saving goals, including creation, update,
 * deletion, and retrieval by status. It uses the Spring MVC Test Framework to mock HTTP requests and
 * assert responses.
 */
@ExtendWith(MockitoExtension.class)
public class SavingGoalControllerTest {
    private MockMvc mockMvc;
    private final ObjectMapper objectMapper = new ObjectMapper();

    @Mock
    private SavingGoalService savingGoalService;

    @Mock
    private User user;

    @Mock
    private Set<SavingChallenge> savingChallenges;

    @InjectMocks
    private SavingGoalController savingGoalController;

    @BeforeEach
    void setUp() {
        mockMvc = standaloneSetup(savingGoalController).build();
        objectMapper.setDateFormat(new SimpleDateFormat("yyyy-MM-dd"));
        user = mock(User.class);
        Set<SavingChallenge> savingChallenges = new HashSet<>();
    }

    /**
     * Tests the retrieval of the total amount saved for a specific saving goal.
     * Verifies that the correct endpoint returns the expected saved amount and status code.
     */
    @Test
    void testGetTotalSaved() throws Exception {
        long goalId = 1L;
        double totalSaved = 1000.00;

        given(savingGoalService.getTotalSavedForSavingGoal(goalId)).willReturn(totalSaved);

        mockMvc.perform(get("/saving-goals/{savingGoalId}/total-saved", goalId))
                .andExpect(status().isOk())
                .andExpect(content().string("1000.0"));

        verify(savingGoalService).getTotalSavedForSavingGoal(goalId);
    }

    /**
     * Tests the updating of a saving goal. Ensures that the service method is called with correct parameters
     * and the endpoint returns the updated goal details and HTTP status code.
     */
    @Test
    void testUpdateSavingGoal() throws Exception {
        Authentication auth = mock(Authentication.class);
        when(auth.getPrincipal()).thenReturn("1");

        Long savingGoalId = 1L;
        Date newDeadline = new SimpleDateFormat("yyyy-MM-dd").parse("2025-01-01");
        SavingGoalDTO updateDto = new SavingGoalDTO(savingGoalId, "Updated Goal", 5500, newDeadline, Status.ACTIVE);
        SavingGoal updatedGoal = new SavingGoal(savingGoalId, "Updated Goal", 5500, newDeadline, user, savingChallenges, Status.ACTIVE);

        given(savingGoalService.updateSavingGoal(eq(1L), eq(savingGoalId), any(SavingGoalDTO.class))).willReturn(updatedGoal);

        mockMvc.perform(put("/saving-goals/{savingGoalId}", savingGoalId)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(updateDto))
                        .principal(auth))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.title").value("Updated Goal"))
                .andExpect(jsonPath("$.goal").value(5500));

        verify(savingGoalService).updateSavingGoal(eq(1L), eq(savingGoalId), any(SavingGoalDTO.class));
    }

    /**
     * Tests the deletion of a saving goal. Verifies the service layer is invoked correctly and
     * the endpoint responds appropriately on successful deletion.
     */
    @Test
    void testDeleteSavingGoal() throws Exception {
        Authentication auth = mock(Authentication.class);
        when(auth.getPrincipal()).thenReturn("1");
        long savingGoalId = 1L;

        given(savingGoalService.deleteSavingGoal(1L, savingGoalId)).willReturn(true);

        mockMvc.perform(delete("/saving-goals/{savingGoalId}", savingGoalId)
                        .principal(auth))
                .andExpect(status().isOk());

        verify(savingGoalService).deleteSavingGoal(1L, savingGoalId);
    }

    /**
     * Tests the retrieval of saving goals by their status for a specific user.
     * Ensures that the service method is correctly filtering goals by status and
     * that the response contains the expected properties and values.
     */
    @Test
    void testGetSavingGoalsByStatus() throws Exception {
        Authentication auth = mock(Authentication.class);
        when(auth.getPrincipal()).thenReturn("1");
        long userId = 1L;

        Status status = Status.ACTIVE;
        Date deadline = new SimpleDateFormat("yyyy-MM-dd").parse("2025-01-01");

        SavingGoal savingGoal = new SavingGoal();
        savingGoal.setSavingGoalId(1L);
        savingGoal.setTitle("Goal 1");
        savingGoal.setGoal(5000);
        savingGoal.setDeadline(deadline);
        savingGoal.setUser(user);
        savingGoal.setSavingChallenges(savingChallenges);
        savingGoal.setStatus(Status.ACTIVE);

        List<SavingGoal> savingGoals = List.of(savingGoal);

        List<SavingGoalDTO> expectedDTOs = savingGoals.stream()
                .map(goal -> new SavingGoalDTO(goal.getSavingGoalId(), goal.getTitle(), goal.getGoal(), goal.getDeadline(), goal.getStatus()))
                .toList();

        given(savingGoalService.getAllSavingGoalsForUser(userId, status)).willReturn(savingGoals);

        mockMvc.perform(get("/saving-goals?status={status}", status.toString())
                        .principal(auth))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(1)))
                .andExpect(jsonPath("$[0].title", is("Goal 1")))
                .andExpect(jsonPath("$[0].status", is("ACTIVE")));

        verify(savingGoalService).getAllSavingGoalsForUser(userId, status);
    }
}

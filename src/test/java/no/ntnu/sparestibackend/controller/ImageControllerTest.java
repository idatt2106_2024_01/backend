package no.ntnu.sparestibackend.controller;

import no.ntnu.sparestibackend.service.TokenService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * Test suite for {@link ImageController} class.
 */
@ExtendWith(SpringExtension.class)
@WebMvcTest(ImageController.class)
public class ImageControllerTest {
    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private TokenService tokenService;

    /**
     * Tests that a GET request to "/images/Coin500.png" returns an image with HTTP status 200 (OK).
     *
     * @throws Exception if the mock MVC request processing fails
     */
    @WithMockUser
    @Test
    public void getImage_ShouldReturnImage() throws Exception {
        mockMvc.perform(get("/images/badge/Coin500.png"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.IMAGE_PNG));
    }

    /**
     * Tests that a GET request to a nonexistent image path "/images/nonexistent.png" returns HTTP status 404 (Not Found).
     *
     * @throws Exception if the mock MVC request processing fails
     */
    @WithMockUser
    @Test
    public void getImage_NotFound() throws Exception {
        mockMvc.perform(get("/images/nonexistent.png"))
                .andExpect(status().isNotFound());
    }

    /**
     * Tests that a GET request to an invalid image path "/images/..%2Ftest.png" returns HTTP status 400 (Bad Request).
     * This test ensures that the system correctly handles potentially malicious input.
     *
     * @throws Exception if the mock MVC request processing fails
     */
    @WithMockUser
    @Test
    public void getImage_BadRequest() throws Exception {
        mockMvc.perform(get("/images/..%2Ftest.png"))
                .andExpect(status().isBadRequest());
    }
}

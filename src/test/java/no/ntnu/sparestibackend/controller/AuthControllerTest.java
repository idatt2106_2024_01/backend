package no.ntnu.sparestibackend.controller;

import jakarta.servlet.http.HttpServletResponse;
import no.ntnu.sparestibackend.service.AuthService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.redirectedUrl;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith(MockitoExtension.class)
public class AuthControllerTest {
    private MockMvc mockMvc;

    @Mock
    private AuthService authService;

    @InjectMocks
    private AuthController authController;

    /**
     * Sets up the test environment before each test, initializing the mock MVC framework with {@link AuthController}.
     */
    @BeforeEach
    public void setUp() {
        mockMvc = MockMvcBuilders.standaloneSetup(authController).build();
    }

    /**
     * Tests the redirection process initiated by the BankID login endpoint.
     * Ensures that the endpoint correctly redirects to the expected URL provided by {@link AuthService}.
     *
     * @throws Exception if the test encounters an error with the request execution
     */
    @Test
    public void loginWithBankIdTest() throws Exception {
        String expectedUrl = "http://example.com/bank-id-login";
        when(authService.createBankIdAuthenticationURL(any(HttpServletResponse.class))).thenReturn(expectedUrl);

        mockMvc.perform(get("/auth/bank-id"))
                .andExpect(status().isFound())
                .andExpect(redirectedUrl(expectedUrl));

        verify(authService).createBankIdAuthenticationURL(any(HttpServletResponse.class));
    }

    /**
     * Tests the handling of the BankID callback endpoint.
     * Verifies that the endpoint correctly handles the authorization code, obtains an authentication token,
     * and redirects the user to the frontend with the token embedded in the URL.
     *
     * @throws Exception if there's an error processing the request or handling the callback logic
     */
    @Test
    public void consumeCallbackTest() throws Exception {
        String code = "authcode123";
        String token = "token123";
        String expectedRedirectUrl = "null/auth?token=" + token;

        when(authService.loginOrRegisterUserFromBankIdAuthentication(code)).thenReturn(token);

        mockMvc.perform(get("/auth/bank-id/callback")
                        .param("code", code))
                .andExpect(status().isFound())
                .andExpect(redirectedUrl(expectedRedirectUrl));

        verify(authService).loginOrRegisterUserFromBankIdAuthentication(code);
    }
}

package no.ntnu.sparestibackend.controller;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.standaloneSetup;

import no.ntnu.sparestibackend.dto.SavingChallengeDTO;
import no.ntnu.sparestibackend.model.SavingChallenge;
import no.ntnu.sparestibackend.service.SavingChallengeService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.MediaType;
import org.springframework.security.core.Authentication;
import org.springframework.test.web.servlet.MockMvc;

import java.util.Collections;
import java.util.List;

/**
 * Unit tests for the {@link SavingChallengeController} class.
 */
@ExtendWith(MockitoExtension.class)
public class SavingChallengeControllerTest {

    private MockMvc mockMvc;

    @Mock
    private SavingChallengeService savingChallengeService;

    @InjectMocks
    private SavingChallengeController savingChallengeController;

    private SavingChallengeDTO testChallenge;

    @BeforeEach
    public void setUp() {
        mockMvc = standaloneSetup(savingChallengeController).build();

        testChallenge = SavingChallengeDTO.builder()
                .savingChallengeId(1L)
                .description("Save for a bike")
                .currentSpending(0.0)
                .targetSpending(1000.0)
                .build();
    }

    /**
     * Tests the creation of a new saving challenge.
     *
     * @throws Exception if an error occurs during the test
     */
    @Test
    public void testCreateSavingChallenge() throws Exception {
        Authentication authentication = mock(Authentication.class);
        when(authentication.getPrincipal()).thenReturn("1");
        given(savingChallengeService.saveSavingChallenge(eq(1L), any(SavingChallengeDTO.class))).willReturn(testChallenge);

        mockMvc.perform(post("/saving-challenges")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content("{\"description\":\"Save for a bike\",\"currentSpending\":0,\"targetSpending\":1000}")
                        .principal(authentication))
                .andExpect(status().isCreated())
                .andExpect(jsonPath("$.savingChallengeId").value(testChallenge.savingChallengeId()))
                .andExpect(jsonPath("$.description").value(testChallenge.description()));

        verify(savingChallengeService).saveSavingChallenge(eq(1L), any(SavingChallengeDTO.class));
    }

    /**
     * Tests the retrieval of all saving challenges for an authenticated user.
     *
     * @throws Exception if an error occurs during the test
     */
    @Test
    public void testGetAllSavingChallengesForAuthenticatedUser() throws Exception {
        Authentication authentication = mock(Authentication.class);
        when(authentication.getPrincipal()).thenReturn("1");
        List<SavingChallengeDTO> userChallenges = Collections.singletonList(SavingChallengeDTO.builder()
                .savingChallengeId(1L)
                .description("Save for a bike")
                .build());
        given(savingChallengeService.getAllSavingChallengesForUser(1L)).willReturn(userChallenges);

        mockMvc.perform(get("/saving-challenges")
                        .principal(authentication))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$[0].savingChallengeId").value(testChallenge.savingChallengeId()))
                .andExpect(jsonPath("$[0].description").value("Save for a bike"));

        verify(savingChallengeService).getAllSavingChallengesForUser(1L);
    }


    /**
     * Tests the retrieval of a saving challenge by its ID.
     *
     * @throws Exception if an error occurs during the test
     */
    @Test
    public void testGetSavingChallengeById() throws Exception {
        Authentication authentication = mock(Authentication.class);
        when(authentication.getPrincipal()).thenReturn("1");
        given(savingChallengeService.getSavingChallengeById(testChallenge.savingChallengeId())).willReturn(testChallenge);

        mockMvc.perform(get("/saving-challenges/{id}", testChallenge.savingChallengeId())
                        .principal(authentication))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.savingChallengeId").value(testChallenge.savingChallengeId()))
                .andExpect(jsonPath("$.description").value("Save for a bike"));

        verify(savingChallengeService).getSavingChallengeById(testChallenge.savingChallengeId());
    }

    /**
     * Tests the update of an existing saving challenge.
     *
     * @throws Exception if an error occurs during the test
     */
    @Test
    public void testUpdateSavingChallenge() throws Exception {
        given(savingChallengeService.updateSavingChallenge(eq(testChallenge.savingChallengeId()), any(SavingChallengeDTO.class))).willReturn(testChallenge);

        mockMvc.perform(put("/saving-challenges/{id}", testChallenge.savingChallengeId())
                        .contentType(MediaType.APPLICATION_JSON)
                        .content("{\"description\":\"Save for a bike\",\"currentSpending\":0,\"targetSpending\":1000}"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.savingChallengeId").exists())
                .andExpect(jsonPath("$.description").value("Save for a bike"));

        verify(savingChallengeService).updateSavingChallenge(eq(testChallenge.savingChallengeId()), any(SavingChallengeDTO.class));
    }

    /**
     * Tests the deletion of a saving challenge.
     *
     * @throws Exception if an error occurs during the test
     */
    @Test
    public void testDeleteSavingChallenge() throws Exception {
        doNothing().when(savingChallengeService).deleteSavingChallenge(testChallenge.savingChallengeId());

        mockMvc.perform(delete("/saving-challenges/{id}", testChallenge.savingChallengeId()))
                .andExpect(status().isOk());

        verify(savingChallengeService).deleteSavingChallenge(testChallenge.savingChallengeId());
    }

    /**
     * Test case for linking a saving challenge to a saving goal.
     *
     * @throws Exception if an error occurs during the test
     */
    @Test
    public void testLinkSavingChallengeToGoal() throws Exception {
        String requestBody = "{\"description\":\"Save more!\",\"currentSpending\":1000,\"targetSpending\":2000,\"savingGoalId\":1}";

        SavingChallenge linkedChallenge = new SavingChallenge();
        when(savingChallengeService.linkChallengeToGoal(eq(1L), eq(1L))).thenReturn(linkedChallenge);

        mockMvc.perform(put("/saving-challenges/{id}/link-goal", 1)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(requestBody))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.savingChallengeId").value(linkedChallenge.getSavingChallengeId()))
                .andExpect(jsonPath("$.description").value(linkedChallenge.getDescription()));

        verify(savingChallengeService).linkChallengeToGoal(eq(1L), eq(1L));
    }

}

package no.ntnu.sparestibackend.controller;

import no.ntnu.sparestibackend.model.Category;
import no.ntnu.sparestibackend.model.User;
import no.ntnu.sparestibackend.service.CategoryService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.Authentication;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.standaloneSetup;

@ExtendWith(MockitoExtension.class)
public class CategoryControllerTest {

    @Mock
    private CategoryService categoryService;

    @Mock
    private Authentication authentication;

    @InjectMocks
    private CategoryController categoryController;

    private MockMvc mockMvc;

    @BeforeEach
    void setUp() {
        mockMvc = standaloneSetup(categoryController).build();
        when(authentication.getPrincipal()).thenReturn("1");
    }


    @Test
    void testGetMyCategories() throws Exception {
        User user = new User();
        user.setUserId(1L);

        Category category = new Category();
        category.setCategoryId(1L);
        category.setName("Category Name");
        category.setUser(user);

        when(categoryService.getMyCategories(1L)).thenReturn(List.of(category));

        mockMvc.perform(get("/categories").principal(authentication))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$[0].name").value("Category Name"));
    }

    @Test
    void testDeleteCategory_CategoryExistsAndBelongsToUser_ShouldReturnOkStatus() throws Exception {
        long categoryId = 1L;
        when(authentication.getPrincipal()).thenReturn("1");

        doNothing().when(categoryService).deleteCategory(categoryId, 1L);

        mockMvc.perform(delete("/categories/" + categoryId).principal(authentication))
                .andExpect(status().isOk());

        verify(categoryService).deleteCategory(categoryId, 1L);
    }

    @Test
    void testDeleteCategory_CategoryDoesNotExistOrNotBelongsToUser_ShouldReturnNotFoundStatus() throws Exception {
        long categoryId = 1L;
        when(authentication.getPrincipal()).thenReturn("1");

        doThrow(new ResponseStatusException(HttpStatus.NOT_FOUND, "Category not found or not belong to the user"))
                .when(categoryService).deleteCategory(categoryId, 1L);

        mockMvc.perform(delete("/categories/" + categoryId).principal(authentication))
                .andExpect(status().isNotFound())
                .andExpect(result -> assertTrue(result.getResolvedException() instanceof ResponseStatusException))
                .andExpect(result -> assertEquals("404 NOT_FOUND \"Category not found or not belong to the user\"",
                        result.getResolvedException().getMessage()));

        verify(categoryService).deleteCategory(categoryId, 1L);
    }
}

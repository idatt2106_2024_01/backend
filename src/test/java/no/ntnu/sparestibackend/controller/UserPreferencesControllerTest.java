package no.ntnu.sparestibackend.controller;

import no.ntnu.sparestibackend.dto.UserPreferencesDTO;
import no.ntnu.sparestibackend.model.IntensityLevel;
import no.ntnu.sparestibackend.model.UserPreferences;
import no.ntnu.sparestibackend.service.UserPreferencesService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;

import java.util.Set;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.*;

class UserPreferencesControllerTest {

    @Mock
    private UserPreferencesService userPreferencesService;

    @InjectMocks
    private UserPreferencesController userPreferencesController;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    void testUpdateUserPreferences() {
        Long userPreferencesId = 1L;
        Set<Long> savingCategoryIds = Set.of(1L, 2L);

        UserPreferencesDTO userPreferencesDTO = UserPreferencesDTO.builder()
                .userPreferencesId(userPreferencesId)
                .savingLevel(IntensityLevel.SOME)
                .willingnessToInvest(IntensityLevel.A_LOT)
                .theme("dark")
                .savingCategoryIds(savingCategoryIds)
                .build();

        UserPreferences userPreferences = new UserPreferences();
        when(userPreferencesService.updateUserPreferences(eq(userPreferencesId), any(UserPreferencesDTO.class)))
                .thenReturn(userPreferences);

        Authentication authentication = mock(Authentication.class);
        when(authentication.getPrincipal()).thenReturn("1");
        ResponseEntity<UserPreferences> response = userPreferencesController.updateUserPreferences(authentication, userPreferencesDTO);

        assertEquals(ResponseEntity.ok(userPreferences), response);
        verify(userPreferencesService).updateUserPreferences(eq(userPreferencesId), any(UserPreferencesDTO.class));
    }

    @Test
    void testAddCategory() {
        Long userPreferencesId = 1L;
        Long categoryId = 2L;
        UserPreferences userPreferences = new UserPreferences();
        when(userPreferencesService.addCategoryToUserPreferences(eq(userPreferencesId), eq(categoryId)))
                .thenReturn(userPreferences);

        ResponseEntity<UserPreferences> response = userPreferencesController.addCategory(
                userPreferencesId,
                categoryId
        );

        assertEquals(ResponseEntity.ok(userPreferences), response);
        verify(userPreferencesService).addCategoryToUserPreferences(eq(userPreferencesId), eq(categoryId));
    }
}

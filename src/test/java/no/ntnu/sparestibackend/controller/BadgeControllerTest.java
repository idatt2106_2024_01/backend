package no.ntnu.sparestibackend.controller;

import no.ntnu.sparestibackend.service.BadgeService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.security.core.Authentication;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * Unit tests for the {@link BadgeController} class.
 */
@ExtendWith(MockitoExtension.class)
public class BadgeControllerTest {

    private MockMvc mockMvc;

    @Mock
    private BadgeService badgeService;

    @InjectMocks
    private BadgeController badgeController;

    @BeforeEach
    public void setUp() {
        mockMvc = MockMvcBuilders.standaloneSetup(badgeController).build();
    }

    /**
     * Tests the badge evaluation endpoint.
     *
     * @throws Exception if an error occurs during the test
     */
    @Test
    public void testEvaluateAndAssignBadges() throws Exception {
        Authentication authentication = mock(Authentication.class);
        when(authentication.getPrincipal()).thenReturn("1");

        doNothing().when(badgeService).evaluateAndAssignBadges(1L);

        mockMvc.perform(get("/badges/evaluate")
                        .principal(authentication))
                .andExpect(status().isOk())
                .andExpect(content().string("Badge evaluation and assignment process completed successfully."));

        verify(badgeService).evaluateAndAssignBadges(1L);
    }
}


package no.ntnu.sparestibackend.controller;

import static no.ntnu.sparestibackend.model.Difficulty.HARD;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.*;

import no.ntnu.sparestibackend.dto.UserBadgeDTO;
import no.ntnu.sparestibackend.dto.UserInfoDTO;
import no.ntnu.sparestibackend.model.Status;
import no.ntnu.sparestibackend.model.User;
import no.ntnu.sparestibackend.service.SavingChallengeService;
import no.ntnu.sparestibackend.service.SavingGoalService;
import no.ntnu.sparestibackend.service.UserService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.MediaType;
import org.springframework.security.core.Authentication;
import org.springframework.test.web.servlet.MockMvc;

import java.util.Collections;
import java.util.List;

@ExtendWith(MockitoExtension.class)
public class UserControllerTest {
    @Mock
    private UserService userService;

    @Mock
    private Authentication authentication;

    @Mock
    private SavingGoalService savingGoalService;

    @Mock
    private SavingChallengeService savingChallengeService;

    @InjectMocks
    private UserController userController;

    private MockMvc mockMvc;

    @BeforeEach
    void setUp() {
        mockMvc = standaloneSetup(userController).build();
        when(authentication.getPrincipal()).thenReturn("1");
    }

    @Test
    void testGetLoggedInUser() throws Exception {
        UserInfoDTO userInfoDTO = UserInfoDTO.builder()
                .userId(1L)
                .email("user@example.com")
                .firstName("First")
                .lastName("Last")
                .bankAccounts(Collections.emptyList())
                .build();
        when(userService.getUserInfoDTOByUserId(1L)).thenReturn(userInfoDTO);

        mockMvc.perform(get("/users/me").principal(authentication))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.userId").value(1L))
                .andExpect(jsonPath("$.email").value("user@example.com"));
    }

    @Test
    void testUpdateLoggedInUser() throws Exception {
        UserInfoDTO userInfoDTO = UserInfoDTO.builder()
                .userId(1L)
                .email("updated@example.com")
                .firstName("UpdatedFirst")
                .lastName("UpdatedLast")
                .bankAccounts(Collections.emptyList())
                .build();
        String requestBody = "{ \"userId\": 1, \"email\": \"updated@example.com\", \"firstName\": \"UpdatedFirst\", \"lastName\": \"UpdatedLast\" }";

        when(userService.updateUser(eq(1L), any(UserInfoDTO.class))).thenReturn(new User());

        mockMvc.perform(put("/users")
                        .principal(authentication)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(requestBody))
                .andExpect(status().isOk());
    }

    @Test
    void testGetAllBadges() throws Exception {
        UserBadgeDTO badgeDTO = new UserBadgeDTO(1L, "Achiever", "Awarded for achieving something great", true, null);
        when(userService.getAllBadges(1L)).thenReturn(List.of(badgeDTO));

        mockMvc.perform(get("/users/badges/all").principal(authentication))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$[0].badgeId").value(1L))
                .andExpect(jsonPath("$[0].badgeName").value("Achiever"));
    }

    @Test
    void testGetAchievedBadges() throws Exception {
        UserBadgeDTO badgeDTO = new UserBadgeDTO(1L, "Achiever", "Awarded for achieving something great", true, null);
        when(userService.getAchievedBadges(1L)).thenReturn(List.of(badgeDTO));

        mockMvc.perform(get("/users/badges/achieved").principal(authentication))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$[0].achieved").value(true));
    }

    @Test
    void testUpdateProfileAvatar_Success() throws Exception {
        String profileAvatarUrl = "http://example.com/new-avatar.png";
        mockMvc.perform(patch("/users/me/profile-avatar")
                        .principal(authentication)
                        .contentType(MediaType.TEXT_PLAIN)
                        .content(profileAvatarUrl))
                .andExpect(status().isOk())
                .andExpect(content().string("Profile Avatar updated successfully"));

        verify(userService).updateProfileAvatar(1L, profileAvatarUrl);
    }

    @Test
    void testUpdateProfileAvatar_UserNotFound() throws Exception {
        String profileAvatarUrl = "http://example.com/new-avatar.png";
        doThrow(new RuntimeException("User not found")).when(userService).updateProfileAvatar(1L, profileAvatarUrl);

        mockMvc.perform(patch("/users/me/profile-avatar")
                        .principal(authentication)
                        .contentType(MediaType.TEXT_PLAIN)
                        .content(profileAvatarUrl))
                .andExpect(status().isNotFound())
                .andExpect(content().string("User not found"));

        verify(userService).updateProfileAvatar(1L, profileAvatarUrl);
    }


    @Test
    void testUpdateNewsletterConsent_Success() throws Exception {
        mockMvc.perform(patch("/users/me/newsletter-consent")
                        .principal(authentication)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content("true"))
                .andExpect(status().isOk())
                .andExpect(content().string("Newsletter consent updated successfully"));

        verify(userService).updateNewsletterConsent(1L, true);
    }

    @Test
    void testUpdateNewsletterConsent_UserNotFound() throws Exception {
        doThrow(new RuntimeException("User not found")).when(userService).updateNewsletterConsent(1L, true);

        mockMvc.perform(patch("/users/me/newsletter-consent")
                        .principal(authentication)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content("true"))
                .andExpect(status().isNotFound());

        verify(userService).updateNewsletterConsent(1L, true);
    }

    @Test
    void testGetUserStats() throws Exception {
        when(savingGoalService.getTotalSavedForAllSavingGoals(anyLong())).thenReturn(5500.0);
        when(savingChallengeService.getCompletedChallengesCount(anyLong(), any(), any())).thenReturn(3);
        when(savingChallengeService.calculateLongestSuccessStreak(anyLong())).thenReturn(5);

        mockMvc.perform(get("/users/stats").principal(authentication))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.totalSavings").value(5500.0))
                .andExpect(jsonPath("$.hardChallengesCompleted").value(3))
                .andExpect(jsonPath("$.successfulStreak").value(5));
        verify(savingGoalService).getTotalSavedForAllSavingGoals(1L);
        verify(savingChallengeService).getCompletedChallengesCount(1L, HARD, Status.COMPLETED);
        verify(savingChallengeService).calculateLongestSuccessStreak(1L);

    }
}

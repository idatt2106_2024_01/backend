package no.ntnu.sparestibackend.mapper;

import no.ntnu.sparestibackend.dto.TransactionDTO;
import no.ntnu.sparestibackend.model.BankAccount;
import no.ntnu.sparestibackend.model.Category;
import no.ntnu.sparestibackend.model.Transaction;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mapstruct.factory.Mappers;

import java.util.Arrays;
import java.util.Date;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

public class TransactionMapperTest {

    private TransactionMapper mapper;

    @BeforeEach
    void setUp() {
        mapper = Mappers.getMapper(TransactionMapper.class);
    }

    @Test
    void testTransactionToTransactionDTO() {
        Category category = new Category();
        category.setCategoryId(1L);
        BankAccount bankAccount = new BankAccount();
        bankAccount.setAccountNumber(123456789L);

        Transaction transaction = new Transaction();
        transaction.setTransactionId(1L);
        transaction.setCategory(category);
        transaction.setDate(new Date());
        transaction.setDescription("Groceries");
        transaction.setBankAccount(bankAccount);
        transaction.setSum(200.0);

        TransactionDTO dto = mapper.transactionToTransactionDTO(transaction);

        assertNotNull(dto);
        assertEquals(transaction.getTransactionId(), dto.transactionId());
        assertEquals(transaction.getCategory().getCategoryId(), dto.categoryId());
        assertEquals(transaction.getDate(), dto.date());
        assertEquals(transaction.getDescription(), dto.description());
        assertEquals(transaction.getBankAccount().getAccountNumber(), dto.accountNumber());
        assertEquals(transaction.getSum(), dto.sum());
    }

    @Test
    void testTransactionsToTransactionDTOs() {
        Category category = new Category();
        category.setCategoryId(1L);
        BankAccount bankAccount = new BankAccount();
        bankAccount.setAccountNumber(123456789L);

        Transaction transaction1 = new Transaction();
        transaction1.setTransactionId(1L);
        transaction1.setCategory(category);
        transaction1.setDate(new Date());
        transaction1.setDescription("Groceries");
        transaction1.setBankAccount(bankAccount);
        transaction1.setSum(200.0);

        Transaction transaction2 = new Transaction();
        transaction2.setTransactionId(2L);
        transaction2.setCategory(category);
        transaction2.setDate(new Date());
        transaction2.setDescription("Utilities");
        transaction2.setBankAccount(bankAccount);
        transaction2.setSum(300.0);

        List<Transaction> transactions = Arrays.asList(transaction1, transaction2);

        List<TransactionDTO> dtos = mapper.transactionsToTransactionDTOs(transactions);

        assertNotNull(dtos);
        assertEquals(2, dtos.size());
        assertTrue(dtos.stream().anyMatch(dto -> dto.transactionId().equals(transaction1.getTransactionId()) && dto.sum() == 200.0));
        assertTrue(dtos.stream().anyMatch(dto -> dto.transactionId().equals(transaction2.getTransactionId()) && dto.sum() == 300.0));
    }
}

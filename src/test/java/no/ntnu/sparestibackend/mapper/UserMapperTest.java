package no.ntnu.sparestibackend.mapper;

import no.ntnu.sparestibackend.dto.UserInfoDTO;
import no.ntnu.sparestibackend.model.User;
import no.ntnu.sparestibackend.model.UserPreferences;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mapstruct.factory.Mappers;

import java.util.Collections;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

public class UserMapperTest {

    private UserMapper mapper;

    @BeforeEach
    void setUp() {
        mapper = Mappers.getMapper(UserMapper.class);
    }

    @Test
    void testUserToUserInfoDTO() {
        User user = new User();
        user.setUserId(1L);
        user.setEmail("test@example.com");
        user.setFirstName("John");
        user.setLastName("Doe");
        user.setBankAccounts(Collections.emptyList());
        user.setUserPreferences(new UserPreferences());
        user.setOnboarded(true);

        UserInfoDTO dto = mapper.userToUserInfoDTO(user);

        assertNotNull(dto);
        assertEquals(user.getUserId(), dto.userId());
        assertEquals(user.getEmail(), dto.email());
        assertEquals(user.getFirstName(), dto.firstName());
        assertEquals(user.getLastName(), dto.lastName());
        assertEquals(user.getBankAccounts(), dto.bankAccounts());
        assertEquals(user.getUserPreferences(), dto.userPreferences());
        assertEquals(user.isOnboarded(), dto.isOnboarded());
    }
}

package no.ntnu.sparestibackend.mapper;

import no.ntnu.sparestibackend.dto.SavingChallengeDTO;
import no.ntnu.sparestibackend.model.*;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mapstruct.factory.Mappers;

import java.util.Date;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

public class SavingChallengeMapperTest {

    private SavingChallengeMapper mapper;

    @BeforeEach
    void setUp() {
        mapper = Mappers.getMapper(SavingChallengeMapper.class);
    }

    @Test
    void testToSavingChallengeDTO() {
        SavingChallenge challenge = getSavingChallenge();

        SavingChallengeDTO dto = mapper.toSavingChallengeDTO(challenge);

        assertNotNull(dto);
        assertEquals(challenge.getSavingChallengeId(), dto.savingChallengeId());
        assertEquals(challenge.getDescription(), dto.description());
        assertEquals(challenge.getCurrentSpending(), dto.currentSpending());
        assertEquals(challenge.getTargetSpending(), dto.targetSpending());
        assertEquals(challenge.getStartDate(), dto.startDate());
        assertEquals(challenge.getEndDate(), dto.endDate());
        assertEquals(challenge.getStatus(), dto.status());
        assertEquals(challenge.getDifficulty(), dto.difficulty());
        assertEquals(challenge.getCategory().getCategoryId(), dto.categoryId());
        assertEquals(challenge.getSavingGoal().getSavingGoalId(), dto.savingGoalId());
    }

    private static SavingChallenge getSavingChallenge() {
        Category category = new Category();
        category.setCategoryId(1L);
        SavingGoal savingGoal = new SavingGoal();
        savingGoal.setSavingGoalId(2L);

        SavingChallenge challenge = new SavingChallenge();
        challenge.setSavingChallengeId(1L);
        challenge.setDescription("Challenge Test");
        challenge.setCurrentSpending(100.0);
        challenge.setTargetSpending(200.0);
        challenge.setStartDate(new Date());
        challenge.setEndDate(new Date());
        challenge.setStatus(Status.ACTIVE);
        challenge.setDifficulty(Difficulty.EASY);
        challenge.setCategory(category);
        challenge.setSavingGoal(savingGoal);
        return challenge;
    }

    @Test
    void testToSavingChallengeDTOWithSpentSoFar() {
        SavingChallenge challenge = getChallenge();

        SavingChallengeDTO dto = mapper.toSavingChallengeDTOWithSpentSoFar(challenge, 150.0);

        assertNotNull(dto);
        assertEquals(150.0, dto.spentSoFar());
        assertEquals(challenge.getDescription(), dto.description());
        assertEquals(challenge.getCurrentSpending(), dto.currentSpending());
        assertEquals(challenge.getTargetSpending(), dto.targetSpending());
        assertEquals(challenge.getStartDate(), dto.startDate());
        assertEquals(challenge.getEndDate(), dto.endDate());
        assertEquals(challenge.getStatus(), dto.status());
        assertEquals(challenge.getDifficulty(), dto.difficulty());
        assertEquals(challenge.getCategory().getCategoryId(), dto.categoryId());
        assertEquals(challenge.getSavingGoal().getSavingGoalId(), dto.savingGoalId());
    }

    private static SavingChallenge getChallenge() {
        Category category = new Category();
        category.setCategoryId(1L);
        SavingGoal savingGoal = new SavingGoal();
        savingGoal.setSavingGoalId(2L);

        SavingChallenge challenge = new SavingChallenge();
        challenge.setSavingChallengeId(1L);
        challenge.setDescription("Challenge Test");
        challenge.setCurrentSpending(100.0);
        challenge.setTargetSpending(200.0);
        challenge.setStartDate(new Date());
        challenge.setEndDate(new Date());
        challenge.setStatus(Status.ACTIVE);
        challenge.setDifficulty(Difficulty.EASY);
        challenge.setCategory(category);
        challenge.setSavingGoal(savingGoal);
        return challenge;
    }
}

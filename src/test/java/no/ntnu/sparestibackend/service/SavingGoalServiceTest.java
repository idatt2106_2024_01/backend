package no.ntnu.sparestibackend.service;

import no.ntnu.sparestibackend.dto.SavingGoalDTO;
import no.ntnu.sparestibackend.model.*;
import no.ntnu.sparestibackend.repository.SavingGoalRepository;
import no.ntnu.sparestibackend.repository.TransactionRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class SavingGoalServiceTest {
    @Mock
    private SavingGoalRepository savingGoalRepository;

    @Mock
    private TransactionRepository transactionRepository;

    @Mock
    private UserService userService;

    @InjectMocks
    private SavingGoalService savingGoalService;

    private final long userId = 123L;
    private SavingGoalDTO savingGoalDTO;

    @BeforeEach
    public void setUp() {
        savingGoalDTO = new SavingGoalDTO(1L, "Holiday", 10000,
                new java.util.Date(), Status.ACTIVE);
    }

    @Test
    public void testCreateSavingGoal() {
        assertNotNull(userService, "UserService should not be null");
        assertNotNull(savingGoalRepository, "SavingGoalRepository should not be null");

        User user = new User();
        when(userService.getUserByUserId(userId)).thenReturn(user);
        when(savingGoalRepository.save(any(SavingGoal.class))).thenAnswer(invocation -> invocation.getArgument(0));

        SavingGoal createdGoal = savingGoalService.createSavingGoal(userId, savingGoalDTO);

        assertNotNull(createdGoal, "The created saving goal should not be null");
        assertEquals(savingGoalDTO.title(), createdGoal.getTitle(), "Title should match the DTO");
        verify(userService).getUserByUserId(userId);
        verify(savingGoalRepository).save(any(SavingGoal.class));
    }

    @Test
    public void testGetTotalSavedForSavingGoal() {
        BankAccount bankAccount = new BankAccount();
        User user = new User();
        long savingGoalId = 1;
        Category category = new Category();
        category.setName("Food");
        SavingGoal savingGoal = SavingGoal.builder()
                .savingGoalId(savingGoalId)
                .goal(500)
                .user(user)
                .title("Test")
                .deadline(new Date())
                .build();

        Transaction transaction1 = new Transaction();
        transaction1.setTransactionId(1L);
        transaction1.setBankAccount(bankAccount);
        transaction1.setCategory(category);
        transaction1.setDate(new Date());
        transaction1.setDescription("Buying cheaper food");
        transaction1.setSum(100.0);
        transaction1.setSavingGoal(savingGoal);

        Transaction transaction2 = new Transaction();
        transaction2.setTransactionId(2L);
        transaction2.setBankAccount(bankAccount);
        transaction2.setCategory(category);
        transaction2.setDate(new Date());
        transaction2.setDescription("Buying even cheaper food");
        transaction2.setSum(200.0);
        transaction2.setSavingGoal(savingGoal);

        List<Transaction> transactions = Arrays.asList(transaction1, transaction2);
        when(transactionRepository.findBySavingGoal_SavingGoalId(savingGoalId)).thenReturn(transactions);

        double totalSaved = savingGoalService.getTotalSavedForSavingGoal(savingGoalId);

        assertEquals(300.0, totalSaved, "Total saved should be the sum of transaction sums");
        verify(transactionRepository).findBySavingGoal_SavingGoalId(savingGoalId);
    }

    @Test
    void updateSavingGoal_WhenUserOwnsGoal_ShouldUpdateAndReturnGoal() {
        long userId = 1L;
        long savingGoalId = 1L;
        User user = new User();
        user.setUserId(userId);
        SavingGoal existingGoal = new SavingGoal();
        existingGoal.setUser(user);
        existingGoal.setSavingGoalId(savingGoalId);
        SavingGoalDTO savingGoalDTO = new SavingGoalDTO(savingGoalId, "New Title", 2000, new Date(),
                Status.COMPLETED);

        when(savingGoalRepository.findById(savingGoalId)).thenReturn(Optional.of(existingGoal));
        when(savingGoalRepository.save(any(SavingGoal.class))).thenAnswer(invocation -> invocation.getArgument(0));

        SavingGoal updatedGoal = savingGoalService.updateSavingGoal(userId, savingGoalId, savingGoalDTO);

        assertNotNull(updatedGoal);
        assertEquals(savingGoalDTO.title(), updatedGoal.getTitle());
        assertEquals(savingGoalDTO.goal(), updatedGoal.getGoal());
        assertEquals(savingGoalDTO.deadline(), updatedGoal.getDeadline());
        verify(savingGoalRepository).save(existingGoal);
    }

    @Test
    void updateSavingGoal_WhenUserDoesNotOwnGoal_ShouldReturnNull() {
        long savingGoalId = 1L;
        User differentUser = new User();
        differentUser.setUserId(2L);
        SavingGoal existingGoal = new SavingGoal();
        existingGoal.setUser(differentUser);
        existingGoal.setSavingGoalId(savingGoalId);
        SavingGoalDTO savingGoalDTO = new SavingGoalDTO(savingGoalId, "New Title", 2000, new Date(),
                Status.FAILED);

        when(savingGoalRepository.findById(savingGoalId)).thenReturn(Optional.of(existingGoal));

        SavingGoal result = savingGoalService.updateSavingGoal(userId, savingGoalId, savingGoalDTO);

        assertNull(result);
        verify(savingGoalRepository, never()).save(any(SavingGoal.class));
    }

    @Test
    void deleteSavingGoal_WhenUserOwnsGoal_ShouldDeleteAndReturnTrue() {
        long userId = 1L;
        long savingGoalId = 1L;
        User user = new User();
        user.setUserId(userId);
        SavingGoal existingGoal = new SavingGoal();
        existingGoal.setUser(user);
        existingGoal.setSavingGoalId(savingGoalId);

        when(savingGoalRepository.findById(savingGoalId)).thenReturn(Optional.of(existingGoal));

        boolean result = savingGoalService.deleteSavingGoal(userId, savingGoalId);

        assertTrue(result);
        verify(savingGoalRepository).deleteById(savingGoalId);
    }

    @Test
    void deleteSavingGoal_WhenUserDoesNotOwnGoal_ShouldNotDeleteAndReturnFalse() {
        long savingGoalId = 1L;
        User differentUser = new User();
        differentUser.setUserId(2L);
        SavingGoal existingGoal = new SavingGoal();
        existingGoal.setUser(differentUser);
        existingGoal.setSavingGoalId(savingGoalId);

        when(savingGoalRepository.findById(savingGoalId)).thenReturn(Optional.of(existingGoal));

        boolean result = savingGoalService.deleteSavingGoal(userId, savingGoalId);

        assertFalse(result);
        verify(savingGoalRepository, never()).deleteById(savingGoalId);
    }

    @Test
    public void getAllSavingGoalsForUser_ShouldReturnAllGoalsForUser() {

        SavingGoal savinggoal1 = new SavingGoal();
        savinggoal1.setSavingGoalId(3L);
        savinggoal1.setTitle("Test goal");
        savinggoal1.setGoal(1000);
        savinggoal1.setDeadline(new Date());
        savinggoal1.setUser(new User());
        savinggoal1.setStatus(Status.ACTIVE);

        SavingGoal savinggoal2 = new SavingGoal();
        savinggoal2.setSavingGoalId(4L);
        savinggoal2.setTitle("Test goal 2");
        savinggoal2.setGoal(3000);
        savinggoal2.setDeadline(new Date());
        savinggoal2.setUser(new User());
        savinggoal2.setStatus(Status.ACTIVE);

        List<SavingGoal> expectedGoals = Arrays.asList(savinggoal1, savinggoal2);

        when(savingGoalRepository.findSavingGoalsByUserUserId(userId)).thenReturn(expectedGoals);

        List<SavingGoal> actualGoals = savingGoalService.getAllSavingGoalsForUser(userId);

        assertNotNull(actualGoals, "Returned list of goals should not be null");
        assertEquals(expectedGoals.size(), actualGoals.size(), "Should return correct number of saving goals");
        assertTrue(actualGoals.containsAll(expectedGoals), "Returned saving goals should match the expected ones");
        verify(savingGoalRepository).findSavingGoalsByUserUserId(userId);
    }

    @Test
    public void getTotalSavedForAllSavingGoals_ShouldCalculateTotalAcrossAllGoals() {
        long userId = 1L;
        SavingGoal savingGoal1 = new SavingGoal();
        savingGoal1.setSavingGoalId(1L);
        savingGoal1.setTitle("Vacation");
        savingGoal1.setGoal(5000);
        savingGoal1.setDeadline(new Date());
        savingGoal1.setUser(new User());
        savingGoal1.setStatus(Status.ACTIVE);

        SavingGoal savingGoal2 = new SavingGoal();
        savingGoal2.setSavingGoalId(2L);
        savingGoal2.setTitle("Car");
        savingGoal2.setGoal(15000);
        savingGoal2.setDeadline(new Date());
        savingGoal2.setUser(new User());
        savingGoal2.setStatus(Status.ACTIVE);

        List<SavingGoal> savingGoals = Arrays.asList(savingGoal1, savingGoal2);

        Transaction transaction1 = new Transaction();
        transaction1.setTransactionId(1L);
        transaction1.setBankAccount(new BankAccount());
        transaction1.setCategory(new Category());
        transaction1.setDate(new Date());
        transaction1.setDescription("Deposit 1");
        transaction1.setSum(2000.0);
        transaction1.setSavingGoal(savingGoals.get(0));

        Transaction transaction2 = new Transaction();
        transaction2.setTransactionId(1L);
        transaction2.setBankAccount(new BankAccount());
        transaction2.setCategory(new Category());
        transaction2.setDate(new Date());
        transaction2.setDescription("Deposit 2");
        transaction2.setSum(1000.0);
        transaction2.setSavingGoal(savingGoals.get(0));

        List<Transaction> transactionsForFirstGoal = Arrays.asList(transaction1, transaction2);

        Transaction transaction3 = new Transaction();
        transaction3.setTransactionId(1L);
        transaction3.setBankAccount(new BankAccount());
        transaction3.setCategory(new Category());
        transaction3.setDate(new Date());
        transaction3.setDescription("Deposit 3");
        transaction3.setSum(5000.0);
        transaction3.setSavingGoal(savingGoals.get(1));

        Transaction transaction4 = new Transaction();
        transaction4.setTransactionId(1L);
        transaction4.setBankAccount(new BankAccount());
        transaction4.setCategory(new Category());
        transaction4.setDate(new Date());
        transaction4.setDescription("Deposit 4");
        transaction4.setSum(7000.0);
        transaction4.setSavingGoal(savingGoals.get(1));

        List<Transaction> transactionsForSecondGoal = Arrays.asList(transaction3, transaction4);

        when(savingGoalRepository.findSavingGoalsByUserUserId(userId)).thenReturn(savingGoals);
        when(transactionRepository.findBySavingGoal_SavingGoalId(savingGoals.get(0).getSavingGoalId()))
                .thenReturn(transactionsForFirstGoal);
        when(transactionRepository.findBySavingGoal_SavingGoalId(savingGoals.get(1).getSavingGoalId()))
                .thenReturn(transactionsForSecondGoal);

        double totalSaved = savingGoalService.getTotalSavedForAllSavingGoals(userId);

        assertEquals(15000.0, totalSaved,
                "Total saved should be the sum of all transaction sums for all saving goals");
        verify(savingGoalRepository).findSavingGoalsByUserUserId(userId);
        verify(transactionRepository).findBySavingGoal_SavingGoalId(savingGoals.get(0).getSavingGoalId());
        verify(transactionRepository).findBySavingGoal_SavingGoalId(savingGoals.get(1).getSavingGoalId());
    }

    @Test
    public void getSavingGoalsByStatusForUser_ShouldFilterGoalsByStatus() {
        SavingGoal savingGoal1 = new SavingGoal();
        savingGoal1.setSavingGoalId(1L);
        savingGoal1.setTitle("Goal 1");
        savingGoal1.setGoal(5000);
        savingGoal1.setDeadline(new Date());
        savingGoal1.setUser(new User());
        savingGoal1.setStatus(Status.ACTIVE);

        SavingGoal savingGoal2 = new SavingGoal();
        savingGoal2.setSavingGoalId(2L);
        savingGoal2.setTitle("Goal 2");
        savingGoal2.setGoal(3000);
        savingGoal2.setDeadline(new Date());
        savingGoal2.setUser(new User());
        savingGoal2.setStatus(Status.COMPLETED);

        SavingGoal savingGoal3 = new SavingGoal();
        savingGoal3.setSavingGoalId(3L);
        savingGoal3.setTitle("Goal 3");
        savingGoal3.setGoal(7000);
        savingGoal3.setDeadline(new Date());
        savingGoal3.setUser(new User());
        savingGoal3.setStatus(Status.ACTIVE);

        List<SavingGoal> allGoals = Arrays.asList(savingGoal1, savingGoal2, savingGoal3);

        when(savingGoalRepository.findSavingGoalsByUserUserIdAndStatus(userId, Status.ACTIVE))
                .thenReturn(allGoals.stream().filter(goal -> goal.getStatus() == Status.ACTIVE).toList());

        List<SavingGoal> activeGoals = savingGoalService.getAllSavingGoalsForUser(userId, Status.ACTIVE);
        System.out.println(activeGoals);
        assertNotNull(activeGoals, "Filtered list should not be null");
        assertEquals(2, activeGoals.size(), "Should return only active goals");
        assertTrue(activeGoals.stream().allMatch(goal -> goal.getStatus() == Status.ACTIVE),
                "All returned goals should have the ACTIVE status");
        verify(savingGoalRepository).findSavingGoalsByUserUserIdAndStatus(userId, Status.ACTIVE);
    }
}

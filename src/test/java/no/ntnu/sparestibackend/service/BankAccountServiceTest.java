package no.ntnu.sparestibackend.service;

import jakarta.persistence.EntityManager;
import no.ntnu.sparestibackend.dto.BankAccountDTO;
import no.ntnu.sparestibackend.model.AccountType;
import no.ntnu.sparestibackend.model.BankAccount;
import no.ntnu.sparestibackend.model.User;
import no.ntnu.sparestibackend.repository.BankAccountRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.web.server.ResponseStatusException;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class BankAccountServiceTest {
    @Mock
    private EntityManager entityManager;
    @Mock
    private BankAccountRepository bankAccountRepository;
    @InjectMocks
    private BankAccountService bankAccountService;

    private BankAccount bankAccount;
    private BankAccount bankAccount2;

    private BankAccountDTO bankAccountDTO;
    private User user;

    @BeforeEach
    void setUp() {
        bankAccount = new BankAccount();
        bankAccount.setAccountNumber(12345L);
        bankAccount.setName("Savings Account");
        bankAccount.setAccountType(AccountType.SAVINGS);

        bankAccount2 = new BankAccount();
        bankAccount2.setAccountNumber(12345L);
        bankAccount2.setName("Current Account");
        bankAccount2.setAccountType(AccountType.CHECKING);


        bankAccountDTO = BankAccountDTO.builder()
                .accountNumber(123L)
                .name("Current Account")
                .accountType(AccountType.CHECKING)
                .build();

        user = new User();
        user.setUserId(1L);
        user.getBankAccounts().add(bankAccount);
    }

    @Test
    void testGetBankAccountByAccountNumber_Found() {
        when(bankAccountRepository.findByAccountNumber(anyLong())).thenReturn(bankAccount);

        BankAccount result = bankAccountService.getBankAccountByAccountNumber(12345L);

        assertNotNull(result);
        assertEquals(bankAccount.getName(), result.getName());
    }

    @Test
    void testUpdateBankAccount_Success() {
        when(bankAccountRepository.findByAccountNumber(anyLong())).thenReturn(bankAccount);
        when(bankAccountRepository.save(any(BankAccount.class))).thenReturn(bankAccount);

        BankAccountDTO result = bankAccountService.updateBankAccount(12345L, bankAccountDTO);

        assertNotNull(result);
        assertEquals(bankAccountDTO.name(), result.name());
        verify(bankAccountRepository).save(bankAccount);
    }

    @Test
    void testUpdateBankAccount_NotFound() {
        when(bankAccountRepository.findByAccountNumber(anyLong())).thenReturn(null);

        assertThrows(ResponseStatusException.class, () -> bankAccountService.updateBankAccount(12345L, bankAccountDTO));
    }
}


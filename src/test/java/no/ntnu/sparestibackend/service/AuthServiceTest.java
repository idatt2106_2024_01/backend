package no.ntnu.sparestibackend.service;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.web.reactive.function.client.WebClient;
import org.springframework.web.reactive.function.client.WebClient.ResponseSpec;
import org.springframework.web.reactive.function.client.WebClientResponseException;
import reactor.core.publisher.Mono;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.*;

/**
 * Test class for AuthService.
 */
@ExtendWith(MockitoExtension.class)
public class AuthServiceTest {
    @Mock
    private WebClient webClient;
    @Mock
    private ResponseSpec responseSpec;

    @InjectMocks
    private AuthService authService;

    /**
     * Setup method to mock WebClient behavior.
     */
    @BeforeEach
    void setUp() {
        WebClient.RequestBodyUriSpec requestBodyUriSpec = mock(WebClient.RequestBodyUriSpec.class);
        WebClient.RequestBodySpec requestBodySpec = mock(WebClient.RequestBodySpec.class);
        ResponseSpec responseSpec = mock(ResponseSpec.class);

        lenient().when(webClient.post()).thenReturn(requestBodyUriSpec);
        lenient().when(requestBodyUriSpec.uri(anyString())).thenReturn(requestBodySpec);
        lenient().when(requestBodySpec.header(anyString(), anyString())).thenReturn(requestBodySpec);
        lenient().when(requestBodySpec.retrieve()).thenReturn(responseSpec);
        lenient().when(responseSpec.bodyToMono(String.class)).thenReturn(Mono.just("response string"));
    }

    /**
     * Test case to check if createBankIdAuthenticationURL method returns a valid URL.
     *
     * @throws Exception if an error occurs
     */
    @Test
    void createBankIdAuthenticationURL_ShouldReturnURL() throws Exception {
        String url = authService.createBankIdAuthenticationURL(null);
        assertTrue(url.startsWith("https://spare-sti.sandbox.signicat.com"));
    }

    /**
     * Test case to verify the handling of errors during BankID authentication.
     */
    @Test
    void loginOrRegisterUserFromBankIdAuthentication_ShouldHandleErrors() {
        String code = "invalidCode";
        lenient().when(responseSpec.bodyToMono(String.class))
                .thenReturn(Mono.error(new WebClientResponseException(400, "Bad Request", null, null, null, null)));

        assertThrows(WebClientResponseException.class, () ->
                authService.loginOrRegisterUserFromBankIdAuthentication(code));
    }
}

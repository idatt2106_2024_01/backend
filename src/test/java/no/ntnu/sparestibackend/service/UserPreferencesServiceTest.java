package no.ntnu.sparestibackend.service;

import no.ntnu.sparestibackend.dto.UserPreferencesDTO;
import no.ntnu.sparestibackend.model.Category;
import no.ntnu.sparestibackend.model.IntensityLevel;
import no.ntnu.sparestibackend.model.UserPreferences;
import no.ntnu.sparestibackend.repository.CategoryRepository;
import no.ntnu.sparestibackend.repository.UserPreferencesRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class UserPreferencesServiceTest {

    @Mock
    private UserPreferencesRepository userPreferencesRepository;

    @Mock
    private CategoryRepository categoryRepository;

    @InjectMocks
    private UserPreferencesService userPreferencesService;

    private UserPreferences userPreferences;
    private UserPreferencesDTO userPreferencesDTO;

    @BeforeEach
    public void setUp() {
        userPreferences = new UserPreferences();
        userPreferences.setUserPreferencesId(1L);
        userPreferences.setSavingLevel(IntensityLevel.A_LOT);
        userPreferences.setWillingnessToInvest(IntensityLevel.SOME);

        userPreferencesDTO = new UserPreferencesDTO(
                1L,
                IntensityLevel.SOME,
                IntensityLevel.A_LOT,
                "Dark",
                Set.of(1L, 2L),
                "Nick"
        );
    }


    @Test
    public void testUpdateUserPreferences_Success() {
        when(userPreferencesRepository.findByUserUserId(1L)).thenReturn(Optional.of(userPreferences));
        when(categoryRepository.findById(anyLong())).thenReturn(Optional.of(new Category()));
        when(userPreferencesRepository.save(any(UserPreferences.class))).thenReturn(userPreferences);

        UserPreferences updatedUserPreferences = userPreferencesService.updateUserPreferences(1L, userPreferencesDTO);

        assertNotNull(updatedUserPreferences);
        assertEquals(IntensityLevel.SOME, updatedUserPreferences.getSavingLevel());
        assertEquals(IntensityLevel.A_LOT, updatedUserPreferences.getWillingnessToInvest());
        verify(userPreferencesRepository, times(1)).findByUserUserId(1L);
        verify(userPreferencesRepository, times(1)).save(any(UserPreferences.class));
    }

    @Test
    public void testUpdateUserPreferences_NotFound() {
        when(userPreferencesRepository.findByUserUserId(1L)).thenReturn(Optional.empty());

        IllegalArgumentException exception = assertThrows(IllegalArgumentException.class, () -> {
            userPreferencesService.updateUserPreferences(1L, userPreferencesDTO);
        });

        assertEquals("User preferences with user_id 1 not found", exception.getMessage());
        verify(userPreferencesRepository, times(1)).findByUserUserId(1L);
    }

    @Test
    public void testAddCategoryToUserPreferences_Success() {
        Category category = new Category();
        category.setCategoryId(1L);

        when(userPreferencesRepository.findById(1L)).thenReturn(Optional.of(userPreferences));
        when(categoryRepository.findById(1L)).thenReturn(Optional.of(category));
        when(userPreferencesRepository.save(any(UserPreferences.class))).thenReturn(userPreferences);

        UserPreferences updated = userPreferencesService.addCategoryToUserPreferences(1L, 1L);

        assertNotNull(updated);
        assertTrue(updated.getSavingCategories().contains(category));
        verify(userPreferencesRepository, times(1)).findById(1L);
        verify(categoryRepository, times(1)).findById(1L);
        verify(userPreferencesRepository, times(1)).save(any(UserPreferences.class));
    }
}

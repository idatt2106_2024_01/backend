package no.ntnu.sparestibackend.service;

import no.ntnu.sparestibackend.dto.CategoryDTO;
import no.ntnu.sparestibackend.model.Category;
import no.ntnu.sparestibackend.model.User;
import no.ntnu.sparestibackend.repository.CategoryRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

/**
 * Test class for CategoryService.
 */

@ExtendWith(MockitoExtension.class)
public class CategoryServiceTest {

    @Mock
    private CategoryRepository categoryRepository;

    @Mock
    private UserService userService;

    @InjectMocks
    private CategoryService categoryService;

    private User user;
    private Category category;

    @BeforeEach
    void setUp() {
        user = new User();
        user.setUserId(1L);

        category = new Category();
        category.setCategoryId(1L);
        category.setName("Household");
        category.setUser(user);
    }

    @Test
    public void createCategory_ShouldCreateAndReturnCategory() {
        CategoryDTO categoryDTO = CategoryDTO.builder()
                .name("Household")
                .img("household.jpg")
                .build();
        when(userService.getUserByUserId(anyLong())).thenReturn(user);
        when(categoryRepository.save(any(Category.class))).thenReturn(category);

        Category createdCategory = categoryService.createCategory(1L, categoryDTO);

        assertNotNull(createdCategory);
        assertEquals("Household", createdCategory.getName());
        verify(categoryRepository).save(any(Category.class));
    }

    @Test
    public void createCategory_ShouldThrowExceptionForDuplicateName() {
        CategoryDTO categoryDTO = CategoryDTO.builder()
                .name("Household")
                .build();
        when(userService.getUserByUserId(anyLong())).thenReturn(user);

        when(categoryRepository.existsByUserAndName(any(User.class), eq("Household"))).thenReturn(true);

        Exception exception = assertThrows(IllegalArgumentException.class, () -> categoryService.createCategory(1L, categoryDTO));

        assertTrue(exception.getMessage().contains("Category with name Household already exists"));
    }


    @Test
    public void getMyCategories_ShouldReturnUserCategories() {
        when(categoryRepository.findCategoriesByUserUserId(anyLong())).thenReturn(Arrays.asList(category));

        List<Category> categories = categoryService.getMyCategories(1L);

        assertFalse(categories.isEmpty());
        assertEquals(1, categories.size());
        verify(categoryRepository).findCategoriesByUserUserId(anyLong());
    }
}

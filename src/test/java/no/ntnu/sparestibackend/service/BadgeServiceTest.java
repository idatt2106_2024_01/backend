package no.ntnu.sparestibackend.service;

import no.ntnu.sparestibackend.model.Badge;
import no.ntnu.sparestibackend.model.Status;
import no.ntnu.sparestibackend.model.User;
import no.ntnu.sparestibackend.model.UserBadge;
import no.ntnu.sparestibackend.repository.BadgeRepository;
import no.ntnu.sparestibackend.repository.SavingChallengeRepository;
import no.ntnu.sparestibackend.repository.UserBadgeRepository;
import no.ntnu.sparestibackend.repository.UserRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static no.ntnu.sparestibackend.model.Difficulty.HARD;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

/**
 * Unit tests for the {@link BadgeService} class.
 */
@ExtendWith(MockitoExtension.class)
public class BadgeServiceTest {

    @Mock
    private BadgeRepository badgeRepository;
    @Mock
    private UserBadgeRepository userBadgeRepository;
    @Mock
    private SavingChallengeRepository savingChallengeRepository;
    @Mock
    private SavingGoalService savingGoalService;
    @Mock
    private SavingChallengeService savingChallengeService;
    @Mock
    private UserRepository userRepository;

    @InjectMocks
    private BadgeService badgeService;

    private User user;
    private List<Badge> badges;
    private List<UserBadge> userBadges;

    @BeforeEach
    void setUp() {
        user = new User();
        user.setUserId(1L);

        badges = List.of(Badge.builder().name("Spart 500 kr").build(), Badge.builder().name("1 vanskelig utfordring").build());
        userBadges = new ArrayList<>();
        lenient().when(userRepository.findByUserId(any())).thenReturn(user);
        lenient().when(badgeRepository.findAll()).thenReturn(badges);
    }

    /**
     * Tests that a badge is assigned when the criteria are met.
     */
    @Test
    void shouldAssignBadgeWhenCriteriaMet() {
        when(savingGoalService.getTotalSavedForAllSavingGoals(user.getUserId())).thenReturn(600.0);
        when(savingChallengeService.getCompletedChallengesCount(user.getUserId(), HARD, Status.COMPLETED)).thenReturn(0);
        when(savingChallengeService.calculateLongestSuccessStreak(user.getUserId())).thenReturn(0);

        badgeService.evaluateAndAssignBadges(user.getUserId());

        verify(userBadgeRepository, times(1)).save(any(UserBadge.class));
    }

    /**
     * Tests that a badge is not assigned when the criteria are not met.
     */
    @Test
    void shouldNotAssignBadgeWhenCriteriaNotMet() {
        when(savingGoalService.getTotalSavedForAllSavingGoals(user.getUserId())).thenReturn(400.0);
        when(savingChallengeService.getCompletedChallengesCount(user.getUserId(), HARD, Status.COMPLETED)).thenReturn(0);
        when(savingChallengeService.calculateLongestSuccessStreak(user.getUserId())).thenReturn(0);

        badgeService.evaluateAndAssignBadges(user.getUserId());

        verify(userBadgeRepository, never()).save(any(UserBadge.class));
    }

    /**
     * Tests that an existing badge is updated when not achieved previously.
     */
    @Test
    void shouldUpdateExistingBadgeWhenNotAchieved() {
        UserBadge existingBadge = new UserBadge();
        existingBadge.setUser(user);
        existingBadge.setBadge(badges.getFirst());
        existingBadge.setAchieved(false);
        userBadges.add(existingBadge);

        when(userBadgeRepository.findByUser_UserId(user.getUserId())).thenReturn(userBadges);
        when(savingGoalService.getTotalSavedForAllSavingGoals(user.getUserId())).thenReturn(600.0);

        badgeService.evaluateAndAssignBadges(user.getUserId());

        assertTrue(existingBadge.isAchieved());
        assertNotNull(existingBadge.getDateAchieved());
        verify(userBadgeRepository).save(existingBadge);
    }

    @Test
    void shouldAssignMultipleBadgesWhenCriteriaMet() {
        when(savingGoalService.getTotalSavedForAllSavingGoals(user.getUserId())).thenReturn(10000.0);
        when(savingChallengeService.getCompletedChallengesCount(user.getUserId(), HARD, Status.COMPLETED)).thenReturn(1);
        when(savingChallengeService.calculateLongestSuccessStreak(user.getUserId())).thenReturn(5);

        badges = List.of(
                Badge.builder().name("Spart 1000 kr").build(),
                Badge.builder().name("Spart 10 000 kr").build(),
                Badge.builder().name("1 vanskelig utfordring").build(),
                Badge.builder().name("5 utfordringer på rad").build()
        );
        when(badgeRepository.findAll()).thenReturn(badges);
        when(userBadgeRepository.findByUser_UserId(user.getUserId())).thenReturn(userBadges);

        badgeService.evaluateAndAssignBadges(user.getUserId());

        verify(userBadgeRepository, times(4)).save(any(UserBadge.class));
    }

    @Test
    void shouldNotAssignAlreadyAchievedBadges() {
        UserBadge existingBadge = new UserBadge();
        existingBadge.setUser(user);
        existingBadge.setBadge(badges.get(0));  // Assuming this is "Spart 500 kr"
        existingBadge.setAchieved(true);
        existingBadge.setDateAchieved(new Date());
        userBadges.add(existingBadge);

        when(savingGoalService.getTotalSavedForAllSavingGoals(user.getUserId())).thenReturn(600.0);
        when(userBadgeRepository.findByUser_UserId(user.getUserId())).thenReturn(userBadges);

        badgeService.evaluateAndAssignBadges(user.getUserId());

        verify(userBadgeRepository, never()).save(existingBadge);  // Make sure no save operation is called on already achieved badge
    }

    @Test
    void shouldNotUpdateDateAchievedForAlreadyAchievedBadges() {
        Date originalDate = new Date();
        UserBadge existingBadge = new UserBadge();
        existingBadge.setUser(user);
        existingBadge.setBadge(badges.get(0));
        existingBadge.setAchieved(true);
        existingBadge.setDateAchieved(originalDate);
        userBadges.add(existingBadge);

        when(savingGoalService.getTotalSavedForAllSavingGoals(user.getUserId())).thenReturn(600.0);
        when(userBadgeRepository.findByUser_UserId(user.getUserId())).thenReturn(userBadges);

        badgeService.evaluateAndAssignBadges(user.getUserId());

        assertEquals(originalDate, existingBadge.getDateAchieved());
        verify(userBadgeRepository, never()).save(existingBadge);
    }

    @Test
    void shouldHandleNoBadgesDefined() {
        when(badgeRepository.findAll()).thenReturn(List.of());
        badgeService.evaluateAndAssignBadges(user.getUserId());
        verify(userBadgeRepository, never()).save(any(UserBadge.class));
    }

    /**
     * Test method to cover all branches of checkBadgeCriteria.
     */
    @Test
    void checkBadgeCriteria_CoverAllBranches() {

        Badge spart500 = Badge.builder().name("Spart 500 kr").build();
        Badge spart1000 = Badge.builder().name("Spart 1000 kr").build();
        Badge spart10000 = Badge.builder().name("Spart 10 000 kr").build();
        Badge spart100000 = Badge.builder().name("Spart 100 000 kr").build();
        Badge hardChallenge1 = Badge.builder().name("1 vanskelig utfordring").build();
        Badge hardChallenge3 = Badge.builder().name("3 vanskelige utfordringer").build();
        Badge hardChallenge5 = Badge.builder().name("5 vanskelige utfordringer").build();
        Badge hardChallenge10 = Badge.builder().name("10 vanskelige utfordringer").build();
        Badge streak3 = Badge.builder().name("3 utfordringer på rad").build();
        Badge streak5 = Badge.builder().name("5 utfordringer på rad").build();
        Badge streak15 = Badge.builder().name("15 utfordringer på rad").build();
        Badge streak50 = Badge.builder().name("50 utfordringer på rad").build();
        Badge unknownBadge = Badge.builder().name("unknown").build();

        double totalSavings = 100500;
        int hardChallenges = 10;
        int streak = 50;

        assertTrue(badgeService.checkBadgeCriteria(spart500, 500, 0, 0));
        assertTrue(badgeService.checkBadgeCriteria(spart1000, 1000, 0, 0));
        assertTrue(badgeService.checkBadgeCriteria(spart10000, 10000, 0, 0));
        assertTrue(badgeService.checkBadgeCriteria(spart100000, totalSavings, 0, 0));

        assertTrue(badgeService.checkBadgeCriteria(hardChallenge1, 0, 1, 0));
        assertTrue(badgeService.checkBadgeCriteria(hardChallenge3, 0, 3, 0));
        assertTrue(badgeService.checkBadgeCriteria(hardChallenge5, 0, 5, 0));
        assertTrue(badgeService.checkBadgeCriteria(hardChallenge10, 0, hardChallenges, 0));

        assertTrue(badgeService.checkBadgeCriteria(streak3, 0, 0, 3));
        assertTrue(badgeService.checkBadgeCriteria(streak5, 0, 0, 5));
        assertTrue(badgeService.checkBadgeCriteria(streak15, 0, 0, 15));
        assertTrue(badgeService.checkBadgeCriteria(streak50, 0, 0, streak));

        assertFalse(badgeService.checkBadgeCriteria(unknownBadge, 0, 0, 0));

        assertFalse(badgeService.checkBadgeCriteria(spart500, 499, 0, 0));
        assertFalse(badgeService.checkBadgeCriteria(hardChallenge1, 0, 0, 0));
        assertFalse(badgeService.checkBadgeCriteria(streak3, 0, 0, 2));
    }
}


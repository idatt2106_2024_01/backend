package no.ntnu.sparestibackend.service;

import jakarta.persistence.EntityManager;
import no.ntnu.sparestibackend.dto.EditTransactionDTO;
import no.ntnu.sparestibackend.dto.TransactionDTO;
import no.ntnu.sparestibackend.dto.transactions.SavingStat;
import no.ntnu.sparestibackend.mapper.TransactionMapper;
import no.ntnu.sparestibackend.model.*;
import no.ntnu.sparestibackend.repository.BankAccountRepository;
import no.ntnu.sparestibackend.repository.TransactionRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;

import java.util.*;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

/**
 * This class tests the functionality of the {@link TransactionService} related to managing transactions within a banking application.
 * It utilizes Mockito for mocking dependencies and asserts interactions to ensure the service handles CRUD operations correctly,
 * adheres to validation rules, and respects authorization constraints.
 */
@ExtendWith(MockitoExtension.class)
public class TransactionServiceTest {
    @Mock
    private TransactionRepository transactionRepository;
    @Mock
    private EntityManager entityManager;
    @Mock
    private TransactionMapper transactionMapper;
    @Mock
    private BankAccountRepository bankAccountRepository;
    @Mock
    private CategoryService categoryService;


    @InjectMocks
    private TransactionService transactionService;

    private BankAccount bankAccount;
    private Category category;
    private Transaction transaction;
    private TransactionDTO transactionDTO;
    private long savingGoalId;
    private SavingGoal savingGoal;

    @BeforeEach
    void setUp() {
        User user = new User();
        user.setUserId(123L);

        bankAccount = new BankAccount();
        bankAccount.setAccountNumber(1L);
        bankAccount.setUser(user);

        category = new Category();
        category.setCategoryId(2L);

        savingGoal = new SavingGoal();
        savingGoal.setSavingGoalId(1L);
        savingGoal.setTitle("Emergency Fund");
        savingGoal.setGoal(1000);
        savingGoal.setDeadline(new Date());
        savingGoal.setUser(user);
        savingGoal.setStatus(Status.ACTIVE);

        transaction = new Transaction();
        transaction.setTransactionId(1L);
        transaction.setCategory(category);
        transaction.setBankAccount(bankAccount);
        transaction.setDate(new Date());
        transaction.setDescription("Groceries");
        transaction.setSum(50.00);
        transaction.setSavingGoal(savingGoal);

        transactionDTO = new TransactionDTO(1L, 2L, new Date(), "Groceries",
                50.00, 1L, savingGoal.getSavingGoalId());

        lenient().when(entityManager.find(BankAccount.class, 1L)).thenReturn(bankAccount);
        lenient().when(entityManager.find(Category.class, 2L)).thenReturn(category);
        lenient().when(entityManager.find(SavingGoal.class, 1L)).thenReturn(savingGoal);
        lenient().when(entityManager.find(SavingGoal.class, 999L)).thenReturn(null);
        Mockito.lenient().when(entityManager.find(eq(Category.class), any())).thenReturn(null);
        lenient().when(entityManager.find(eq(Category.class), anyLong())).thenReturn(null);
        lenient().when(entityManager.find(eq(SavingGoal.class), anyLong())).thenReturn(null);
    }

    /**
     * Tests the creation of a transaction ensuring that the transaction is successfully created
     * and saved when valid input parameters are passed.
     */
    @Test
    public void createTransaction_ShouldCreateAndReturnTransaction() {
        when(entityManager.find(BankAccount.class, 1L)).thenReturn(bankAccount);
        when(entityManager.find(Category.class, 2L)).thenReturn(category);
        when(entityManager.find(SavingGoal.class, 1L)).thenReturn(savingGoal);

        when(transactionRepository.save(any(Transaction.class))).thenReturn(transaction);

        Transaction createdTransaction = transactionService.createTransaction(1L, transactionDTO);

        assertNotNull(createdTransaction);
        assertEquals("Groceries", createdTransaction.getDescription());
        verify(transactionRepository).save(any(Transaction.class));
    }

    /**
     * Verifies that all transactions associated with a specific bank account number are correctly fetched
     * and transformed to their DTO representations.
     */
    @Test
    public void getTransactionsByBankAccountNumber_ShouldReturnTransactionDTOs() {
        int page = 1;
        int size = 20;
        Pageable pageable = PageRequest.of(page, size);

        Transaction transaction = new Transaction();
        transaction.setDescription("Groceries");

        List<Transaction> transactionList = Collections.singletonList(transaction);
        Page<Transaction> transactionPage = new PageImpl<>(transactionList, pageable, transactionList.size());

        TransactionDTO transactionDTO = TransactionDTO.builder()
                .description("Groceries")
                .build();

        when(transactionRepository.findByBankAccountAccountNumberOrderByDateDesc(1L, pageable)).thenReturn(transactionPage);

        Page<Transaction> transactionDTOs = transactionService.getTransactionsByBankAccountNumber(1L, page, size);


        assertFalse(transactionDTOs.isEmpty());
        assertEquals(1, transactionList.size());
        assertEquals("Groceries", transactionDTOs.getContent().getFirst().getDescription());
        verify(transactionRepository).findByBankAccountAccountNumberOrderByDateDesc(1L, pageable);
    }


    /**
     * Tests the update functionality by ensuring a transaction is updated correctly when provided
     * with valid category and description changes.
     */
    @Test
    public void updateTransaction_ShouldUpdateAndReturnTransactionDTO() {
        Long transactionId = 1L;
        Long newCategoryId = 3L;
        String newDescription = "Updated Groceries";
        EditTransactionDTO editTransactionDTO = new EditTransactionDTO(newCategoryId, newDescription);

        Category newCategory = new Category();
        newCategory.setCategoryId(newCategoryId);
        transaction.setCategory(newCategory);
        transaction.setDescription(newDescription);

        TransactionDTO updatedTransactionDTO = new TransactionDTO(transactionId, newCategoryId, new Date(), newDescription, 50.00, 1L, savingGoalId);

        when(entityManager.find(Transaction.class, transactionId)).thenReturn(transaction);
        when(entityManager.find(Category.class, newCategoryId)).thenReturn(newCategory);
        when(transactionRepository.save(transaction)).thenReturn(transaction);

        Transaction result = transactionService.updateTransaction(transactionId, editTransactionDTO);

        assertNotNull(result);
        assertEquals(newDescription, result.getDescription(), "Description should match the updated value");
        assertEquals(newCategory, result.getCategory(), "Category ID should match the updated value");
        verify(transactionRepository).save(transaction);
    }

    /**
     * Verifies that the service correctly identifies when a user is the owner of a specific bank account,
     * returning true in such cases.
     */
    @Test
    void userOwnsBankAccount_ShouldReturnTrue_IfUserOwnsAccount() {
        when(bankAccountRepository.findByAccountNumber(1L)).thenReturn(bankAccount);

        boolean result = transactionService.userOwnsBankAccount(123L, 1L);

        assertTrue(result, "Should return true when user owns the account");
    }

    /**
     * Ensures that the service correctly identifies when a user does not own a specific bank account,
     * returning false in such cases.
     */
    @Test
    void userOwnsBankAccount_ShouldReturnFalse_IfUserDoesNotOwnAccount() {
        User anotherUser = new User();
        anotherUser.setUserId(999L);
        bankAccount.setUser(anotherUser);
        when(bankAccountRepository.findByAccountNumber(1L)).thenReturn(bankAccount);

        boolean result = transactionService.userOwnsBankAccount(123L, 1L);

        assertFalse(result, "Should return false when user does not own the account");
    }

    /**
     * Confirms that the service returns false when checking ownership for a non-existent bank account.
     */
    @Test
    void userOwnsBankAccount_ShouldReturnFalse_IfAccountDoesNotExist() {
        when(bankAccountRepository.findByAccountNumber(999L)).thenReturn(null);

        boolean result = transactionService.userOwnsBankAccount(123L, 999L);

        assertFalse(result, "Should return false when the account does not exist");
    }

    /**
     * Tests that updating a transaction with a non-existent category ID throws an IllegalArgumentException,
     * ensuring that the category must exist for an update operation.
     */
    @Test
    void updateTransaction_ThrowsException_IfCategoryDoesNotExist() {
        Long transactionId = 1L;
        Long invalidCategoryId = 999L;
        EditTransactionDTO editTransactionDTOWithInvalidCategory = new EditTransactionDTO(invalidCategoryId,
                "Attempt Update With Invalid Category");

        when(entityManager.find(Transaction.class, transactionId)).thenReturn(transaction);
        when(entityManager.find(Category.class, invalidCategoryId)).thenReturn(null);

        Exception exception = assertThrows(IllegalArgumentException.class, () -> transactionService
                .updateTransaction(transactionId, editTransactionDTOWithInvalidCategory));

        assertEquals("Category with ID 999 not found", exception.getMessage());
        verify(transactionRepository, never()).save(any(Transaction.class));
    }

    @Test
    void getTransactionsByUserIdCategoryIdAndDateInterval_ShouldReturnTransactions() {
        Date fromDate = new GregorianCalendar(2023, Calendar.JANUARY, 1).getTime();
        Date toDate = new GregorianCalendar(2023, Calendar.JANUARY, 31).getTime();
        Long categoryId = 2L;

        List<Transaction> expectedTransactions = Collections.singletonList(transaction);
        when(transactionRepository.findByCategory_CategoryIdAndDateBetween(categoryId, fromDate, toDate))
                .thenReturn(expectedTransactions);

        List<Transaction> result = transactionService
                .getTransactionsByUserIdCategoryIdAndDateInterval(categoryId, fromDate, toDate);

        assertNotNull(result);
        assertFalse(result.isEmpty());
        assertEquals(1, result.size());
        assertEquals(transaction, result.get(0));
        verify(transactionRepository).findByCategory_CategoryIdAndDateBetween(categoryId, fromDate, toDate);
    }


    @Test
    void populateBankAccountWithTransactions_ShouldGenerateAndSaveTransactions() {

        List<Category> categories = Collections.singletonList(category);
        when(categoryService.getMyCategories(anyLong())).thenReturn(categories);
        when(transactionRepository.saveAll(anyList())).thenReturn(null);

        transactionService.populateBankAccountWithTransactions(bankAccount);

        verify(categoryService).getMyCategories(bankAccount.getUser().getUserId());
        verify(transactionRepository).saveAll(anyList());
    }

    @Test
    void getSavingsTransactionsForLastYearByEachMonth_ShouldReturnCorrectStats() {
        BankAccount savingsAccount = new BankAccount();
        savingsAccount.setAccountNumber(2L);
        savingsAccount.setUser(bankAccount.getUser());

        List<Transaction> transactions = Collections.singletonList(transaction); // Reuse the transaction with a SavingsGoal

        when(bankAccountRepository.findByUserUserIdAndAccountType(anyLong(), eq(AccountType.SAVINGS)))
                .thenReturn(savingsAccount);
        when(transactionRepository.findByBankAccount_AccountNumberAndDateBetween(anyLong(), any(), any())).thenReturn(transactions);

        HashMap<String, List<SavingStat>> result = transactionService
                .getSavingsTransactionsForLastYearByEachMonth(bankAccount.getUser().getUserId());

        assertNotNull(result);
        assertFalse(result.isEmpty());
        assertTrue(result.containsKey("all"));
        assertTrue(result.containsKey("onlyFromSavingGoal"));
        verify(bankAccountRepository).findByUserUserIdAndAccountType(bankAccount.getUser().getUserId(), AccountType.SAVINGS);
        verify(transactionRepository)
                .findByBankAccount_AccountNumberAndDateBetween(eq(savingsAccount.getAccountNumber()), any(), any());
    }

}

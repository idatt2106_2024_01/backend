package no.ntnu.sparestibackend.service;

import no.ntnu.sparestibackend.dto.UserBadgeDTO;
import no.ntnu.sparestibackend.dto.UserInfoDTO;
import no.ntnu.sparestibackend.mapper.UserMapper;
import no.ntnu.sparestibackend.model.Badge;
import no.ntnu.sparestibackend.model.User;
import no.ntnu.sparestibackend.model.UserBadge;
import no.ntnu.sparestibackend.repository.UserBadgeRepository;
import no.ntnu.sparestibackend.repository.UserRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class UserServiceTest {
    @Mock
    private UserRepository userRepository;

    @Mock
    private UserMapper userMapper;

    @Mock
    private UserBadgeRepository userBadgeRepository;

    @InjectMocks
    private UserService userService;

    private User user;
    private UserInfoDTO userInfoDTO;
    private UserBadge userBadge;
    private UserBadge userBadge2;
    private Badge badge;
    private Badge badge2;
    private final Date dateAchieved = new Date();

    @BeforeEach
    void setUp() {
        user = new User();
        user.setUserId(1L);
        user.setEmail("example@example.com");
        user.setFirstName("First");
        user.setLastName("Last");
        user.setUserBadges(new HashSet<>());

        badge = new Badge();
        badge.setBadgeId(1L);
        badge.setName("Achiever");
        badge.setDescription("Awarded for achieving something great");

        userBadge = new UserBadge();
        userBadge.setId(1L);
        userBadge.setBadge(badge);
        userBadge.setAchieved(true);
        userBadge.setDateAchieved(dateAchieved);

        badge2 = new Badge();
        badge2.setBadgeId(2L);
        badge2.setName("winner");
        badge2.setDescription("Awarded for winning something great");

        userBadge2 = new UserBadge();
        userBadge2.setId(2L);
        userBadge2.setBadge(badge2);
        userBadge2.setAchieved(false);
        userBadge2.setDateAchieved(null);

        user.getUserBadges().add(userBadge);
        user.getUserBadges().add(userBadge2);

        userInfoDTO = UserInfoDTO.builder()
                .userId(1L)
                .email("updated@example.com")
                .firstName("UpdatedFirst")
                .lastName("UpdatedLast")
                .bankAccounts(new ArrayList<>())
                .build();
    }

    @Test
    void testGetUserInfoByUserId_ShouldReturnUserInfoDTO() {
        when(userMapper.userToUserInfoDTO(any(User.class))).thenReturn(userInfoDTO);
        when(userRepository.findByUserId(1L)).thenReturn(user);

        UserInfoDTO result = userService.getUserInfoDTOByUserId(1L);

        assertNotNull(result);
        assertEquals(userInfoDTO.email(), result.email());
        assertEquals(userInfoDTO.firstName(), result.firstName());
        assertEquals(userInfoDTO.lastName(), result.lastName());
        verify(userRepository).findByUserId(1L);
    }

    @Test
    void testUpdateUser_ShouldUpdateUserDetails() {
        when(userRepository.findByUserId(1L)).thenReturn(user);
        when(userRepository.save(user)).thenReturn(user);

        User updatedUser = userService.updateUser(1L, userInfoDTO);

        assertNotNull(updatedUser);
        assertEquals(userInfoDTO.email(), updatedUser.getEmail());
        assertEquals(userInfoDTO.firstName(), updatedUser.getFirstName());
        assertEquals(userInfoDTO.lastName(), updatedUser.getLastName());
        verify(userRepository).findByUserId(1L);
        verify(userRepository).save(user);
    }

    @Test
    void testUpdateUser_WhenUserDoesNotExist_ShouldReturnNull() {
        when(userRepository.findByUserId(2L)).thenReturn(null);

        User result = userService.updateUser(2L, userInfoDTO);

        assertNull(result);
        verify(userRepository).findByUserId(2L);
        verify(userRepository, never()).save(any(User.class));
    }

    @Test
    void testGetAllBadges_ShouldReturnAllUserBadges() {
        when(userBadgeRepository.findByUser_UserId(1L))
                .thenReturn(new ArrayList<>(user.getUserBadges()));

        List<UserBadgeDTO> result = userService.getAllBadges(1L);

        assertNotNull(result);
        assertFalse(result.isEmpty());
        assertEquals(2, result.size());
        assertTrue(result.stream().anyMatch(dto -> dto.getBadgeId().equals(badge.getBadgeId()) && dto.isAchieved()));
        assertTrue(result.stream().anyMatch(dto -> dto.getBadgeId().equals(badge2.getBadgeId()) && !dto.isAchieved()));
        verify(userBadgeRepository).findByUser_UserId(1L);
    }

    @Test
    void testGetAllBadges_WhenNoUserBadges_ShouldReturnEmptyList() {
        when(userBadgeRepository.findByUser_UserId(1L))
                .thenReturn(new ArrayList<>());

        List<UserBadgeDTO> result = userService.getAllBadges(1L);

        assertNotNull(result);
        assertTrue(result.isEmpty());
        verify(userBadgeRepository).findByUser_UserId(1L);
    }

    @Test
    void testGetAchievedBadges_ShouldFilterAchievedBadges() {
        when(userBadgeRepository.findByUser_UserId(1L))
                .thenReturn(new ArrayList<>(user.getUserBadges()));

        List<UserBadgeDTO> result = userService.getAchievedBadges(1L);

        assertNotNull(result);
        assertEquals(1, result.size());
        UserBadgeDTO dto = result.getFirst();
        assertEquals(badge.getBadgeId(), dto.getBadgeId());
        assertTrue(dto.isAchieved());
        assertEquals(dateAchieved, dto.getDateAchieved());
        verify(userBadgeRepository).findByUser_UserId(1L);
    }

    @Test
    void testUpdateNewsletterConsent_UserExists_ShouldUpdateConsent() {
        when(userRepository.findByUserId(1L)).thenReturn(user);
        when(userRepository.save(any(User.class))).thenReturn(user);

        userService.updateNewsletterConsent(1L, true);

        assertTrue(user.isNewsletterConsent());
        verify(userRepository).save(user);
    }

    @Test
    void testUpdateNewsletterConsent_UserNotFound_ShouldThrowException() {
        when(userRepository.findByUserId(1L)).thenReturn(null);

        Exception exception = assertThrows(RuntimeException.class, () -> {
            userService.updateNewsletterConsent(1L, true);
        });

        assertEquals("User not found", exception.getMessage());
        verify(userRepository, never()).save(any(User.class));
    }

    @Test
    void testUpdateProfileAvatar_UserExists_ShouldUpdateAvatar() {
        when(userRepository.findByUserId(1L)).thenReturn(user);
        when(userRepository.save(any(User.class))).thenReturn(user);

        String newAvatarUrl = "http://example.com/new-avatar.png";
        userService.updateProfileAvatar(1L, newAvatarUrl);

        assertEquals(newAvatarUrl, user.getProfileAvatar());
        verify(userRepository).save(user);
    }

    @Test
    void testUpdateProfileAvatar_UserNotFound_ShouldThrowException() {
        when(userRepository.findByUserId(1L)).thenReturn(null);

        Exception exception = assertThrows(RuntimeException.class, () -> {
            userService.updateProfileAvatar(1L, "http://example.com/new-avatar.png");
        });

        assertEquals("User not found", exception.getMessage());
        verify(userRepository, never()).save(any(User.class));
    }

}


package no.ntnu.sparestibackend.service;

import jakarta.persistence.EntityManager;
import no.ntnu.sparestibackend.dto.SavingChallengeDTO;
import no.ntnu.sparestibackend.mapper.SavingChallengeMapper;
import no.ntnu.sparestibackend.model.*;
import no.ntnu.sparestibackend.repository.SavingChallengeRepository;
import no.ntnu.sparestibackend.repository.SavingGoalRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.*;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class SavingChallengeServiceTest {
    @Mock
    private EntityManager entityManager;
    @Mock
    private SavingChallengeRepository challengeRepository;
    @Mock
    private SavingGoalRepository goalRepository;
    @Mock
    private SavingChallengeMapper savingChallengeMapper;
    @Mock
    private TransactionService transactionService;

    @InjectMocks
    private SavingChallengeService service;

    @BeforeEach
    void setup() {
        Mockito.lenient().when(transactionService.getTransactionsByUserIdCategoryIdAndDateInterval(anyLong(), any(), any()))
                .thenReturn(Collections.emptyList());

        Mockito.lenient().when(savingChallengeMapper.toSavingChallengeDTO(any(SavingChallenge.class)))
                .thenAnswer(invocation -> {
                    SavingChallenge challenge = invocation.getArgument(0);
                    return new SavingChallengeDTO(
                            challenge.getSavingChallengeId(),
                            challenge.getDescription(),
                            challenge.getCurrentSpending(),
                            challenge.getTargetSpending(),
                            0.0,
                            challenge.getStartDate(),
                            challenge.getEndDate(),
                            challenge.getStatus(),
                            challenge.getDifficulty(),
                            challenge.getCategory().getCategoryId(),
                            challenge.getSavingGoal() != null ? challenge.getSavingGoal().getSavingGoalId() : null,
                            challenge.getCategory()
                    );
                });
    }


    private SavingChallengeDTO createSavingChallengeDTO() {
        return new SavingChallengeDTO(
                null,
                "Save more!",
                1000.0,
                2000.0,
                0.0,
                new Date(),
                new Date(),
                Status.ACTIVE,
                Difficulty.EASY,
                1L,
                1L,
                new Category()
        );
    }

    /**
     * Test case for saving a new saving challenge.
     */
    @Test
    void testSaveSavingChallenge() {
        SavingChallengeDTO dto = createSavingChallengeDTO();
        SavingGoal savingGoal = new SavingGoal();
        savingGoal.setSavingGoalId(dto.savingGoalId());
        User user = new User();
        user.setUserId(1L);
        Category category = new Category();
        category.setCategoryId(dto.categoryId());

        when(entityManager.find(User.class, 1L)).thenReturn(user);
        when(entityManager.find(Category.class, dto.categoryId())).thenReturn(category);
        when(entityManager.find(SavingGoal.class, dto.savingGoalId())).thenReturn(savingGoal);
        when(challengeRepository.save(any(SavingChallenge.class))).thenAnswer(invocation -> invocation.getArgument(0));

        SavingChallengeDTO savedChallenge = service.saveSavingChallenge(1L, dto);

        assertNotNull(savedChallenge);
        assertEquals(dto.description(), savedChallenge.description());
        assertEquals(dto.categoryId(), savedChallenge.categoryId());
        assertEquals(dto.savingGoalId(), savedChallenge.savingGoalId());
        verify(challengeRepository).save(any(SavingChallenge.class));
    }

    /**
     * Test case for retrieving a saving challenge by its ID.
     */
    @Test
    void testGetSavingChallengeById() {
        Long challengeId = 1L;
        SavingChallenge challenge = new SavingChallenge();
        challenge.setSavingChallengeId(challengeId);
        Category category = new Category();
        category.setCategoryId(1L);
        challenge.setCategory(category);

        when(challengeRepository.findById(challengeId)).thenReturn(Optional.of(challenge));

        SavingChallengeDTO result = service.getSavingChallengeById(challengeId);

        assertNotNull(result);
        verify(challengeRepository).findById(challengeId);
    }


    @Test
    void testGetSavingChallengeById_NotFound() {
        when(challengeRepository.findById(1L)).thenReturn(Optional.empty());

        assertThrows(RuntimeException.class, () -> service.getSavingChallengeById(1L));
        verify(challengeRepository).findById(1L);
    }


    /**
     * Test case for deleting a saving challenge.
     */
    @Test
    void testDeleteSavingChallenge() {
        doNothing().when(challengeRepository).deleteById(1L);

        service.deleteSavingChallenge(1L);

        verify(challengeRepository).deleteById(1L);
    }

    /**
     * Test case for linking a saving challenge to a saving goal successfully.
     */
    @Test
    void testLinkChallengeToGoal_Success() {
        Long challengeId = 1L;
        Long goalId = 1L;
        SavingChallenge challenge = new SavingChallenge();
        challenge.setSavingChallengeId(challengeId);
        SavingGoal goal = new SavingGoal();
        goal.setSavingGoalId(goalId);

        when(challengeRepository.findById(challengeId)).thenReturn(Optional.of(challenge));
        when(goalRepository.findById(goalId)).thenReturn(Optional.of(goal));
        when(challengeRepository.save(any(SavingChallenge.class))).thenAnswer(invocation -> invocation.getArgument(0));

        SavingChallenge updatedChallenge = service.linkChallengeToGoal(challengeId, goalId);

        assertNotNull(updatedChallenge);
        assertEquals(goal, updatedChallenge.getSavingGoal(), "The goal should be linked correctly");
        verify(challengeRepository).save(challenge);
        verify(goalRepository).findById(goalId);
        verify(challengeRepository).findById(challengeId);
    }

    /**
     * Test case for handling the scenario where the saving challenge is not found.
     */
    @Test
    void testLinkChallengeToGoal_ChallengeNotFound() {
        Long challengeId = 1L;
        Long goalId = 1L;

        assertThrows(RuntimeException.class, () -> service.linkChallengeToGoal(challengeId, goalId),
                "Expected exception for not finding challenge");
        verify(goalRepository, never()).save(any(SavingGoal.class));
    }

    /**
     * Test case for handling the scenario where the saving goal is not found.
     */
    @Test
    void testLinkChallengeToGoal_GoalNotFound() {
        Long challengeId = 1L;
        Long goalId = 1L;
        SavingChallenge challenge = new SavingChallenge();
        challenge.setSavingChallengeId(challengeId);

        when(challengeRepository.findById(challengeId)).thenReturn(Optional.of(challenge));
        when(goalRepository.findById(goalId)).thenReturn(Optional.empty());

        assertThrows(RuntimeException.class, () -> service.linkChallengeToGoal(challengeId, goalId),
                "Expected exception for not finding goal");
        verify(challengeRepository, never()).save(any(SavingChallenge.class));
    }

    /**
     * Tests the calculation of the longest success streak for a user.
     */
    @Test
    void testCalculateLongestSuccessStreak() {
        Long userId = 1L;

        SavingChallenge challenge1 = new SavingChallenge();
        challenge1.setSavingChallengeId(1L);
        challenge1.setStatus(Status.COMPLETED);

        SavingChallenge challenge2 = new SavingChallenge();
        challenge2.setSavingChallengeId(2L);
        challenge2.setStatus(Status.COMPLETED);

        SavingChallenge challenge3 = new SavingChallenge();
        challenge3.setSavingChallengeId(3L);
        challenge3.setStatus(Status.FAILED);

        List<SavingChallenge> challenges = Arrays.asList(challenge1, challenge2, challenge3);

        when(challengeRepository.findByUserUserIdOrderByEndDateAsc(userId)).thenReturn(challenges);

        int result = service.calculateLongestSuccessStreak(userId);

        assertEquals(2, result, "The longest streak should be two");
        verify(challengeRepository).findByUserUserIdOrderByEndDateAsc(userId);
    }

    /**
     * Tests the retrieval of the count of completed challenges for a user with a specific difficulty.
     */
    @Test
    void testGetCompletedChallengesCount() {
        Long userId = 1L;
        User user = new User();
        user.setUserId(userId);
        Difficulty difficulty = Difficulty.HARD;
        Long challengeId = 1L;
        SavingChallenge challenge = new SavingChallenge();
        challenge.setSavingChallengeId(challengeId);
        challenge.setStatus(Status.COMPLETED);
        challenge.setDifficulty(difficulty);
        challenge.setUser(user);
        Long challengeId2 = 2L;
        SavingChallenge challenge2 = new SavingChallenge();
        challenge2.setSavingChallengeId(challengeId2);
        challenge2.setStatus(Status.COMPLETED);
        challenge2.setDifficulty(difficulty);
        challenge2.setUser(user);

        Long challengeId3 = 3L;
        SavingChallenge challenge3 = new SavingChallenge();
        challenge3.setSavingChallengeId(challengeId3);
        challenge3.setStatus(Status.FAILED);
        challenge3.setDifficulty(difficulty);
        challenge3.setUser(user);

        List<SavingChallenge> completedChallenges = Arrays.asList(challenge, challenge2);

        when(challengeRepository.findSavingChallengesByDifficultyAndUserUserIdAndStatus(
                difficulty, userId, Status.COMPLETED)).thenReturn(completedChallenges);

        int result = service.getCompletedChallengesCount(userId, difficulty, Status.COMPLETED);

        assertEquals(2, result, "Only two challenges should be counted as completed");
        verify(challengeRepository).findSavingChallengesByDifficultyAndUserUserIdAndStatus(difficulty, userId,
                Status.COMPLETED);
    }
}
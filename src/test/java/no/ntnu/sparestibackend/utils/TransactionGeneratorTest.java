package no.ntnu.sparestibackend.utils;

import no.ntnu.sparestibackend.model.AccountType;
import no.ntnu.sparestibackend.model.BankAccount;
import no.ntnu.sparestibackend.model.Category;
import no.ntnu.sparestibackend.model.Transaction;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class TransactionGeneratorTest {

    @Test
    public void generateRandomTransactions() {
        // Act
        BankAccount bankAccount = BankAccount.builder()
                .accountNumber(1234L)
                .name("Checkings account")
                .accountType(AccountType.CHECKING)
                .build();
        Category[] categories = {
                Category.builder()
                        .name("Kaffe")
                        .img("/path/to/image")
                        .build(),
                Category.builder()
                        .name("Frisør")
                        .img("/path/to/image")
                        .build(),
                Category.builder()
                        .name("Takeaway")
                        .img("/path/to/image")
                        .build(),
                Category.builder()
                        .name("Telefon")
                        .img("/path/to/image")
                        .build(),
                Category.builder()
                        .name("Gaming")
                        .img("/path/to/image")
                        .build(),
        };
        List<Transaction> transactions = TransactionGenerator.generateRandomTransactions(bankAccount, List.of(categories));

        // Assert
        assertEquals(50, transactions.size());

        for (Transaction transaction : transactions) {
            long diff = System.currentTimeMillis() - transaction.getDate().getTime();
            assertTrue(diff >= 0 && diff <= 30L * 24 * 60 * 60 * 1000);
        }

        for (Transaction transaction : transactions) {
            boolean found = false;
            for (Category category : categories) {
                if (transaction.getDescription().contains(category.getName())) {
                    found = true;
                    break;
                }
            }
            assertTrue(found);
        }
    }
}

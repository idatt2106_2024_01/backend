package no.ntnu.sparestibackend.utils;

import no.ntnu.sparestibackend.model.Difficulty;
import no.ntnu.sparestibackend.model.IntensityLevel;
import no.ntnu.sparestibackend.model.User;
import no.ntnu.sparestibackend.model.UserPreferences;
import org.junit.jupiter.api.RepeatedTest;

import static org.junit.jupiter.api.Assertions.assertNotNull;

public class DifficultyGeneratorTest {


    @RepeatedTest(100)
    public void testGenerate3RandomDifficulty() {
        User user = User.builder()
                .userId(1L)
                .firstName("First")
                .lastName("Last")
                .build();
        UserPreferences userPreferences = UserPreferences.builder()
                .willingnessToInvest(IntensityLevel.A_LITTLE)
                .user(user)
                .build();

        DifficultyGenerator difficultyGenerator = new DifficultyGenerator(userPreferences, 3);
        Difficulty difficulty1 = difficultyGenerator.getNextDifficulty();
        Difficulty difficulty2 = difficultyGenerator.getNextDifficulty();
        Difficulty difficulty3 = difficultyGenerator.getNextDifficulty();

        assertNotNull(difficulty1);
        assertNotNull(difficulty2);
        assertNotNull(difficulty3);

        assertNotNull(difficultyGenerator.generatedDifficulties.stream()
                .filter(diff -> diff.equals(Difficulty.EASY))
                .findFirst()
                .orElse(null));
    }
}

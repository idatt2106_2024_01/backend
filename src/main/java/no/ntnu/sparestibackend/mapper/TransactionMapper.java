package no.ntnu.sparestibackend.mapper;

import no.ntnu.sparestibackend.dto.TransactionDTO;
import no.ntnu.sparestibackend.model.Transaction;
import org.mapstruct.Mapper;

import java.util.List;
import java.util.stream.Collectors;

/**
 * Mapper for the Transaction entity.
 */
@Mapper(componentModel = "spring")
public interface TransactionMapper {

    /**
     * Maps a Transaction entity to a TransactionDTO.
     *
     * @param transaction The Transaction entity to be mapped.
     * @return A TransactionDTO mapped from the Transaction entity.
     */
    default TransactionDTO transactionToTransactionDTO(Transaction transaction) {
        return TransactionDTO.builder()
                .transactionId(transaction.getTransactionId())
                .categoryId(transaction.getCategory().getCategoryId())
                .date(transaction.getDate())
                .description(transaction.getDescription())
                .accountNumber(transaction.getBankAccount().getAccountNumber())
                .sum(transaction.getSum())
                .build();
    }

    /**
     * Maps a list of Transaction entities to a list of TransactionDTOs.
     *
     * @param transactions The list of Transaction entities to be mapped.
     * @return A list of TransactionDTOs mapped from the list of Transaction entities.
     */
    default List<TransactionDTO> transactionsToTransactionDTOs(List<Transaction> transactions) {
        return transactions.stream()
                .map(this::transactionToTransactionDTO)
                .collect(Collectors.toList());
    }
}

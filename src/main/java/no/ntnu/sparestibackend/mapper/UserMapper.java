package no.ntnu.sparestibackend.mapper;

import no.ntnu.sparestibackend.dto.UserInfoDTO;
import no.ntnu.sparestibackend.model.User;
import org.mapstruct.Mapper;

/**
 * Mapper for converting between User entities and UserInfoDTO objects.
 * Utilizes MapStruct for automatic mapping of properties.
 */
@Mapper(componentModel = "spring")
public interface UserMapper {

    /**
     * Converts a User entity to a UserInfoDTO.
     *
     * @param user The User entity to convert.
     * @return A UserInfoDTO containing data from the User entity.
     */
    default UserInfoDTO userToUserInfoDTO(User user) {
        return UserInfoDTO.builder()
                .userId(user.getUserId())
                .email(user.getEmail())
                .firstName(user.getFirstName())
                .lastName(user.getLastName())
                .bankAccounts(user.getBankAccounts())
                .userPreferences(user.getUserPreferences())
                .profileAvatar(user.getProfileAvatar())
                .newsletterConsent(user.isNewsletterConsent())
                .isOnboarded(user.isOnboarded())
                .build();
    }
}
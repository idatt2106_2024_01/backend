package no.ntnu.sparestibackend.mapper;

import no.ntnu.sparestibackend.dto.SavingChallengeDTO;
import no.ntnu.sparestibackend.model.SavingChallenge;
import org.mapstruct.Mapper;

/**
 * Mapper interface for mapping between SavingChallenge and SavingChallengeDTO.
 */
@Mapper(componentModel = "spring")
public interface SavingChallengeMapper {

    /**
     * Maps a SavingChallenge entity to a SavingChallengeDTO.
     *
     * @param savingChallenge The SavingChallenge entity to be mapped.
     * @return A SavingChallengeDTO mapped from the SavingChallenge entity.
     */
    default SavingChallengeDTO toSavingChallengeDTO(SavingChallenge savingChallenge) {
        Long savingGoalId = savingChallenge.getSavingGoal() != null ? savingChallenge.getSavingGoal().getSavingGoalId() : null;
        return SavingChallengeDTO.builder()
                .savingChallengeId(savingChallenge.getSavingChallengeId())
                .description(savingChallenge.getDescription())
                .currentSpending(savingChallenge.getCurrentSpending())
                .targetSpending(savingChallenge.getTargetSpending())
                .startDate(savingChallenge.getStartDate())
                .endDate(savingChallenge.getEndDate())
                .status(savingChallenge.getStatus())
                .difficulty(savingChallenge.getDifficulty())
                .savingGoalId(savingGoalId)
                .categoryId(savingChallenge.getCategory().getCategoryId())
                .category(savingChallenge.getCategory())
                .build();
    }

    /**
     * Maps a SavingChallenge entity to a SavingChallengeDTO with spentSoFar included.
     *
     * @param savingChallenge The SavingChallenge entity to be mapped.
     * @param spentSoFar      The amount spent so far.
     * @return A SavingChallengeDTO mapped from the SavingChallenge entity with spentSoFar included.
     */
    default SavingChallengeDTO toSavingChallengeDTOWithSpentSoFar(SavingChallenge savingChallenge, double spentSoFar) {
        Long savingGoalId = savingChallenge.getSavingGoal() != null ? savingChallenge.getSavingGoal().getSavingGoalId() : null;
        return SavingChallengeDTO.builder()
                .savingChallengeId(savingChallenge.getSavingChallengeId())
                .description(savingChallenge.getDescription())
                .currentSpending(savingChallenge.getCurrentSpending())
                .targetSpending(savingChallenge.getTargetSpending())
                .startDate(savingChallenge.getStartDate())
                .endDate(savingChallenge.getEndDate())
                .status(savingChallenge.getStatus())
                .difficulty(savingChallenge.getDifficulty())
                .savingGoalId(savingGoalId)
                .categoryId(savingChallenge.getCategory().getCategoryId())
                .category(savingChallenge.getCategory())
                .spentSoFar(spentSoFar)
                .build();
    }
}

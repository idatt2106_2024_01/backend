package no.ntnu.sparestibackend.repository;

import no.ntnu.sparestibackend.model.Difficulty;
import no.ntnu.sparestibackend.model.SavingChallenge;
import no.ntnu.sparestibackend.model.Status;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;


/**
 * Spring Data repository interface for {@link SavingChallenge} entities.
 * Extends {@link JpaRepository} to provide basic CRUD operations on the SavingChallenge database table.
 * Utilizes Spring Data JPA to reduce boilerplate code necessary to implement data access layers for applications.
 *
 * @see JpaRepository
 */
public interface SavingChallengeRepository extends JpaRepository<SavingChallenge, Long> {

    /**
     * Finds saving challenges for a specific user.
     *
     * @param userId the ID of the user
     * @return a list of saving challenges for the user
     */
    List<SavingChallenge> findByUserUserId(Long userId);

    /**
     * Finds saving challenges for a specific user, ordered by end date in ascending order.
     *
     * @param userId the ID of the user
     * @return a list of saving challenges for the user, ordered by end date in ascending order
     */
    List<SavingChallenge> findByUserUserIdOrderByEndDateAsc(Long userId);

    /**
     * Finds saving challenges by difficulty, user ID, and status.
     *
     * @param difficulty the difficulty level of the saving challenges
     * @param userId     the ID of the user
     * @param status     the status of the saving challenges
     * @return a list of saving challenges matching the given difficulty, user ID, and status
     */
    List<SavingChallenge> findSavingChallengesByDifficultyAndUserUserIdAndStatus(Difficulty difficulty, Long userId,
                                                                                 Status status);
}

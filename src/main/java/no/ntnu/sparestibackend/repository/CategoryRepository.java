package no.ntnu.sparestibackend.repository;

import no.ntnu.sparestibackend.model.Category;
import no.ntnu.sparestibackend.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Repository for the category entity.
 */
@Repository
public interface CategoryRepository extends JpaRepository<Category, Long> {

    /**
     * Finds all categories associated with a particular user.
     *
     * @param userId The userID is representing the user whose categories are to be retrieved.
     * @return A list of Category entities that belong to the specified userId.
     */
    List<Category> findCategoriesByUserUserId(Long userId);

    /**
     * Checks if a category with a specific name already exists for a given user.
     *
     * @param user The user object to check for category existence.
     * @param name The name of the category to check.
     * @return true if a category with the specified name exists for the given user, false otherwise.
     */
    boolean existsByUserAndName(User user, String name);

    /**
     * Checks if a category with a specific ID is associated with a given user ID.
     *
     * @param categoryId The ID of the category to check.
     * @param userId     The ID of the user to check against the category.
     * @return true if the category is associated with the specified user ID, false otherwise.
     */
    boolean existsByCategoryIdAndUserUserId(Long categoryId, Long userId);
}

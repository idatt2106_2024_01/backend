package no.ntnu.sparestibackend.repository;

import no.ntnu.sparestibackend.model.Transaction;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Date;
import java.util.List;

/**
 * Provides access to the Transaction database table, extending the JpaRepository
 * for basic CRUD operations and supports pagination.
 */
public interface TransactionRepository extends JpaRepository<Transaction, Long> {

    /**
     * Finds all transactions for a given bank account with pagination support.
     *
     * @param bankAccountId The ID of the bank account to find transactions for.
     * @param pageable      The Pageable object specifying the pagination configuration.
     * @return A page of transactions for the given bank account.
     */
    Page<Transaction> findByBankAccountAccountNumberOrderByDateDesc(Long bankAccountId, Pageable pageable);

    /**
     * Finds all transactions for a given saving goal.
     *
     * @param savingGoalId The ID of the saving goal to find transactions for.
     * @return A list of transactions for the given saving goal.
     */
    List<Transaction> findBySavingGoal_SavingGoalId(Long savingGoalId);

    /**
     * Finds transactions for a given bank account and category after a specific date.
     *
     * @param bankAccountId the ID of the bank account to find transactions for
     * @param categoryId    the ID of the category to filter transactions by
     * @param date          the date after which to retrieve transactions
     * @return a list of transactions matching the criteria
     */
    List<Transaction> findByBankAccount_AccountNumberAndCategory_CategoryIdAndDateAfter(Long bankAccountId, Long categoryId, Date date);

    /**
     * Finds transactions for a given category between two dates.
     *
     * @param categoryId the ID of the category to filter transactions by
     * @param startDate  the start date for the date range
     * @param endDate    the end date for the date range
     * @return a list of transactions matching the criteria
     */
    List<Transaction> findByCategory_CategoryIdAndDateBetween(Long categoryId, Date startDate, Date endDate);

    /**
     * Finds transactions for a given bank account between two dates.
     *
     * @param bankAccountId the ID of the bank account to find transactions for
     * @param startDate     the start date for the date range
     * @param endDate       the end date for the date range
     * @return a list of transactions matching the criteria
     */
    List<Transaction> findByBankAccount_AccountNumberAndDateBetween(Long bankAccountId, Date startDate, Date endDate);
}

package no.ntnu.sparestibackend.repository;

import no.ntnu.sparestibackend.model.SavingGoal;
import no.ntnu.sparestibackend.model.Status;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Repository interface for managing {@link SavingGoal} entities.
 * Provides methods to perform CRUD operations on saving goals.
 */
@Repository
public interface SavingGoalRepository extends JpaRepository<SavingGoal, Long> {
    /**
     * Finds all saving goals associated with a particular user.
     *
     * @param userId The userID is representing the user whose saving goals are to be retrieved.
     * @return A list of {@link SavingGoal} entities that belong to the specified user.
     */
    List<SavingGoal> findSavingGoalsByUserUserId(Long userId);

    /**
     * Finds all saving goals associated with a particular user and status.
     *
     * @param userId the ID of the user whose saving goals are to be retrieved
     * @param status the status of the saving goals
     * @return a list of {@link SavingGoal} entities that belong to the specified user and status
     */
    List<SavingGoal> findSavingGoalsByUserUserIdAndStatus(Long userId, Status status);
}

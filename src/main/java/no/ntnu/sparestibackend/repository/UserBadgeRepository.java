package no.ntnu.sparestibackend.repository;

import no.ntnu.sparestibackend.model.UserBadge;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

/**
 * Spring Data JPA repository for {@link UserBadge} entities.
 * This interface handles database operations related to user badges,
 * such as retrieving all badges associated with a specific user.
 */
public interface UserBadgeRepository extends JpaRepository<UserBadge, Long> {
    /**
     * Retrieves all {@link UserBadge} entries for a given user ID.
     * This method fetches a list of user badges that are linked to the specified user.
     *
     * @param userId the unique identifier of the user whose badges are to be retrieved.
     * @return a list of {@link UserBadge} instances associated with the provided user ID.
     */
    List<UserBadge> findByUser_UserId(Long userId);
}

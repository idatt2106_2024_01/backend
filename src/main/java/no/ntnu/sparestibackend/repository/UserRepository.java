package no.ntnu.sparestibackend.repository;

import no.ntnu.sparestibackend.model.User;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserRepository extends JpaRepository<User, Long> {
    User findByEmail(String email);

    User findByUserId(Long userId);

    User findByIdpId(String idpId);
}

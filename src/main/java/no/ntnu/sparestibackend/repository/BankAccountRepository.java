package no.ntnu.sparestibackend.repository;

import no.ntnu.sparestibackend.model.AccountType;
import no.ntnu.sparestibackend.model.BankAccount;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Repository interface for managing {@link BankAccount} entities.
 * Provides methods to perform CRUD operations on bank accounts.
 */
public interface BankAccountRepository extends JpaRepository<BankAccount, Long> {

    /**
     * Finds a bank account by its account number.
     *
     * @param accountNumber the account number of the bank account
     * @return the bank account with the specified account number, or {@code null} if no such account exists
     */
    BankAccount findByAccountNumber(Long accountNumber);

    /**
     * Finds a bank account by user ID and account type.
     *
     * @param userId      the ID of the user associated with the bank account
     * @param accountType the type of account
     * @return the bank account that matches the user ID and account type, or {@code null} if no such account exists
     */
    BankAccount findByUserUserIdAndAccountType(Long userId, AccountType accountType);
}

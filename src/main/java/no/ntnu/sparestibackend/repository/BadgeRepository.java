package no.ntnu.sparestibackend.repository;

import no.ntnu.sparestibackend.model.Badge;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Spring Data JPA repository for {@link Badge} entities.
 * This interface manages database operations related to badges,
 * providing automatic CRUD operations and the ability to extend with custom queries.
 */
@Repository
public interface BadgeRepository extends JpaRepository<Badge, Long> {
}

package no.ntnu.sparestibackend.repository;

import no.ntnu.sparestibackend.model.UserPreferences;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

/**
 * Repository interface for user preferences.
 * This repository interacts with the database to perform operations on user preferences.
 */
@Repository
public interface UserPreferencesRepository extends JpaRepository<UserPreferences, Long> {
    Optional<UserPreferences> findByUserUserId(Long userId);
}

package no.ntnu.sparestibackend.utils;

import no.ntnu.sparestibackend.model.Difficulty;
import no.ntnu.sparestibackend.model.IntensityLevel;
import no.ntnu.sparestibackend.model.UserPreferences;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * Class to generate difficulties based on the user's preferences.
 */
public class DifficultyGenerator {
    private final UserPreferences userPreferences;
    private final int numberOfRandomDifficulties;
    private boolean userPreferenceGenerated = false;
    List<Difficulty> generatedDifficulties = new ArrayList<>();
    private final Random random = new Random();


    /**
     * Constructor for the DifficultyGenerator class.
     *
     * @param userPreferences      The user's preferences
     * @param numberOfDifficulties The number of difficulties to generate
     */
    public DifficultyGenerator(UserPreferences userPreferences, int numberOfDifficulties) {
        this.userPreferences = userPreferences;
        this.numberOfRandomDifficulties = new Random().nextInt(numberOfDifficulties);
    }

    /**
     * Method to get the next difficulty. Uses the previously generated difficulties to generate the next one.
     *
     * @return The next difficulty
     */
    public Difficulty getNextDifficulty() {
        Difficulty difficulty;

        if (!userPreferenceGenerated) {
            difficulty = generateDifficultyByUserPreference();
            userPreferenceGenerated = true;
        } else {
            if (generatedDifficulties.size() < numberOfRandomDifficulties) {
                difficulty = generateRandomDifficulty();
            } else {
                difficulty = generateDifficultyByUserPreference();
            }
        }

        generatedDifficulties.add(difficulty);
        return difficulty;
    }

    /**
     * Method to generate a random difficulty.
     *
     * @return A random difficulty
     */
    private Difficulty generateRandomDifficulty() {
        int randomDifficultyIndex = random.nextInt(3);
        return switch (randomDifficultyIndex) {
            case 0 -> Difficulty.EASY;
            case 1 -> Difficulty.MEDIUM;
            case 2 -> Difficulty.HARD;
            default -> throw new IllegalArgumentException("Invalid difficulty index");
        };
    }

    /**
     * Method to generate a difficulty based on the user's preferences.
     *
     * @return A difficulty based on the user's preferences
     */
    private Difficulty generateDifficultyByUserPreference() {
        return switch (userPreferences.getWillingnessToInvest()) {
            case IntensityLevel.A_LITTLE -> Difficulty.EASY;
            case IntensityLevel.SOME -> Difficulty.MEDIUM;
            case IntensityLevel.A_LOT -> Difficulty.HARD;
        };
    }
}

package no.ntnu.sparestibackend.utils;

import no.ntnu.sparestibackend.model.BankAccount;
import no.ntnu.sparestibackend.model.Category;
import no.ntnu.sparestibackend.model.Transaction;

import java.util.*;

/**
 * Utility class for generating random transactions for testing purposes.
 */
public class TransactionGenerator {

    /**
     * Generates a list of random transactions for the given bank account and categories.
     *
     * @param bankAccount The bank account to which the transactions belong.
     * @param categories  The list of categories to choose from when generating transactions.
     * @return A list of randomly generated transactions.
     */
    public static List<Transaction> generateRandomTransactions(BankAccount bankAccount, List<Category> categories) {
        List<Transaction> transactions = new ArrayList<>();

        for (int i = 0; i < 50; i++) {
            Category randomCategory = categories.get(new Random().nextInt(categories.size()));
            Transaction transaction = Transaction.builder()
                    .bankAccount(bankAccount)
                    .sum(-new Random().nextInt(1000))
                    .date(randomDateInLast30Days())
                    .description("Kjøp av " + randomCategory.getName())
                    .category(randomCategory)
                    .build();
            transactions.add(transaction);
        }
        return transactions;
    }

    /**
     * Generates a random date within the last 30 days.
     *
     * @return A random date within the last 30 days.
     */
    private static Date randomDateInLast30Days() {
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.DAY_OF_MONTH, -(new Random()).nextInt(30));
        return calendar.getTime();
    }
}

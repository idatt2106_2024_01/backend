package no.ntnu.sparestibackend.utils;

import no.ntnu.sparestibackend.model.Category;
import no.ntnu.sparestibackend.model.User;

/**
 * Utility class for managing default categories for a user.
 */
public class CategoryDefaults {
    private static final Category[] DEFAULT_CATEGORIES = {
            Category.builder()
                    .name("Klær")
                    .img("https://api.sparesti.no/images/category/clothes.png")
                    .build(),
            Category.builder()
                    .name("Kaffe")
                    .img("https://api.sparesti.no/images/category/coffee.png")
                    .build(),
            Category.builder()
                    .name("Drivstoff")
                    .img("https://api.sparesti.no/images/category/fuel.png")
                    .build(),
            Category.builder()
                    .name("Gaming")
                    .img("https://api.sparesti.no/images/category/gaming.png")
                    .build(),
            Category.builder()
                    .name("Matvarer")
                    .img("https://api.sparesti.no/images/category/groceries.png")
                    .build(),
            Category.builder()
                    .name("Strøm")
                    .img("https://api.sparesti.no/images/category/house.png")
                    .build(),
            Category.builder()
                    .name("Telefon")
                    .img("https://api.sparesti.no/images/category/phone.png")
                    .build(),
            Category.builder()
                    .name("Spise ute")
                    .img("https://api.sparesti.no/images/category/restaurant.png")
                    .build(),
            Category.builder()
                    .name("Sport")
                    .img("https://api.sparesti.no/images/category/sport.png")
                    .build(),
            Category.builder()
                    .name("Underholdning")
                    .img("https://api.sparesti.no/images/category/streaming.png")
                    .build(),
            Category.builder()
                    .name("Reise")
                    .img("https://api.sparesti.no/images/category/travel.png")
                    .build(),
    };

    /**
     * Returns the default categories, each associated with the given user.
     *
     * @param user the user to associate with the categories
     * @return an array of default categories
     */
    public static Category[] getDefaultCategories(User user) {
        for (Category category : DEFAULT_CATEGORIES) {
            category.setUser(user);
        }
        return DEFAULT_CATEGORIES;
    }
}

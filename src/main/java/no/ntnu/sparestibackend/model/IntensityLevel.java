package no.ntnu.sparestibackend.model;

/**
 * Represents the intensity level for a particular activity.
 */
public enum IntensityLevel {
    A_LITTLE,
    SOME,
    A_LOT
}

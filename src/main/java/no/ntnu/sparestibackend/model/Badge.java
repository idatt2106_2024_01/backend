package no.ntnu.sparestibackend.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.persistence.*;
import lombok.*;

import java.util.HashSet;
import java.util.Set;

/**
 * Represents a badge that can be achieved by a user. Badges are rewards earned for completing specific
 * activities or meeting certain criteria within the application.
 */
@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Entity
@Table(name = "badges")
public class Badge {

    /**
     * Unique identifier for the badge. This ID is generated automatically.
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "badge_id")
    private Long badgeId;

    /**
     * The name of the badge. This is unique across all badges.
     */
    @Column(name = "badgeName", unique = true)
    @NonNull
    private String name;

    /**
     * A brief description of what the badge represents or the criteria to earn it.
     */
    @Column(name = "description")
    private String description;

    /**
     * URL to an image or icon representing the badge visually.
     */
    @Column(name = "picture")
    private String url;

    /**
     * Set of associations to users who have earned or are in progress of earning this badge.
     * This setup supports a many-to-many relationship encapsulated by the UserBadge entity
     * that tracks the progress and achievement status of each badge for a user.
     */
    @JsonIgnore
    @OneToMany(mappedBy = "badge", cascade = CascadeType.ALL, orphanRemoval = true)
    @Builder.Default
    private Set<UserBadge> userBadges = new HashSet<>();

}

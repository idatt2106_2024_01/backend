package no.ntnu.sparestibackend.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.persistence.*;
import lombok.*;

import java.util.Date;


/**
 * Represents a saving challenge in the database.
 */
@Getter
@Setter
@ToString
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Entity
@Table(name = "saving_challenges")
public class SavingChallenge {

    /**
     * The unique identifier of the saving challenge.
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "saving_challenge_id")
    private Long savingChallengeId;


    /**
     * The description of the saving challenge.
     */
    @Column(name = "description")
    @NonNull
    private String description;

    /**
     * The category to which the saving challenge belongs.
     */
    @ManyToOne
    @JoinColumn(name = "category_id")
    @NonNull
    private Category category;

    /**
     * The current spending of the saving challenge.
     */
    @Column(name = "current_spending")
    private double currentSpending;

    /**
     * The target spending of the saving challenge.
     */
    @Column(name = "target_spending")
    private double targetSpending;

    /**
     * The start date of the saving challenge.
     */
    @Column(name = "start_date")
    @NonNull
    private Date startDate;

    /**
     * The end date of the saving challenge.
     */
    @Column(name = "end_date")
    @NonNull
    private Date endDate;

    /**
     * The status of the saving challenge.
     */
    @Enumerated(EnumType.STRING)
    @Column(name = "status", nullable = false)
    private Status status;

    /**
     * The difficulty of the saving challenge
     */
    @Enumerated(EnumType.STRING)
    @Column(name = "difficulty", nullable = false)
    private Difficulty difficulty;

    /**
     * The saving goal the saving challenge is linked to.
     */
    @JsonIgnore
    @ManyToOne
    @JoinColumn(name = "saving_goal_id")
    private SavingGoal savingGoal;

    /**
     * The user the saving challenge is linked to.
     */
    @JsonIgnore
    @ManyToOne
    @JoinColumn(name = "user_id")
    @NonNull
    private User user;
}

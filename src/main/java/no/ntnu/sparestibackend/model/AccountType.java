package no.ntnu.sparestibackend.model;

/**
 * Enum representing the account types.
 */
public enum AccountType {
    SAVINGS,
    CHECKING
}

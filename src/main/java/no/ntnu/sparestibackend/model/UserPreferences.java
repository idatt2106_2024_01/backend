package no.ntnu.sparestibackend.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.persistence.*;
import lombok.*;

import java.util.Set;

/**
 * Represents the user preferences in the system.
 * User preferences capture information like saving level, willingness to invest, theme choice, and preferred saving categories.
 * Each preference set is linked to a specific user.
 */
@Getter
@Setter
@ToString
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Entity
@Table(name = "user_preferences")
public class UserPreferences {

    /**
     * The unique identifier for user preferences.
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "user_preferences_id")
    private Long userPreferencesId;

    /**
     * The user associated with these preferences.
     * This represents a one-to-one relationship with the User entity.
     */
    @JsonIgnore
    @OneToOne
    @JoinColumn(name = "user_id", referencedColumnName = "user_id")
    @NonNull
    private User user;

    /**
     * The level of intensity for the user's saving habits.
     * Can be a little, some, or a lot.
     */
    @Enumerated(EnumType.STRING)
    @Column(name = "saving_level")
    private IntensityLevel savingLevel;

    /**
     * The level of intensity for the user's willingness to invest.
     * Can be a little, some, or a lot.
     */
    @Enumerated(EnumType.STRING)
    @Column(name = "willingness_to_invest")
    private IntensityLevel willingnessToInvest;

    /**
     * The visual theme preference for the user interface.
     */
    @Column(name = "theme")
    private String theme;

    /**
     * The categories that the user prefers for saving.
     * This represents a many-to-many relationship with the Category entity.
     */
    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(
            name = "user_preferences_categories",
            joinColumns = @JoinColumn(name = "preferences_id"),
            inverseJoinColumns = @JoinColumn(name = "category_id")
    )
    private Set<Category> savingCategories;

    /**
     * The user's nickname or alias.
     */
    @Column(name = "nickname")
    private String nickname;
}

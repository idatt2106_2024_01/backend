package no.ntnu.sparestibackend.model;

import jakarta.persistence.*;
import lombok.*;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Represents a user in the system.
 * Each user has a unique ID, email, password, and personal information such as first and last names.
 * Users may also be associated with multiple bank accounts.
 */
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Entity
@ToString(exclude = "bankAccounts")
@Table(name = "users")
public class User {

    /**
     * The unique identifier for a user.
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "user_id")
    private Long userId;

    /**
     * The identity provider ID, which is unique for each user.
     */
    @Column(name = "idp_id", unique = true)
    private String idpId;

    /**
     * The user's email address.
     * Must be unique for each user.
     */
    @Column(name = "email", unique = true)
    private String email;

    /**
     * The user's first name.
     * This field is required and cannot be null.
     */
    @Column(name = "first_name")
    @NonNull
    private String firstName;

    /**
     * The user's last name.
     * This field is required and cannot be null.
     */
    @Column(name = "last_name")
    @NonNull
    private String lastName;

    /**
     * List of bank accounts associated with the user. This relationship is managed automatically
     * by Hibernate through the cascade and orphan removal settings.
     */
    @OneToMany(mappedBy = "user", cascade = CascadeType.ALL, orphanRemoval = true)
    @Builder.Default
    private List<BankAccount> bankAccounts = new ArrayList<>();

    /**
     * Set of badges the user has earned. Each entry in this set represents a many-to-many
     * relationship detailing the user's progress and achievements related to specific badges.
     */
    @OneToMany(mappedBy = "user", cascade = CascadeType.ALL, orphanRemoval = true)
    @Builder.Default
    private Set<UserBadge> userBadges = new HashSet<>();

    /**
     * The URL or identifier for the user's profile avatar.
     * With default value to Standard avatar
     */
    @Column(name = "profile_avatar")
    @Builder.Default
    private String profileAvatar = "https://api.sparesti.no/images/avatar/StdAvatar.png";

    /**
     * The user's newsletter consent status.
     */
    @Column(name = "newsletter_consent")
    @Builder.Default
    private boolean newsletterConsent = false;

    /**
     * The user's preferences.
     */
    @OneToOne(mappedBy = "user", cascade = CascadeType.ALL, orphanRemoval = true)
    @JoinColumn(name = "user_preferences_id")
    private UserPreferences userPreferences;

    /**
     * The user's onboarding status.
     */
    @Column(name = "is_onboarded")
    @Builder.Default
    private boolean isOnboarded = false;
}

package no.ntnu.sparestibackend.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.persistence.*;
import lombok.*;

import java.util.Date;

/**
 * Represents a badge earned by a user.
 * This entity links badges to users and records whether the badge
 * has been achieved and the date it was achieved.
 */
@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "user_badges")
public class UserBadge {

    /**
     * The id of the user and badge relationship
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    /**
     * The user who owns the badge.
     */
    @JsonIgnore
    @ManyToOne
    @JoinColumn(name = "user_id", referencedColumnName = "user_id")
    private User user;

    /**
     * The badge that has been achieved.
     */
    @ManyToOne
    @JoinColumn(name = "badge_id", referencedColumnName = "badge_id")
    private Badge badge;

    /**
     * Indicates if the badge has been achieved by the user.
     */
    @Column(name = "achieved")
    private boolean achieved;

    /**
     * The date on which the badge was achieved.
     */
    @Column(name = "date_achieved")
    private Date dateAchieved;
}

package no.ntnu.sparestibackend.model;

/**
 * Enum representing the status of a saving challenge.
 */
public enum Status {
    ACTIVE,
    COMPLETED,
    FAILED
}

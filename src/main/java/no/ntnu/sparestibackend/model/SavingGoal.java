package no.ntnu.sparestibackend.model;

import jakarta.persistence.*;
import lombok.*;

import java.util.Date;
import java.util.Set;

/**
 * Represents a saving goal within the system. This entity is used to store and manage data
 * related to individual saving goals set by users.
 */
@Getter
@Setter
@ToString
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Entity
@Table(name = "saving_goals")
public class SavingGoal {

    /**
     * Unique identifier for the SavingGoal.
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "saving_goal_id")
    private Long savingGoalId;

    /**
     * Title of the saving goal, provides a brief description.
     */
    @Column(name = "title")
    @NonNull
    private String title;

    /**
     * The numerical goal or target amount to be saved.
     */
    @Column(name = "goal")
    private int goal;

    /**
     * The deadline by which the goal should be achieved.
     */
    @Column(name = "deadline")
    @NonNull
    private Date deadline;

    /**
     * The user the saving goal is related to.
     */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "user_id", nullable = false)
    @ToString.Exclude
    private User user;

    /**
     * The saving challenges related to the saving goal.
     */
    @OneToMany(mappedBy = "savingGoal")
    private Set<SavingChallenge> savingChallenges;

    /**
     * The status of the saving goal, indicating whether it's active, achieved, or failed.
     */
    @Enumerated(EnumType.STRING)
    @Column(name = "status", nullable = false)
    @Builder.Default
    private Status status = Status.ACTIVE;
}


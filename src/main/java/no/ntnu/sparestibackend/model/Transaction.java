package no.ntnu.sparestibackend.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.persistence.*;
import lombok.*;

import java.util.Date;

/**
 * Represents a financial transaction in the system.
 * Each transaction is associated with a category, has a specific date,
 * a description, and a monetary amount.
 */
@Getter
@Setter
@ToString
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Entity
@Table(name = "transaction")
public class Transaction {

    /**
     * The unique identifier for the transaction. This ID is automatically generated
     * for each new transaction.
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "transaction_id")
    private Long transactionId;

    /**
     * The bank account the transaction is related to.
     */
    @JsonIgnore
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "bank_account_id", nullable = false)
    private BankAccount bankAccount;

    /**
     * The category of the transaction which helps in grouping transactions.
     */
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "category_id")
    private Category category;

    /**
     * The date when the transaction occurred.
     */
    @Column(name = "date")
    @NonNull
    private Date date;

    /**
     * A description of the transaction, providing more details about it.
     */
    @Column(name = "description")
    private String description;

    /**
     * The monetary sum of the transaction.
     */
    @Column(name = "sum")
    private double sum;

    /**
     * The saving goal the transaction is linked to.
     * Nullable for transactions not linked directly to a goal
     */
    @JsonIgnore
    @ManyToOne
    @JoinColumn(name = "saving_goal_id")
    @ToString.Exclude
    private SavingGoal savingGoal;
}

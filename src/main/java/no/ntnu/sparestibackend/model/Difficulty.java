package no.ntnu.sparestibackend.model;

/**
 * Enum representing the difficulty levels of saving challenges.
 */
public enum Difficulty {
    EASY,
    MEDIUM,
    HARD
}




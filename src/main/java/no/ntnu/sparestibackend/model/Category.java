package no.ntnu.sparestibackend.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.persistence.*;
import lombok.*;

/**
 * Represents a category for transactions within the financial management system.
 * Categories are used to organize transactions into meaningful groups, facilitating
 * the management and analysis of financial data.
 */
@Getter
@Setter
@ToString
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Entity
@Table(name = "categories",
        uniqueConstraints = @UniqueConstraint(columnNames = {"user_id", "name"}))
public class Category {

    /**
     * The unique identifier for the category. This ID is automatically generated
     * for each new category.
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "category_id")
    private Long categoryId;

    /**
     * The name of the category which identifies or describes the group
     * that a transaction belongs to. This field is marked as non-nullable.
     */
    @Column(name = "name")
    @NonNull
    private String name;


    /**
     * The image of the category.
     */
    @Column(name = "img")
    @NonNull
    private String img;


    /**
     * The user the saving goal is related to.
     */
    @JsonIgnore
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "user_id", nullable = false)
    @ToString.Exclude
    private User user;
}

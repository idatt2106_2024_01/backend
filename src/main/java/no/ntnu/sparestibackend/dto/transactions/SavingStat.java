package no.ntnu.sparestibackend.dto.transactions;

import lombok.Builder;

/**
 * Represents a saving statistic.
 *
 * @param totalSaved   The total amount saved.
 * @param yearAndMonth The year and month associated with the statistic.
 */
@Builder
public record SavingStat(
        double totalSaved,
        String yearAndMonth
) {
}

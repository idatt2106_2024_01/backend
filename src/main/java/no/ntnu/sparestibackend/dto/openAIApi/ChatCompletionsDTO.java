package no.ntnu.sparestibackend.dto.openAIApi;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Builder;

import java.util.List;

/**
 * Represents the response from the OpenAI API for chat completions.
 *
 * <p>Contains information about the model used for completion, response format, and the list of messages generated as completions.</p>
 *
 * @param model          The model used for the completion.
 * @param responseFormat The response format for the completion.
 * @param messages       The list of messages generated as completions.
 */
@Builder
public record ChatCompletionsDTO(
        String model,
        @JsonProperty("response_format") ChatCompletionsFormatDTO responseFormat,
        List<ChatCompletionsMessageDTO> messages
) {
}

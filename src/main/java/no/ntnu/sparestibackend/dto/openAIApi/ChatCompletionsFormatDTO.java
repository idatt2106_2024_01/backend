package no.ntnu.sparestibackend.dto.openAIApi;

import lombok.Builder;

/**
 * Represents the format of chat completions in the OpenAI API response.
 *
 * @param type The type of format.
 */
@Builder
public record ChatCompletionsFormatDTO(
        String type
) {
}

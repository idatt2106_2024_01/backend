package no.ntnu.sparestibackend.dto.openAIApi;

import lombok.Builder;

/**
 * Represents a message in the context of chat completions from the OpenAI API response.
 *
 * @param role    The role associated with the message.
 * @param content The content of the message.
 */
@Builder
public record ChatCompletionsMessageDTO(
        String role,
        String content
) {
}

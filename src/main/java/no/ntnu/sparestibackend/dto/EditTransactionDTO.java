package no.ntnu.sparestibackend.dto;

import lombok.Builder;

/**
 * DTO for editing a transaction.
 *
 * @param categoryId  The id of the category
 * @param description The description of the transaction
 */
@Builder
public record EditTransactionDTO(
        Long categoryId,
        String description
) {
}

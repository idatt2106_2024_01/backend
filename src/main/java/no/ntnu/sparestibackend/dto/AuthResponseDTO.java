package no.ntnu.sparestibackend.dto;

import lombok.Builder;
import lombok.NonNull;

/**
 * Data transfer object for authentication responses.
 *
 * @param token The authentication token
 * @param user  The user information
 */
@Builder
public record AuthResponseDTO(
        @NonNull
        String token,
        UserInfoDTO user
) {
}


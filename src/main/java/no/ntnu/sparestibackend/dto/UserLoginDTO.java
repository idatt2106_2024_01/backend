package no.ntnu.sparestibackend.dto;

import lombok.Builder;

/**
 * DTO for user login.
 *
 * @param email    The user's email
 * @param password The user's password
 */
@Builder
public record UserLoginDTO(
        String email,
        String password
) {
}

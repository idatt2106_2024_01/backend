package no.ntnu.sparestibackend.dto;

import lombok.Builder;
import no.ntnu.sparestibackend.model.Category;
import no.ntnu.sparestibackend.model.Difficulty;
import no.ntnu.sparestibackend.model.Status;

import java.util.Date;

/**
 * A data transfer object representing a saving challenge.
 * This record is used to transfer saving challenge data
 * throughout different layers of the application,
 * typically between services and controllers.
 * It encapsulates the state of a saving challenge
 * without behaviors.
 *
 * @param description     The description of the saving challenge.
 * @param currentSpending The current spending amount for the challenge.
 * @param targetSpending  The targeted spending amount to reach.
 * @param startDate       The start date of the saving challenge.
 * @param endDate         The end date of the saving challenge.
 * @param status          The current status of the saving challenge, e.g., "active", "completed".
 * @param difficulty      The difficulty level of the challenge, expressed as a string.
 */
@Builder
public record SavingChallengeDTO(
        Long savingChallengeId,
        String description,
        Double currentSpending,
        Double spentSoFar,
        Double targetSpending,
        Date startDate,
        Date endDate,
        Status status,
        Difficulty difficulty,
        Long savingGoalId,
        Long categoryId,
        Category category
) {
}

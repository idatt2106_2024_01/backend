package no.ntnu.sparestibackend.dto;

import lombok.Builder;

/**
 * Data transfer object for the Category entity.
 *
 * @param name The name of the category
 */

@Builder
public record CategoryDTO(
        String name,
        String img
) {
}

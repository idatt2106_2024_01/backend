package no.ntnu.sparestibackend.dto;

import lombok.Builder;
import no.ntnu.sparestibackend.model.Status;

import java.util.Date;

/**
 * Data transfer object for saving goals.
 *
 * @param title    The title of the saving goal
 * @param goal     The goal amount
 * @param deadline The deadline for reaching the goal
 * @param status   The current status of the saving goal
 */
@Builder
public record SavingGoalDTO(
        Long savingGoalId,
        String title,
        Integer goal,
        Date deadline,
        Status status
) {
}

package no.ntnu.sparestibackend.dto;

import lombok.Builder;
import no.ntnu.sparestibackend.model.BankAccount;
import no.ntnu.sparestibackend.model.UserPreferences;

import java.util.List;

/**
 * DTO for user information.
 *
 * @param userId          The user's ID
 * @param email           The user's email
 * @param firstName       The user's first name
 * @param lastName        The user's last name
 * @param bankAccounts    The user's bank accounts
 * @param userPreferences The user's preferences
 */
@Builder
public record UserInfoDTO(
        Long userId,
        String email,
        String firstName,
        String lastName,
        List<BankAccount> bankAccounts,
        UserPreferences userPreferences,
        String profileAvatar,
        boolean newsletterConsent,
        boolean isOnboarded
) {
}

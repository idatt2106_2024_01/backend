package no.ntnu.sparestibackend.dto;

import lombok.Builder;

import java.util.Date;

/**
 * Data transfer object for the Transaction entity.
 *
 * @param transactionId The id of the transaction
 * @param categoryId    The id of the category
 * @param date          The date of the transaction
 * @param description   The description of the transaction
 * @param sum           The sum of the transaction
 * @param accountNumber The id of the bank account
 * @param savingGoalId  The id of the saving goal
 */
@Builder
public record TransactionDTO(
        Long transactionId,
        Long categoryId,
        Date date,
        String description,
        Double sum,
        Long accountNumber,
        Long savingGoalId
) {
}

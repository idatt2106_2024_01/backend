package no.ntnu.sparestibackend.dto;

import lombok.Builder;

/**
 * Data transfer object for user stats.
 *
 * @param totalSavings            The user's total savings in the program.
 * @param hardChallengesCompleted The user's amount of hard challenges completed.
 * @param successfulStreak        The user's longest streak of completed challenges.
 */
@Builder
public record UserStatsDTO(
        Double totalSavings,
        Integer hardChallengesCompleted,
        Integer successfulStreak
) {
}

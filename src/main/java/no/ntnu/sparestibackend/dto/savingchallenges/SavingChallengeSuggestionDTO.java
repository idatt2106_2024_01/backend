package no.ntnu.sparestibackend.dto.savingchallenges;

import lombok.Builder;
import no.ntnu.sparestibackend.model.Category;
import no.ntnu.sparestibackend.model.Difficulty;

/**
 * Represents a suggestion for a saving challenge.
 *
 * @param category        The category associated with the saving challenge.
 * @param description     The description of the saving challenge.
 * @param currentSpending The current spending related to the category.
 * @param difficulty      The difficulty level of the saving challenge.
 */
@Builder
public record SavingChallengeSuggestionDTO(
        Category category,
        String description,
        Double currentSpending,
        Difficulty difficulty
) {
}

package no.ntnu.sparestibackend.dto;

import lombok.Builder;
import lombok.NonNull;
import no.ntnu.sparestibackend.model.AccountType;

/**
 * Data transfer object for bank accounts.
 *
 * @param name          The name of the account
 * @param accountNumber The account number
 * @param accountType   The type of the account
 */
@Builder
public record BankAccountDTO(
        @NonNull String name,
        Long accountNumber,
        AccountType accountType
) {
}

package no.ntnu.sparestibackend.dto;

import lombok.Builder;
import no.ntnu.sparestibackend.model.IntensityLevel;

import java.util.Set;

/**
 * DTO for user preferences.
 *
 * @param savingLevel         The user's saving level
 * @param willingnessToInvest The user's willingness to invest
 * @param theme               The user's theme
 * @param savingCategoryIds   The user's saving categories
 */
@Builder
public record UserPreferencesDTO(
        Long userPreferencesId,
        IntensityLevel savingLevel,
        IntensityLevel willingnessToInvest,
        String theme,
        Set<Long> savingCategoryIds,
        String nickname
) {
}

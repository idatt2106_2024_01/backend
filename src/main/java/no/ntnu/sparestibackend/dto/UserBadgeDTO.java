package no.ntnu.sparestibackend.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import java.util.Date;

/**
 * Data Transfer Object (DTO) representing a user badge.
 *
 * <p>Contains information about a badge, including its ID, name, description, achievement status, and date achieved.</p>
 */
@Getter
@Setter
@AllArgsConstructor
@Builder
public class UserBadgeDTO {
    private Long badgeId;
    private String badgeName;
    private String description;
    private boolean achieved;
    private Date dateAchieved;
}

package no.ntnu.sparestibackend.dto;

import lombok.Builder;

/**
 * Data transfer object for user registration.
 *
 * @param email     The user's email
 * @param password  The user's password
 * @param firstName The user's first name
 * @param lastName  The user's last name
 */
@Builder
public record UserRegisterDTO(
        String email,
        String password,
        String firstName,
        String lastName
) {
}

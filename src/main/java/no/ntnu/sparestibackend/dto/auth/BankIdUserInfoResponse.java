/**
 * A data transfer object representing the response received from the BankID user info endpoint.
 * This response typically contains information about the authenticated user.
 */
package no.ntnu.sparestibackend.dto.auth;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Builder;

/**
 * A record representing the BankID user info response.
 *
 * @param idpId      The identity provider's ID associated with the user.
 * @param givenName  The given name (first name) of the authenticated user.
 * @param familyName The family name (last name) of the authenticated user.
 */
@Builder
public record BankIdUserInfoResponse(
        @JsonProperty("idp_id") String idpId,
        @JsonProperty("given_name") String givenName,
        @JsonProperty("family_name") String familyName
) {
}

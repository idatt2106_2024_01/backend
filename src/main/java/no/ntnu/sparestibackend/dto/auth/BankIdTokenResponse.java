/**
 * A data transfer object representing the response received from the BankID token endpoint.
 * This response typically contains various tokens and related information.
 */
package no.ntnu.sparestibackend.dto.auth;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Builder;

/**
 * A record representing the BankID token response.
 *
 * @param idToken      The ID token received from the BankID service.
 * @param accessToken  The access token received from the BankID service.
 * @param refreshToken The refresh token received from the BankID service.
 * @param expiresIn    The duration in seconds for which the tokens are valid.
 * @param tokenType    The type of token received (e.g., "Bearer").
 * @param scope        The scope of the tokens,
 *                     indicating the resources they can access.
 */
@Builder
public record BankIdTokenResponse(
        @JsonProperty(value = "id_token") String idToken,
        @JsonProperty(value = "access_token") String accessToken,
        @JsonProperty(value = "refresh_token") String refreshToken,
        @JsonProperty(value = "expires_in") String expiresIn,
        @JsonProperty(value = "token_type") String tokenType,
        @JsonProperty(value = "scope") String scope
) {
}

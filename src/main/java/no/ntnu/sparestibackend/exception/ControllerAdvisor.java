package no.ntnu.sparestibackend.exception;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import java.time.LocalDateTime;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 * Global exception handler for controllers.
 */
@RestControllerAdvice
public class ControllerAdvisor extends ResponseEntityExceptionHandler {
    /**
     * This method handles all other non-specified exceptions.
     *
     * @param e          The actual exception being thrown
     * @param webRequest The web request
     * @return ResponseEntity with message and status code
     */
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(Exception.class)
    public ResponseEntity<Object> exceptionAction(Exception e, WebRequest webRequest) {
        e.printStackTrace();
        Map<String, Object> body = new LinkedHashMap<>();
        body.put("Message: ", e.getMessage());
        body.put("Time of error: ", LocalDateTime.now());
        return ResponseEntity.badRequest().body(body);
    }
}

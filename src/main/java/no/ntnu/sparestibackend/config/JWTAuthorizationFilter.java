package no.ntnu.sparestibackend.config;

import jakarta.servlet.FilterChain;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import no.ntnu.sparestibackend.service.TokenService;
import org.antlr.v4.runtime.misc.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;

import java.io.IOException;

@Component
public class JWTAuthorizationFilter extends OncePerRequestFilter {
    private final TokenService tokenService;
    private final Logger logger = LoggerFactory.getLogger(JWTAuthorizationFilter.class);

    /**
     * Constructs a new JWTAuthorizationFilter with the specified {@link TokenService}.
     *
     * @param tokenService the TokenService to use for token verification.
     */
    @Autowired
    public JWTAuthorizationFilter(TokenService tokenService) {
        this.tokenService = tokenService;
    }

    /**
     * Filters incoming requests to verify JWT token and set authentication if valid.
     *
     * @param request     the HTTP servlet request.
     * @param response    the HTTP servlet response.
     * @param filterChain the filter chain to execute.
     * @throws ServletException if there is a servlet-related problem.
     * @throws IOException      if there is an I/O problem.
     */
    @Override
    protected void doFilterInternal(@NotNull HttpServletRequest request, @NotNull HttpServletResponse response, @NotNull FilterChain filterChain) throws ServletException, IOException {
        final String header = request.getHeader(HttpHeaders.AUTHORIZATION);
        if (header == null || !header.startsWith("Bearer ")) {
            filterChain.doFilter(request, response);
            logger.warn("No Bearer token found in request");
            return;
        }

        String token = header.substring(7);
        final String userId = tokenService.verifyTokenAndGetUserId(token);
        if (userId == null) {
            filterChain.doFilter(request, response);
            logger.warn("Token validation failed");
            return;
        }

        UsernamePasswordAuthenticationToken auth = new UsernamePasswordAuthenticationToken(
                userId,
                null,
                null);
        SecurityContextHolder.getContext().setAuthentication(auth);

        filterChain.doFilter(request, response);
    }
}

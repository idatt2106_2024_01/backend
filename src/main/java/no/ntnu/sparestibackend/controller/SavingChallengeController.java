package no.ntnu.sparestibackend.controller;

import lombok.NonNull;
import no.ntnu.sparestibackend.dto.SavingChallengeDTO;
import no.ntnu.sparestibackend.dto.savingchallenges.SavingChallengeSuggestionDTO;
import no.ntnu.sparestibackend.model.SavingChallenge;
import no.ntnu.sparestibackend.service.SavingChallengeService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * REST controller for managing {@link SavingChallenge}.
 * This controller handles all HTTP requests for creating,
 * retrieving, updating, and deleting saving challenges.
 * Mapped to the "/saving-challenges" URL path segment.
 */
@RestController
@RequestMapping("/saving-challenges")
public class SavingChallengeController {

    private static final Logger logger = LoggerFactory.getLogger(SavingChallengeController.class);

    @Autowired
    private SavingChallengeService savingChallengeService;

    /**
     * Creates a new saving challenge.
     *
     * @param savingChallengeDTO the saving challenge to create
     * @return the created saving challenge
     */
    @PostMapping
    public ResponseEntity<SavingChallengeDTO> createSavingChallenge(@NonNull Authentication authentication, @RequestBody SavingChallengeDTO savingChallengeDTO) {
        Long userId = Long.parseLong((String) authentication.getPrincipal());
        logger.info("Creating saving challenge for user ID: {}", userId);
        SavingChallengeDTO savedChallenge = savingChallengeService.saveSavingChallenge(userId, savingChallengeDTO);
        logger.info("Saving challenge created successfully with ID: {}", savedChallenge.savingChallengeId());
        return ResponseEntity.status(HttpStatus.CREATED).body(savedChallenge);
    }

    /**
     * Retrieves all saving challenges for the authenticated user.
     *
     * @param authentication The authentication object containing information about the authenticated user.
     * @return ResponseEntity containing a list of all saving challenges for the authenticated user.
     */
    @GetMapping
    public ResponseEntity<List<SavingChallengeDTO>> getAllSavingChallengesForAuthenticatedUser(Authentication authentication) {
        Long userId = Long.parseLong((String) authentication.getPrincipal());
        logger.info("Retrieving all saving challenges for user ID: {}", userId);
        List<SavingChallengeDTO> challenges = savingChallengeService.getAllSavingChallengesForUser(userId);
        logger.info("Found {} saving challenges for user ID {}", challenges.size(), userId);
        return ResponseEntity.ok(challenges);
    }

    /**
     * Generates saving challenges for the authenticated user.
     *
     * @param authentication the authentication object containing the user's credentials
     * @return a list of generated saving challenges
     */
    @GetMapping("/generate")
    public ResponseEntity<List<SavingChallengeSuggestionDTO>> generateSavingChallenges(@NonNull Authentication authentication) {
        Long userId = Long.parseLong((String) authentication.getPrincipal());
        logger.info("Generating saving challenges for user ID: {}", userId);
        List<SavingChallengeSuggestionDTO> suggestions = savingChallengeService.generateSavingChallenges(userId, 3);
        logger.info("Generated {} saving challenge suggestions for user ID: {}", suggestions.size(), userId);
        return ResponseEntity.ok(suggestions);
    }

    /**
     * Retrieves a specific saving challenge by its ID.
     *
     * @param id the ID of the saving challenge to retrieve
     * @return the requested saving challenge wrapped in a {@link ResponseEntity}
     */
    @GetMapping("/{id}")
    public ResponseEntity<SavingChallengeDTO> getSavingChallengeById(@PathVariable Long id, Authentication authentication) {
        Long userId = Long.parseLong(authentication.getPrincipal().toString());
        logger.info("Retrieving saving challenge ID: {} for user ID: {}", id, userId);
        SavingChallengeDTO challenge = savingChallengeService.getSavingChallengeById(id);
        return ResponseEntity.ok(challenge);
    }


    /**
     * Updates a saving challenge.
     *
     * @param id                 the ID of the saving challenge to update
     * @param savingChallengeDTO the data transfer object containing updated fields
     * @return the updated saving challenge wrapped in a {@link ResponseEntity}
     */
    @PutMapping("/{id}")
    public ResponseEntity<SavingChallengeDTO> updateSavingChallenge(@PathVariable Long id, @RequestBody SavingChallengeDTO savingChallengeDTO) {
        logger.info("Updating saving challenge with ID: {}", id);
        SavingChallengeDTO updatedChallenge = savingChallengeService.updateSavingChallenge(id, savingChallengeDTO);
        logger.info("Saving challenge with ID: {} updated successfully", id);
        return ResponseEntity.ok(updatedChallenge);
    }

    /**
     * Deletes a specific saving challenge by its ID.
     *
     * @param id the ID of the saving challenge to delete
     * @return a {@link ResponseEntity} with HTTP status OK
     */
    @DeleteMapping("/{id}")
    public ResponseEntity<?> deleteSavingChallenge(@PathVariable Long id) {
        logger.info("Deleting saving challenge with ID: {}", id);
        savingChallengeService.deleteSavingChallenge(id);
        logger.info("Saving challenge with ID: {} deleted successfully", id);
        return ResponseEntity.ok().build();
    }

    /**
     * Links a saving challenge to a specified saving goal.
     *
     * @param id                 The ID of the saving challenge to be updated.
     * @param savingChallengeDTO The DTO containing the ID of the saving goal to be linked.
     * @return A ResponseEntity containing the updated saving challenge if successful, or an appropriate error response otherwise.
     * @throws RuntimeException if the challenge or goal does not exist.
     */
    @PutMapping("/{id}/link-goal")
    public ResponseEntity<SavingChallenge> linkSavingChallengeToGoal(@PathVariable Long id, @RequestBody SavingChallengeDTO savingChallengeDTO) {
        logger.info("Linking saving challenge ID: {} to saving goal ID: {}", id, savingChallengeDTO.savingGoalId());
        SavingChallenge updatedChallenge = savingChallengeService.linkChallengeToGoal(id, savingChallengeDTO.savingGoalId());
        logger.info("Saving challenge ID: {} linked to saving goal ID: {}", id, savingChallengeDTO.savingGoalId());
        return ResponseEntity.ok(updatedChallenge);
    }
}

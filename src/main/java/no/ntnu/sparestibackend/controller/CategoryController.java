package no.ntnu.sparestibackend.controller;

import no.ntnu.sparestibackend.dto.CategoryDTO;
import no.ntnu.sparestibackend.model.Category;
import no.ntnu.sparestibackend.service.CategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.*;

/**
 * Controller for handling requests related to categories
 */
@RestController
@RequestMapping("/categories")
public class CategoryController {

    @Autowired
    private CategoryService categoryService;

    /**
     * Endpoint for creating a new category
     *
     * @param authentication the authentication object
     * @param categoryDTO    the categoryDTO object
     * @return the created category
     */
    @PostMapping
    public ResponseEntity<Category> addCategory(Authentication authentication, @RequestBody CategoryDTO categoryDTO) {
        long userId = Long.parseLong((String) authentication.getPrincipal());
        return ResponseEntity.status(HttpStatus.CREATED).body(categoryService.createCategory(userId, categoryDTO));
    }

    /**
     * Endpoint for getting all categories for the authenticated user
     *
     * @param authentication the authentication object
     * @return the categories
     */
    @GetMapping
    public ResponseEntity<Iterable<Category>> getMyCategories(Authentication authentication) {
        long userId = Long.parseLong((String) authentication.getPrincipal());
        return ResponseEntity.ok(categoryService.getMyCategories(userId));
    }

    /**
     * Endpoint to delete a specific category based on the category ID for the authenticated user.
     *
     * @param authentication The authentication object containing the user's details.
     * @param categoryId     The unique identifier of the category to be deleted.
     * @return A ResponseEntity with HTTP status OK if deletion was successful, or NOT_FOUND if the category does not exist.
     */
    @DeleteMapping("/{categoryId}")
    public ResponseEntity<?> deleteCategory(Authentication authentication, @PathVariable Long categoryId) {
        long userId = Long.parseLong((String) authentication.getPrincipal());
        categoryService.deleteCategory(categoryId, userId);
        return ResponseEntity.ok().build();
    }
}

package no.ntnu.sparestibackend.controller;

import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import no.ntnu.sparestibackend.service.AuthService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;

/**
 * Controller handling authentication-related endpoints.
 */
@RestController
@RequestMapping("/auth")
public class AuthController {
    @Autowired
    private AuthService authService;

    @Value("${frontend.url}")
    private String frontendUrl;

    @Value("${backend.url}")
    private String backendUrl;

    private Logger logger = LoggerFactory.getLogger(AuthController.class);

    /**
     * Initiates BankID authentication flow by redirecting the user to the BankID service.
     *
     * @param request  The HTTP servlet request.
     * @param response The HTTP servlet response.
     * @return ResponseEntity indicating the success of the redirect.
     * @throws IOException if an I/O error occurs.
     */
    @GetMapping("/bank-id")
    public ResponseEntity<String> loginWithBankId(HttpServletRequest request, HttpServletResponse response) throws IOException {
        logger.info("Attempting to redirect user for BankID authentication");
        String redirectUrl = this.authService.createBankIdAuthenticationURL(response);
        logger.debug("Redirect URL: {}", redirectUrl);
        response.sendRedirect(redirectUrl);
        logger.info("Redirect initiated for BankID login");
        return ResponseEntity.ok("OK");
    }

    /**
     * Consumes the callback from the BankID service after authentication.
     *
     * @param code     The authorization code received from the BankID service.
     * @param request  The HTTP servlet request.
     * @param response The HTTP servlet response.
     * @return ResponseEntity indicating the success of the authentication process.
     * @throws Exception if an error occurs during authentication.
     */
    @GetMapping("/bank-id/callback")
    @ResponseBody
    ResponseEntity<String> consumeCallback(@RequestParam(value = "code",
            required = true) String code, HttpServletRequest request, HttpServletResponse response) throws Exception {
        logger.info("Received callback from BankID with code: {}", code);
        try {
            String token = this.authService.loginOrRegisterUserFromBankIdAuthentication(code);
            logger.debug("Authentication token generated: {}", token);
            String finalRedirectUrl = frontendUrl + "/auth" + "?token=" + token;
            response.sendRedirect(finalRedirectUrl);
            logger.info("User redirected to frontend with token");
        } catch (Exception e) {
            logger.error("Error processing the BankID callback", e);
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Authentication failed");
        }
        return ResponseEntity.ok("OK");
    }
}

package no.ntnu.sparestibackend.controller;

import no.ntnu.sparestibackend.dto.EditTransactionDTO;
import no.ntnu.sparestibackend.dto.TransactionDTO;
import no.ntnu.sparestibackend.dto.transactions.SavingStat;
import no.ntnu.sparestibackend.model.Transaction;
import no.ntnu.sparestibackend.service.TransactionService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;


/**
 * Controller for handling transactions
 */
@RestController
@RequestMapping("/bank-accounts/{bankAccountId}/transactions")
public class TransactionController {
    private final TransactionService transactionService;

    private final Logger logger = LoggerFactory.getLogger(TransactionController.class);

    @Autowired
    public TransactionController(TransactionService transactionService) {
        this.transactionService = transactionService;
    }

    /**
     * Adds a transaction to a bank account if the authenticated user owns the account.
     *
     * @param authentication The security context providing the authenticated user's details.
     * @param bankAccountId  The bank account ID to which the transaction is added.
     * @param transactionDTO The details of the transaction to be added.
     * @return A ResponseEntity containing the created transaction if successful, or an appropriate error status.
     */
    @PostMapping
    public ResponseEntity<Transaction> addTransaction(Authentication authentication, @PathVariable Long bankAccountId, @RequestBody TransactionDTO transactionDTO) {
        long userId = Long.parseLong((String) authentication.getPrincipal());
        logger.info("User {} is attempting to add transaction to bank account ID: {}", userId, bankAccountId);

        if (!transactionService.userOwnsBankAccount(userId, bankAccountId)) {
            logger.warn("Unauthorized attempt by user {} to access bank account {}", userId, bankAccountId);
            return ResponseEntity.status(HttpStatus.FORBIDDEN).build();
        }
        try {
            Transaction result = transactionService.createTransaction(bankAccountId, transactionDTO);
            logger.info("Transaction added successfully to bank account ID: {}", bankAccountId);
            return ResponseEntity.status(HttpStatus.CREATED).body(result);
        } catch (Exception e) {
            logger.error("Error adding transaction to bank account ID: {}: {}", bankAccountId, e.getMessage());
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }

    /**
     * Retrieves a paginated list of transactions for a specified bank account if the authenticated user has ownership
     * of that account. This method checks ownership before retrieving transactions to ensure that the user has the
     * appropriate permissions to view the transactions.
     *
     * @param authentication The security context providing the authenticated user's details, used to verify ownership of the bank account.
     * @param bankAccountId  The unique identifier of the bank account for which transactions are to be retrieved.
     * @param page           The zero-based page index of the transactions to be retrieved, defaulting to 0 if not specified.
     * @param size           The size of the page to be returned, defaulting to 10 if not specified.
     * @return A {@link ResponseEntity} containing a {@link Page} of {@link TransactionDTO} if the user is authorized,
     * status if the user does not have permission to access the transactions of the specified bank account.
     * Each {@link TransactionDTO} includes details such as transaction ID, category ID, date, description, sum, account number,
     * and saving goal ID if applicable. The transactions are sorted and paginated based on the provided page and size parameters.
     */
    @GetMapping()
    public ResponseEntity<Page<Transaction>> getTransactions(Authentication authentication,
                                                             @PathVariable Long bankAccountId,
                                                             @RequestParam(defaultValue = "0") int page,
                                                             @RequestParam(defaultValue = "10") int size) {
        long userId = Long.parseLong((String) authentication.getPrincipal());
        logger.info("User {} is fetching transactions for bank account ID: {} with page {}", userId, bankAccountId, page);

        if (!transactionService.userOwnsBankAccount(userId, bankAccountId)) {
            logger.warn("Unauthorized attempt by user {} to access transactions for bank account {}", userId, bankAccountId);
            return ResponseEntity.status(HttpStatus.FORBIDDEN).build();
        }
        Page<Transaction> transactions = transactionService.getTransactionsByBankAccountNumber(bankAccountId, page, size);
        return ResponseEntity.ok(transactions);
    }

    /**
     * Updates a transaction only if the authenticated user owns the bank account linked to the transaction.
     *
     * @param authentication     The security context providing the authenticated user's details.
     * @param bankAccountId      The bank account ID associated with the transaction.
     * @param transactionId      The transaction ID to update.
     * @param editTransactionDTO The updated transaction details.
     * @return A ResponseEntity containing the updated transaction if successful, or an appropriate error status.
     */
    @PutMapping("/{transactionId}")
    public ResponseEntity<Transaction> updateTransaction(Authentication authentication, @PathVariable Long bankAccountId, @PathVariable Long transactionId, @RequestBody EditTransactionDTO editTransactionDTO) {
        long userId = Long.parseLong((String) authentication.getPrincipal());
        logger.info("User {} is attempting to update transaction ID: {} for bank account ID: {}", userId, transactionId, bankAccountId);

        if (!transactionService.userOwnsBankAccount(userId, bankAccountId)) {
            logger.error("Unauthorized attempt by user {} to update transaction ID: {} for bank account {}", userId, transactionId, bankAccountId);
            return ResponseEntity.status(HttpStatus.FORBIDDEN).build();
        }

        try {
            Transaction updatedTransaction = transactionService.updateTransaction(transactionId, editTransactionDTO);
            logger.info("Transaction ID: {} updated successfully for bank account ID: {}", transactionId, bankAccountId);
            return ResponseEntity.ok(updatedTransaction);
        } catch (Exception e) {
            logger.error("Failed to update transaction ID: {}: {}", transactionId, e.getMessage());
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();
        }
    }

    /**
     * Retrieves a map of savings transactions for each month of the last year.
     *
     * @param authentication The security context providing the authenticated user's details.
     * @return A ResponseEntity containing a HashMap of saving statistics for each month of the last year, or an appropriate error status.
     */
    @GetMapping("last-year")
    public ResponseEntity<HashMap<String, List<SavingStat>>> getSavingsTransactionsForLastYearByEachMonth(Authentication authentication) {
        Long userId = Long.parseLong((String) authentication.getPrincipal());
        logger.info("User {} is fetching savings transactions for the last year by each month", userId);
        return ResponseEntity.status(HttpStatus.OK).body(transactionService.getSavingsTransactionsForLastYearByEachMonth(userId));
    }
}

package no.ntnu.sparestibackend.controller;

import no.ntnu.sparestibackend.service.SavingTipService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * Controller handling saving tip-related endpoints.
 */
@RestController
@RequestMapping("/saving-tip")
public class SavingTipController {

    @Autowired
    private SavingTipService savingTipService;

    /**
     * Generates a saving tip based on the provided category.
     *
     * @param category the category for which to generate a saving tip
     * @return ResponseEntity containing the generated saving tip
     */
    @GetMapping("/generate")
    public ResponseEntity<String> generateSavingTip(@RequestParam String category) {
        String savingTip = savingTipService.getAiGeneratedSavingTips(category);
        return ResponseEntity.ok(savingTip);
    }
}

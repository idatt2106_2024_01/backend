package no.ntnu.sparestibackend.controller;

import no.ntnu.sparestibackend.dto.SavingGoalDTO;
import no.ntnu.sparestibackend.model.SavingGoal;
import no.ntnu.sparestibackend.model.Status;
import no.ntnu.sparestibackend.service.SavingGoalService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

/**
 * Handles web requests related to saving goals. It provides REST APIs that facilitate
 * operations such as the creation of new saving goals.
 * <p>
 * This controller integrates with the {@link SavingGoalService} to perform business
 * logic operations and interact with the database.
 */
@RestController
@RequestMapping("/saving-goals")
public class SavingGoalController {
    private final SavingGoalService savingGoalService;
    private final Logger logger = LoggerFactory.getLogger(SavingGoalController.class);

    /**
     * Instantiates a new SavingGoalController with the specified {@link SavingGoalService}.
     *
     * @param savingGoalService The service layer used to manage saving goals, injected by Spring's IoC container.
     */
    @Autowired
    public SavingGoalController(SavingGoalService savingGoalService) {
        this.savingGoalService = savingGoalService;
    }

    /**
     * Retrieves all saving goals associated with the authenticated user.
     * This endpoint requires authentication and uses the authenticated user's ID
     * to fetch the saving goals.
     *
     * @param authentication The security context providing the authenticated user's details.
     * @return A {@link ResponseEntity} containing a list of {@link SavingGoalDTO} representing the user's saving goals,
     * wrapped in the HTTP response body with an OK status.
     */
    @GetMapping
    public ResponseEntity<List<SavingGoalDTO>> getAllSavingGoalsForAuthenticatedUser(Authentication authentication, @RequestParam(required = false) Status status) {
        long userId = Long.parseLong(authentication.getPrincipal().toString());
        logger.info("Received request to retrieve all saving goals for user {}", userId);

        List<SavingGoal> savingGoals;
        if (status != null) {
            savingGoals = savingGoalService.getAllSavingGoalsForUser(userId, status);
        } else {
            savingGoals = savingGoalService.getAllSavingGoalsForUser(userId);
        }
        List<SavingGoalDTO> savingGoalDTOs = savingGoals.stream()
                .map(goal -> new SavingGoalDTO(goal.getSavingGoalId(), goal.getTitle(), goal.getGoal(), goal.getDeadline(), goal.getStatus()))
                .collect(Collectors.toList());
        return ResponseEntity.ok(savingGoalDTOs);
    }

    /**
     * Endpoint for retrieving the total amount saved across all saving goals associated
     * with the authenticated user. This endpoint requires authentication and uses the
     * authenticated user's ID to calculate the total saved amount across all their saving goals.
     * It aggregates the saved amounts from all relevant transactions linked to the user's saving goals.
     *
     * @param authentication The security context providing the authenticated user's details.
     * @return A {@link ResponseEntity<Double>} containing the total amount saved across all saving goals,
     * wrapped in the HTTP response body with an OK status.
     */
    @GetMapping("/total-saved")
    public ResponseEntity<Double> getTotalSavedForAllGoals(Authentication authentication) {
        long userId = Long.parseLong((String) authentication.getPrincipal());
        logger.info("Received request to get total saved across all saving goals for user {}", userId);
        double totalSaved = savingGoalService.getTotalSavedForAllSavingGoals(userId);
        return ResponseEntity.ok(totalSaved);
    }

    /**
     * Endpoint for retrieving the total amount saved for a specific saving goal.
     *
     * @param savingGoalId The ID of the saving goal.
     * @return A ResponseEntity containing the total amount saved.
     */
    @GetMapping("/{savingGoalId}/total-saved")
    public ResponseEntity<Double> getTotalSaved(@PathVariable Long savingGoalId) {
        logger.info("Received request to get total saved on saving goal with id {}", savingGoalId);
        double totalSaved = savingGoalService.getTotalSavedForSavingGoal(savingGoalId);
        return ResponseEntity.ok(totalSaved);
    }

    /**
     * Creates a new saving goal using the data provided by the client and associates it with
     * the authenticated user. This method requires a valid user authentication context.
     *
     * @param authentication The security context providing the authenticated user's details.
     * @param savingGoalDTO  The data transfer object containing the saving goal details.
     *                       This object is deserialized from the request body.
     * @return A {@link ResponseEntity} object containing the newly created saving goal,
     * wrapped in the HTTP response body with a status of 201 (Created).
     */
    @PostMapping
    public ResponseEntity<SavingGoal> createSavingGoal(Authentication authentication, @RequestBody SavingGoalDTO savingGoalDTO) {
        long userId = Long.parseLong((String) authentication.getPrincipal());
        logger.info("User {} is attempting to create a new saving goal with details: {}", userId, savingGoalDTO);
        return ResponseEntity.status(HttpStatus.CREATED).body(savingGoalService.createSavingGoal(userId, savingGoalDTO));
    }

    /**
     * Updates an existing saving goal with the provided details from the request body. This method requires
     * authentication and the user must be the owner of the saving goal.
     *
     * @param authentication The security context providing the authenticated user's details.
     * @param savingGoalId   The unique identifier of the saving goal to update.
     * @param savingGoalDTO  The data transfer object containing the updated saving goal details.
     * @return A {@link ResponseEntity} containing the updated saving goal if successful, or a not found response if
     * the saving goal does not exist or the user does not have permission to update it.
     */
    @PutMapping("/{savingGoalId}")
    public ResponseEntity<SavingGoalDTO> updateSavingGoal(Authentication authentication, @PathVariable Long savingGoalId, @RequestBody SavingGoalDTO savingGoalDTO) {
        logger.info("Received request to update goal with id {}", savingGoalId);
        long userId = Long.parseLong((String) authentication.getPrincipal());
        SavingGoal updatedGoal = savingGoalService.updateSavingGoal(userId, savingGoalId, savingGoalDTO);
        logger.info("Updated goal successfully {}", updatedGoal);
        return ResponseEntity.ok(SavingGoalDTO.builder()
                .title(updatedGoal.getTitle())
                .goal(updatedGoal.getGoal())
                .deadline(updatedGoal.getDeadline())
                .build());
    }


    /**
     * Deletes an existing saving goal based on the specified identifier. This method requires authentication
     * and the user must be the owner of the saving goal to delete it.
     *
     * @param authentication The security context providing the authenticated user's details.
     * @param savingGoalId   The unique identifier of the saving goal to delete.
     * @return A {@link ResponseEntity} indicating successful deletion with an OK status, or a not found response if
     * the saving goal does not exist or the user does not have permission to delete it.
     */
    @DeleteMapping("/{savingGoalId}")
    public ResponseEntity<Void> deleteSavingGoal(Authentication authentication, @PathVariable Long savingGoalId) {
        long userId = Long.parseLong((String) authentication.getPrincipal());
        logger.info("Attempting to delete saving goal with ID {} for user {}", savingGoalId, userId);
        boolean deleted = savingGoalService.deleteSavingGoal(userId, savingGoalId);
        if (deleted) {
            logger.info("Successfully deleted saving goal with ID {}", savingGoalId);
            return ResponseEntity.ok().build();
        } else {
            return ResponseEntity.notFound().build();
        }
    }
}

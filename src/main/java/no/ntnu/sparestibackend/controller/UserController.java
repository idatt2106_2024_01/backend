package no.ntnu.sparestibackend.controller;

import lombok.RequiredArgsConstructor;
import no.ntnu.sparestibackend.dto.UserBadgeDTO;
import no.ntnu.sparestibackend.dto.UserInfoDTO;
import no.ntnu.sparestibackend.dto.UserStatsDTO;
import no.ntnu.sparestibackend.model.Status;
import no.ntnu.sparestibackend.model.User;
import no.ntnu.sparestibackend.service.SavingChallengeService;
import no.ntnu.sparestibackend.service.SavingGoalService;
import no.ntnu.sparestibackend.service.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;

import static no.ntnu.sparestibackend.model.Difficulty.HARD;

/**
 * Controller for handling user-related operations.
 */
@RequiredArgsConstructor
@RestController
@RequestMapping("/users")
public class UserController {
    private final UserService userService;
    private final Logger logger = LoggerFactory.getLogger(UserController.class);
    private final SavingGoalService savingGoalService;
    private final SavingChallengeService savingChallengeService;

    /**
     * Retrieves information about the currently authenticated user.
     *
     * @param authentication The security context providing the authenticated user's details.
     * @return A {@link ResponseEntity} containing the {@link UserInfoDTO} of the authenticated user.
     */
    @GetMapping("/me")
    public ResponseEntity<UserInfoDTO> getLoggedInUser(Authentication authentication) {
        long userId = Long.parseLong((String) authentication.getPrincipal());
        logger.info("Attempting to get the logged in user: {}", userId);
        UserInfoDTO userInfoDTO = this.userService.getUserInfoDTOByUserId(userId);
        return ResponseEntity.status(HttpStatus.OK).body(userInfoDTO);
    }

    /**
     * Updates the authenticated user's profile information based on the provided {@link UserInfoDTO}.
     *
     * @param authentication The security context providing the authenticated user's details.
     * @param userInfoDTO    The data transfer object containing updated user information.
     * @return A {@link ResponseEntity} indicating the result of the update operation.
     */
    @PutMapping
    public ResponseEntity<UserInfoDTO> updateLoggedInUser(Authentication authentication,
                                                          @RequestBody UserInfoDTO userInfoDTO) {
        Long userId = Long.parseLong((String) authentication.getPrincipal());
        logger.info("Received update request for userId {} with data: {}", userId, userInfoDTO);
        User updatedUser = userService.updateUser(userId, userInfoDTO);

        if (updatedUser != null) {
            UserInfoDTO updatedUserInfoDTO = userService.getUserInfoDTOByUserId(updatedUser.getUserId());
            return ResponseEntity.ok(updatedUserInfoDTO);
        }
        return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
    }

    /**
     * Retrieves a list of all badges (achieved and not achieved) for the authenticated user.
     * This method reads the user ID from the authenticated user's context.
     *
     * @param authentication The security context containing the authenticated user's details.
     * @return A {@link ResponseEntity} containing a list of {@link UserBadgeDTO} for all badges,
     * or a No Content response if no badges are found.
     */
    @GetMapping("/badges/all")
    public ResponseEntity<List<UserBadgeDTO>> getAllBadges(Authentication authentication) {
        Long userId = Long.parseLong((String) authentication.getPrincipal());
        List<UserBadgeDTO> badges = userService.getAllBadges(userId);
        if (badges.isEmpty()) {
            return ResponseEntity.noContent().build();
        }
        logger.info("User {} has {} total badges", userId, badges.size());
        return ResponseEntity.ok(badges);
    }

    /**
     * Retrieves a list of badges that the authenticated user has achieved.
     * This method reads the user ID from the authenticated user's context.
     *
     * @param authentication The security context containing the authenticated user's details.
     * @return A {@link ResponseEntity} containing a list of {@link UserBadgeDTO} if badges are found,
     * or a No Content response if the user has not achieved any badges.
     */
    @GetMapping("/badges/achieved")
    public ResponseEntity<List<UserBadgeDTO>> getAchievedBadges(Authentication authentication) {
        Long userId = Long.parseLong((String) authentication.getPrincipal());
        List<UserBadgeDTO> badges = userService.getAchievedBadges(userId);
        if (badges.isEmpty()) {
            return ResponseEntity.noContent().build();
        }
        logger.info("User {} has achieved {} badges", userId, badges.size());
        return ResponseEntity.ok(badges);
    }

    /**
     * Updates the profile avatar of the currently authenticated user.
     *
     * @param authentication The security context that provides information about the authenticated user.
     * @param profileAvatar  A {@link String} representing the URL or identifier for the new profile avatar.
     * @return A {@link ResponseEntity} with a success message if the update is successful, or a {@link ResponseStatusException}
     * with an HTTP status of {@link HttpStatus#NOT_FOUND} if the user cannot be found.
     * @throws ResponseStatusException if the user is not found or another error occurs during the update process.
     */
    @PatchMapping("/me/profile-avatar")
    public ResponseEntity<?> updateProfileAvatar(Authentication authentication,
                                                 @RequestBody String profileAvatar) {
        Long userId = Long.parseLong((String) authentication.getPrincipal());
        try {
            userService.updateProfileAvatar(userId, profileAvatar);
            return ResponseEntity.ok("Profile Avatar updated successfully");
        } catch (RuntimeException e) {
            logger.error("Error updating profile avatar for user {}: {}", userId, e.getMessage());
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body("User not found");
        }

    }

    /**
     * Updates the newsletter consent status of the currently authenticated user.
     *
     * @param authentication    The security context that provides information about the auth user.
     * @param newsletterConsent A {@link Boolean} value indicating the user's consent status.
     * @return A {@link ResponseEntity} with a success message if the update is successful.
     */
    @PatchMapping("/me/newsletter-consent")
    public ResponseEntity<?> updateNewsletterConsent(Authentication authentication,
                                                     @RequestBody boolean newsletterConsent) {
        Long userId = Long.parseLong((String) authentication.getPrincipal());
        try {
            userService.updateNewsletterConsent(userId, newsletterConsent);
            return ResponseEntity.ok("Newsletter consent updated successfully");
        } catch (RuntimeException e) {
            logger.error("Error updating newsletter consent ofr user {}: {}", userId, e.getMessage());
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body("User not found");
        }
    }

    /**
     * Retrieves statistical data about the user's savings, challenge completions, and streaks.
     *
     * @param authentication The security context providing the authenticated user's details.
     * @return ResponseEntity containing {@link UserStatsDTO} with the total savings amount,
     * number of hard challenges completed, and the longest streak of successful challenges.
     */
    @GetMapping("/stats")
    public ResponseEntity<UserStatsDTO> getUserStats(Authentication authentication) {
        Long userId = Long.parseLong((String) authentication.getPrincipal());
        double totalSavings = savingGoalService.getTotalSavedForAllSavingGoals(userId);
        int hardChallengesCompleted =
                savingChallengeService.getCompletedChallengesCount(userId, HARD, Status.COMPLETED);
        int successfulStreak = savingChallengeService.calculateLongestSuccessStreak(userId);

        UserStatsDTO stats = new UserStatsDTO(totalSavings, hardChallengesCompleted, successfulStreak);
        return ResponseEntity.ok(stats);
    }
}

package no.ntnu.sparestibackend.controller;

import no.ntnu.sparestibackend.dto.UserPreferencesDTO;
import no.ntnu.sparestibackend.model.UserPreferences;
import no.ntnu.sparestibackend.service.UserPreferencesService;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.*;

/**
 * REST Controller for managing user preferences.
 * Provides endpoints for updating user preferences, saving levels, willingness to invest, and adding categories.
 * Uses the UserPreferencesService to handle business logic and data persistence.
 */
@RestController
@RequestMapping("/users/user-preferences")
public class UserPreferencesController {

    private final UserPreferencesService userPreferencesService;

    /**
     * Constructor to initialize the controller with the UserPreferencesService.
     *
     * @param userPreferencesService The service that handles user preferences operations.
     */
    public UserPreferencesController(UserPreferencesService userPreferencesService) {
        this.userPreferencesService = userPreferencesService;
    }

    /**
     * Endpoint to update user preferences.
     * Accepts a UserPreferencesDTO with the updated information and updates the corresponding UserPreferences.
     *
     * @param authentication     The authentication object containing information about the authenticated user.
     * @param userPreferencesDTO The DTO containing the updated preferences.
     * @return The updated UserPreferences wrapped in a ResponseEntity.
     */
    @PutMapping
    public ResponseEntity<UserPreferences> updateUserPreferences(
            Authentication authentication,
            @RequestBody UserPreferencesDTO userPreferencesDTO
    ) {
        Long userId = Long.parseLong((String) authentication.getPrincipal());
        UserPreferences updatedUserPreferences = userPreferencesService.updateUserPreferences(userId, userPreferencesDTO);
        return ResponseEntity.ok(updatedUserPreferences);
    }


    /**
     * Endpoint to add a category to user preferences.
     * Takes user preferences ID and category ID, and adds the category to the user preferences.
     *
     * @param userPreferencesId The ID of the user preferences to update.
     * @param categoryId        The ID of the category to add.
     * @return The updated UserPreferences wrapped in a ResponseEntity.
     */
    @PostMapping("/{userPreferencesId}/add-category/{categoryId}")
    public ResponseEntity<UserPreferences> addCategory(
            @PathVariable Long userPreferencesId,
            @PathVariable Long categoryId
    ) {
        UserPreferences updatedUserPreferences = userPreferencesService.addCategoryToUserPreferences(userPreferencesId, categoryId);
        return ResponseEntity.ok(updatedUserPreferences);
    }
}

package no.ntnu.sparestibackend.controller;

import lombok.RequiredArgsConstructor;
import no.ntnu.sparestibackend.dto.BankAccountDTO;
import no.ntnu.sparestibackend.service.BankAccountService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.*;

/**
 * Controller for handling bank account related operations.
 */
@RestController
@RequestMapping("/bank-accounts")
@RequiredArgsConstructor
public class BankAccountController {
    private final BankAccountService bankAccountService;
    private final Logger logger = LoggerFactory.getLogger(BankAccountController.class);

    /**
     * Creates a new bank account for the authenticated user.
     *
     * @param authentication the security context providing the authenticated user's details.
     * @param bankAccountDTO the bank account data transfer object containing the new account details.
     * @return a {@link ResponseEntity} containing the created {@link BankAccountDTO}.
     */
    @PostMapping("/integrate")
    public ResponseEntity<BankAccountDTO> integrateWithBankAccount(Authentication authentication, @RequestBody BankAccountDTO bankAccountDTO) {
        logger.info("Received request to integrate with bank account");
        Long userId = Long.parseLong((String) authentication.getPrincipal());
        BankAccountDTO createdBankAccountDTO = this.bankAccountService.integrateWithBankAccount(userId, bankAccountDTO);
        logger.info("Successfully added bank account");
        return ResponseEntity.status(HttpStatus.CREATED).body(createdBankAccountDTO);
    }

    /**
     * Updates an existing bank account.
     *
     * @param accountNumber  the unique identifier of the bank account to be updated.
     * @param bankAccountDTO the bank account data transfer object containing the updated details.
     * @return a {@link ResponseEntity} containing the updated {@link BankAccountDTO}.
     */
    @PutMapping("/{accountNumber}")
    public ResponseEntity<BankAccountDTO> updateBankAccount(@PathVariable Long accountNumber, @RequestBody BankAccountDTO bankAccountDTO) {
        logger.info("Received request to update bank account with account number: {}", accountNumber);
        BankAccountDTO updatedBankAccountDTO = bankAccountService.updateBankAccount(accountNumber, bankAccountDTO);
        logger.info("Successfully updated bank account");
        return ResponseEntity.ok(updatedBankAccountDTO);
    }
}


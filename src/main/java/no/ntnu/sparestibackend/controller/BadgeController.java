package no.ntnu.sparestibackend.controller;

import lombok.RequiredArgsConstructor;
import no.ntnu.sparestibackend.model.Badge;
import no.ntnu.sparestibackend.service.BadgeService;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/badges")
@RequiredArgsConstructor
public class BadgeController {
    private final BadgeService badgeService;

    /**
     * Endpoint to evaluate and assign badges for the authenticated user.
     *
     * @param authentication The security context providing the authenticated user's details.
     * @return ResponseEntity indicating the process completion.
     */
    @GetMapping("/evaluate")
    public ResponseEntity<String> evaluateAndAssignBadges(Authentication authentication) {
        long userId = Long.parseLong((String) authentication.getPrincipal());
        badgeService.evaluateAndAssignBadges(userId);
        return ResponseEntity.ok("Badge evaluation and assignment process completed successfully.");
    }

    /**
     * Endpoint to retrieve all badges.
     *
     * @return ResponseEntity containing a list of all badges.
     */
    @GetMapping
    public ResponseEntity<List<Badge>> getAllBadges() {
        List<Badge> badges = badgeService.getAllBadges();
        return ResponseEntity.ok(badges);
    }
}

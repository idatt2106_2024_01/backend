package no.ntnu.sparestibackend;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SparestiBackendApplication {

    public static void main(String[] args) {
        SpringApplication.run(SparestiBackendApplication.class, args);
    }

}

package no.ntnu.sparestibackend.service;

import lombok.RequiredArgsConstructor;
import no.ntnu.sparestibackend.model.Badge;
import no.ntnu.sparestibackend.model.Status;
import no.ntnu.sparestibackend.model.UserBadge;
import no.ntnu.sparestibackend.repository.BadgeRepository;
import no.ntnu.sparestibackend.repository.SavingChallengeRepository;
import no.ntnu.sparestibackend.repository.UserBadgeRepository;
import no.ntnu.sparestibackend.repository.UserRepository;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

import static no.ntnu.sparestibackend.model.Difficulty.HARD;

/**
 * Service class for managing badges and their assignment to users.
 */
@Service
@RequiredArgsConstructor
public class BadgeService {
    private final BadgeRepository badgeRepository;
    private final UserBadgeRepository userBadgeRepository;
    private final SavingChallengeRepository savingChallengeRepository;
    private final SavingGoalService savingGoalService;
    private final SavingChallengeService savingChallengeService;
    private final UserRepository userRepository;

    /**
     * Evaluates the criteria for each badge and assigns badges to the specified user if criteria are met.
     *
     * @param userId The ID of the user for whom badges are to be evaluated and assigned.
     */
    public void evaluateAndAssignBadges(Long userId) {
        List<Badge> allBadges = badgeRepository.findAll();
        double totalSavings = savingGoalService.getTotalSavedForAllSavingGoals(userId);
        int hardChallengesCompleted = savingChallengeService.getCompletedChallengesCount(userId, HARD, Status.COMPLETED);
        int successfulStreak = savingChallengeService.calculateLongestSuccessStreak(userId);

        List<UserBadge> userBadges = userBadgeRepository.findByUser_UserId(userId);

        for (Badge badge : allBadges) {
            boolean isAchieved = checkBadgeCriteria(badge, totalSavings, hardChallengesCompleted, successfulStreak);
            UserBadge userBadge = userBadges.stream()
                    .filter(ub -> ub.getBadge().equals(badge))
                    .findFirst()
                    .orElse(null);

            if (isAchieved && (userBadge == null || !userBadge.isAchieved())) {
                if (userBadge == null) {
                    userBadge = new UserBadge();
                    userBadge.setUser(userRepository.findByUserId(userId));
                    userBadge.setBadge(badge);
                    userBadge.setAchieved(false);
                    userBadges.add(userBadge);
                }
                userBadge.setAchieved(true);
                userBadge.setDateAchieved(new Date());
                userBadgeRepository.save(userBadge);
            }
        }
    }

    /**
     * Checks the criteria for a specific badge based on the user's savings, completed hard challenges, and successful streak.
     *
     * @param badge          The badge to check the criteria for.
     * @param totalSavings   The total savings of the user.
     * @param hardChallenges The number of hard challenges completed by the user.
     * @param streak         The user's longest successful streak.
     * @return True if the badge criteria are met, otherwise false.
     */
    public boolean checkBadgeCriteria(Badge badge, double totalSavings, int hardChallenges, int streak) {
        return switch (badge.getName()) {
            case "Spart 500 kr" -> totalSavings >= 500;
            case "Spart 1000 kr" -> totalSavings >= 1000;
            case "Spart 10 000 kr" -> totalSavings >= 10000;
            case "Spart 100 000 kr" -> totalSavings >= 100000;
            case "1 vanskelig utfordring" -> hardChallenges >= 1;
            case "3 vanskelige utfordringer" -> hardChallenges >= 3;
            case "5 vanskelige utfordringer" -> hardChallenges >= 5;
            case "10 vanskelige utfordringer" -> hardChallenges >= 10;
            case "3 utfordringer på rad" -> streak >= 3;
            case "5 utfordringer på rad" -> streak >= 5;
            case "15 utfordringer på rad" -> streak >= 15;
            case "50 utfordringer på rad" -> streak >= 50;
            default -> false;
        };
    }

    /**
     * Retrieves all badges from the repository
     *
     * @return List of badges.
     */
    public List<Badge> getAllBadges() {
        return badgeRepository.findAll();
    }
}

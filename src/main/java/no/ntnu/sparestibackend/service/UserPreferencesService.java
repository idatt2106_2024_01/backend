package no.ntnu.sparestibackend.service;

import no.ntnu.sparestibackend.dto.UserPreferencesDTO;
import no.ntnu.sparestibackend.model.Category;
import no.ntnu.sparestibackend.model.IntensityLevel;
import no.ntnu.sparestibackend.model.User;
import no.ntnu.sparestibackend.model.UserPreferences;
import no.ntnu.sparestibackend.repository.CategoryRepository;
import no.ntnu.sparestibackend.repository.UserPreferencesRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.Set;

/**
 * Service class for managing user preferences.
 * It handles creating, updating, and modifying user preferences including saving level, willingness to invest, and related categories.
 * Also provides logging for significant events and error handling.
 */
@Service
public class UserPreferencesService {

    private static final Logger logger = LoggerFactory.getLogger(UserPreferencesService.class);


    private final UserPreferencesRepository userPreferencesRepository;
    private final CategoryRepository categoryRepository;

    /**
     * Constructs the service with the required repositories.
     *
     * @param userPreferencesRepository Repository for user preferences data.
     * @param categoryRepository        Repository for category data.
     */
    public UserPreferencesService(UserPreferencesRepository userPreferencesRepository,
                                  CategoryRepository categoryRepository) {
        this.userPreferencesRepository = userPreferencesRepository;
        this.categoryRepository = categoryRepository;
    }

    /**
     * Retrieves the user preferences by user ID.
     *
     * @param userId the ID of the user
     * @return the user preferences for the specified user
     */
    public UserPreferences getUserPreferencesByUserId(Long userId) {
        return userPreferencesRepository.findByUserUserId(userId)
                .orElseThrow(() -> new IllegalArgumentException("User preferences with ID " + userId + " not found"));
    }

    /**
     * Creates default user preferences for a new user.
     *
     * @param user               the new user
     * @param categoriesToSaveOn the categories to save on
     */
    public void createUserPreferenceForNewUser(User user, HashSet<Category> categoriesToSaveOn) {
        UserPreferences userPreferences = UserPreferences.builder()
                .savingLevel(IntensityLevel.SOME)
                .willingnessToInvest(IntensityLevel.SOME)
                .theme("theme1")
                .savingCategories(categoriesToSaveOn)
                .user(user)
                .build();
        userPreferencesRepository.save(userPreferences);
    }


    /**
     * Updates user preferences based on the provided DTO.
     *
     * @param userId             The ID of the authenticated user.
     * @param userPreferencesDTO The data transfer object containing updated preference information.
     * @return The updated UserPreferences object.
     * @throws IllegalArgumentException if the user preferences or category doesn't exist.
     */
    public UserPreferences updateUserPreferences(Long userId, UserPreferencesDTO userPreferencesDTO) {
        UserPreferences userPreferences = userPreferencesRepository.findByUserUserId(userId)
                .orElseThrow(() -> new IllegalArgumentException("User preferences with user_id " + userId + " not found"));

        if (userPreferencesDTO.savingLevel() != null) {
            userPreferences.setSavingLevel(userPreferencesDTO.savingLevel());
        }
        if (userPreferencesDTO.willingnessToInvest() != null) {
            userPreferences.setWillingnessToInvest(userPreferencesDTO.willingnessToInvest());
        }
        if (userPreferencesDTO.theme() != null) {
            userPreferences.setTheme(userPreferencesDTO.theme());
        }
        if (userPreferencesDTO.nickname() != null) {
            userPreferences.setNickname(userPreferencesDTO.nickname());
        }

        Set<Category> categories = new HashSet<>();
        for (Long categoryId : userPreferencesDTO.savingCategoryIds()) {
            Category category = categoryRepository.findById(categoryId)
                    .orElseThrow(() -> new IllegalArgumentException("Category with ID " + categoryId + " not found"));
            categories.add(category);
        }

        userPreferences.setSavingCategories(categories);

        return userPreferencesRepository.save(userPreferences);
    }


    /**
     * Adds a category to the user preferences.
     *
     * @param userPreferencesId The ID of the user preferences to update.
     * @param categoryId        The ID of the category to add.
     * @return The updated UserPreferences object.
     * @throws IllegalArgumentException if the user preferences or category doesn't exist.
     */
    public UserPreferences addCategoryToUserPreferences(Long userPreferencesId, Long categoryId) {
        UserPreferences userPreferences = userPreferencesRepository.findById(userPreferencesId)
                .orElseThrow(() -> {
                    logger.warn("User preferences with ID {} not found", userPreferencesId);
                    return new IllegalArgumentException("User preferences with ID " + userPreferencesId + " not found");
                });

        Category category = categoryRepository.findById(categoryId)
                .orElseThrow(() -> {
                    logger.warn("Category with ID {} not found", categoryId);
                    return new IllegalArgumentException("Category with ID " + categoryId + " not found");
                });

        if (userPreferences.getSavingCategories() == null) {
            userPreferences.setSavingCategories(new HashSet<>());
        }

        if (!userPreferences.getSavingCategories().contains(category)) {
            userPreferences.getSavingCategories().add(category);
            logger.info("Added category ID {} to userPreferencesId {}", categoryId, userPreferencesId);
        } else {
            logger.info("Category ID {} already exists in userPreferencesId {}", categoryId, userPreferencesId);
        }

        return userPreferencesRepository.save(userPreferences);
    }

    /**
     * Deletes a category from the user preferences.
     *
     * @param userId     the ID of the user
     * @param categoryId the ID of the category to delete
     * @throws IllegalArgumentException if the user preferences or category doesn't exist
     */
    public void deleteCategoryFromUserPreferences(Long userId, Long categoryId) {
        UserPreferences userPreferences = userPreferencesRepository.findByUserUserId(userId)
                .orElseThrow(() -> {
                    logger.warn("User preferences with user_id {} not found", userId);
                    return new IllegalArgumentException("User preferences with user_id " + userId + " not found");
                });

        Category category = categoryRepository.findById(categoryId)
                .orElseThrow(() -> {
                    logger.warn("Category with ID {} not found", categoryId);
                    return new IllegalArgumentException("Category with ID " + categoryId + " not found");
                });

        if (userPreferences.getSavingCategories() == null) {
            userPreferences.setSavingCategories(new HashSet<>());
        }

        if (userPreferences.getSavingCategories().contains(category)) {
            userPreferences.getSavingCategories().remove(category);
            logger.info("Removed category ID {} from userPreferencesId {}", categoryId, userId);
        } else {
            logger.info("Category ID {} does not exist in userPreferencesId {}", categoryId, userId);
        }

        userPreferencesRepository.save(userPreferences);
    }
}

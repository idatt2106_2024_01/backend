package no.ntnu.sparestibackend.service;

import jakarta.persistence.EntityManager;
import jakarta.persistence.EntityNotFoundException;
import lombok.RequiredArgsConstructor;
import no.ntnu.sparestibackend.dto.EditTransactionDTO;
import no.ntnu.sparestibackend.dto.TransactionDTO;
import no.ntnu.sparestibackend.dto.transactions.SavingStat;
import no.ntnu.sparestibackend.mapper.TransactionMapper;
import no.ntnu.sparestibackend.model.*;
import no.ntnu.sparestibackend.repository.BankAccountRepository;
import no.ntnu.sparestibackend.repository.TransactionRepository;
import no.ntnu.sparestibackend.utils.TransactionGenerator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.text.SimpleDateFormat;
import java.util.*;

/**
 * Service class for handling transactions
 */
@Service
@RequiredArgsConstructor
public class TransactionService {
    private final TransactionRepository transactionRepository;
    private final EntityManager entityManager;
    private final TransactionMapper transactionMapper;
    private final BankAccountRepository bankAccountRepository;
    private final CategoryService categoryService;

    private final Logger logger = LoggerFactory.getLogger(TransactionService.class);

    /**
     * Creates a transaction.
     *
     * @param bankAccountNumber the bank account number
     * @param transactionDTO    the transaction to create
     * @return the created transaction
     */
    public Transaction createTransaction(Long bankAccountNumber, TransactionDTO transactionDTO) {
        logger.info("Attempting to find BankAccount with ID: {}", bankAccountNumber);
        BankAccount bankAccount = entityManager.find(BankAccount.class, bankAccountNumber);


        if (bankAccount == null) {
            logger.error("Bank account with ID {} not found", bankAccountNumber);
            throw new EntityNotFoundException("Bank account with ID " + bankAccountNumber + " not found");
        }

        Transaction transaction = new Transaction();
        transaction.setBankAccount(bankAccount);
        transaction.setSum(transactionDTO.sum());
        transaction.setDate(transactionDTO.date());
        transaction.setDescription(transactionDTO.description());

        if (transactionDTO.categoryId() != null) {
            logger.info("Attempting to find Category with ID: {}", transactionDTO.categoryId());
            Category category = entityManager.find(Category.class, transactionDTO.categoryId());
            transaction.setCategory(category);
        }

        if (transactionDTO.savingGoalId() != null) {
            logger.info("Attempting to find SavingGoal with ID: {}", transactionDTO.savingGoalId());
            SavingGoal savingGoal = entityManager.find(SavingGoal.class, transactionDTO.savingGoalId());
            transaction.setSavingGoal(savingGoal);
        }
        Transaction savedTransaction = transactionRepository.save(transaction);
        logger.info("Transaction created successfully with ID {}", savedTransaction.getTransactionId());
        return savedTransaction;
    }

    /**
     * Retrieves a paginated list of transactions associated with a specific bank account.
     * This method uses pagination information to fetch a subset of transaction records.
     *
     * @param bankAccountNumber the unique identifier of the bank account for which transactions are to be retrieved
     * @param page              the zero-based page index of the transactions to be retrieved
     * @param size              the size of the page to be returned
     * @return a {@link Page} of {@link TransactionDTO} objects containing the transactions for the specified bank account
     * within the provided pageable constraints. Each {@link TransactionDTO} includes transaction details such as
     * transaction ID, category ID, transaction date, description, amount, account number, and associated saving goal ID.
     * This method returns a page of transactions sorted and paginated according to the {@link Pageable} instance created
     * from the page and size parameters.
     */

    public Page<Transaction> getTransactionsByBankAccountNumber(Long bankAccountNumber, int page, int size) {
        logger.info("Retrieving transactions for bank account ID: {}", bankAccountNumber);
        Page<Transaction> transactions = transactionRepository.findByBankAccountAccountNumberOrderByDateDesc(bankAccountNumber, PageRequest.of(page, size));
        logger.info("Retrieved {} transactions for bank account ID: {}", transactions.getTotalElements(), bankAccountNumber);
        return transactions;
    }

    /**
     * Retrieves transactions for a specific bank account and category within the last 28 days.
     *
     * @param bankAccountNumber the unique identifier of the bank account
     * @param categoryId        the ID of the category
     * @return a list of transactions that match the criteria
     */
    public List<Transaction> getTransactionsByBankAccountNumberAndCategoriesForLast28Days(Long bankAccountNumber, Long categoryId) {
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.DATE, -28);
        Date thirtyDaysAgo = calendar.getTime();
        logger.info("Retrieving transactions for bank account ID: {}", bankAccountNumber);
        List<Transaction> transactions = transactionRepository.findByBankAccount_AccountNumberAndCategory_CategoryIdAndDateAfter(bankAccountNumber, categoryId, thirtyDaysAgo);
        logger.info("Retrieved {} transactions for bank account ID: {}", transactions.size(), bankAccountNumber);
        return transactions;
    }

    /**
     * Retrieves transactions by category ID and date interval.
     *
     * @param categoryId the ID of the category
     * @param fromDate   the start date
     * @param toDate     the end date
     * @return a list of transactions that match the criteria
     */
    public List<Transaction> getTransactionsByUserIdCategoryIdAndDateInterval(Long categoryId, Date fromDate, Date toDate) {
        logger.info("Retrieving transactions for category ID: {} between dates {} and {}", categoryId, fromDate, toDate);
        List<Transaction> transactions = transactionRepository.findByCategory_CategoryIdAndDateBetween(categoryId, fromDate, toDate);
        logger.info("Retrieved {} transactions for category ID: {} between dates {} and {}", transactions.size(), categoryId, fromDate);
        return transactions;
    }


    /**
     * Updates a transaction
     *
     * @param transactionId      the transaction id
     * @param editTransactionDTO the updated transaction
     * @return the updated transaction
     */
    public Transaction updateTransaction(Long transactionId, EditTransactionDTO editTransactionDTO) {
        logger.info("Updating transaction ID: {}", transactionId);
        Transaction transaction = entityManager.find(Transaction.class, transactionId);
        Category newCategory = entityManager.find(Category.class, editTransactionDTO.categoryId());

        if (newCategory == null) {
            logger.error("Category with ID {} not found during update", editTransactionDTO.categoryId());
            throw new IllegalArgumentException("Category with ID " + editTransactionDTO.categoryId() + " not found");
        }

        transaction.setDescription(editTransactionDTO.description());
        transaction.setCategory(newCategory);
        logger.info("Transaction ID: {} updated successfully", transactionId);
        return transactionRepository.save(transaction);
    }

    /**
     * Checks if a user owns a specific bank account.
     *
     * @param userId        The ID of the user to check.
     * @param accountNumber The account number of the bank account to check against.
     * @return true if the user is the owner of the bank account, otherwise false.
     */
    public boolean userOwnsBankAccount(long userId, long accountNumber) {
        logger.info("Checking ownership for user ID: {} on bank account ID: {}", userId, accountNumber);
        BankAccount bankAccount = bankAccountRepository.findByAccountNumber(accountNumber);
        if (bankAccount == null) {
            logger.error("Bank account ID: {} not found", accountNumber);
            return false;
        }
        boolean owns = bankAccount.getUser().getUserId() == userId;
        logger.info("Ownership check for user ID: {} on bank account ID: {} returned {}", userId, accountNumber, owns);
        return owns;
    }

    /**
     * Method to populate a bank account with transactions
     *
     * @param bankAccount the bank account to populate
     */
    public void populateBankAccountWithTransactions(BankAccount bankAccount) {
        List<Category> categories = this.categoryService.getMyCategories(bankAccount.getUser().getUserId());
        logger.info("Populating bank account with transactions");
        List<Transaction> transactions = TransactionGenerator.generateRandomTransactions(bankAccount, categories);
        transactionRepository.saveAll(transactions);
    }

    /**
     * Retrieves savings transactions for the last year grouped by each month.
     *
     * @param userId the ID of the user
     * @return a map where the key is "all" or "onlyFromSavingGoal", and the value is a list of SavingStat objects
     */
    public HashMap<String, List<SavingStat>> getSavingsTransactionsForLastYearByEachMonth(Long userId) {
        BankAccount savingsAccount = bankAccountRepository.findByUserUserIdAndAccountType(userId, AccountType.SAVINGS);
        if (savingsAccount == null) {
            logger.error("No savings account found for user ID: {}", userId);
            throw new EntityNotFoundException("No savings account found for user ID: " + userId);
        }

        Calendar calendar = Calendar.getInstance();
        Date currentDate = calendar.getTime();
        Date oneYearAgo = new Date(currentDate.getTime() - 31556952000L); // 1 year in milliseconds

        List<Transaction> transactions = transactionRepository.findByBankAccount_AccountNumberAndDateBetween(savingsAccount
                .getAccountNumber(), oneYearAgo, currentDate);

        Map<String, Double> monthlyTotal = new LinkedHashMap<>();
        Map<String, Double> monthlyGoalTotal = new LinkedHashMap<>();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM");

        Calendar start = Calendar.getInstance();
        start.setTime(currentDate);
        for (int i = 12; i >= 0; i--) {
            String monthKey = sdf.format(start.getTime());
            monthlyTotal.put(monthKey, 0.0);
            monthlyGoalTotal.put(monthKey, 0.0);
            start.add(Calendar.MONTH, -1);
        }

        // Group transactions by month and if saving goal is present
        for (Transaction transaction : transactions) {
            String monthKey = sdf.format(transaction.getDate());
            monthlyTotal.put(monthKey, monthlyTotal.get(monthKey) + transaction.getSum());

            if (transaction.getSavingGoal() != null) {
                monthlyGoalTotal.put(monthKey, monthlyGoalTotal.get(monthKey) + transaction.getSum());
            }
        }

        List<SavingStat> savingStats = new ArrayList<>();
        List<SavingStat> savingStatsOnlyFromSavingGoal = new ArrayList<>();

        for (String month : monthlyTotal.keySet()) {
            savingStats.add(SavingStat.builder()
                    .totalSaved(monthlyTotal.get(month))
                    .yearAndMonth(month)
                    .build());

            double totalGoal = monthlyGoalTotal.getOrDefault(month, 0.0);
            savingStatsOnlyFromSavingGoal.add(SavingStat.builder()
                    .totalSaved(totalGoal)
                    .yearAndMonth(month)
                    .build());
        }

        HashMap<String, List<SavingStat>> savingsStats = new HashMap<>();
        savingsStats.put("all", savingStats);
        savingsStats.put("onlyFromSavingGoal", savingStatsOnlyFromSavingGoal);
        return savingsStats;
    }
}

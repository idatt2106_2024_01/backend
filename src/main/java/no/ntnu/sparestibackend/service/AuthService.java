package no.ntnu.sparestibackend.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import jakarta.servlet.http.HttpServletResponse;
import lombok.RequiredArgsConstructor;
import no.ntnu.sparestibackend.dto.auth.BankIdTokenResponse;
import no.ntnu.sparestibackend.dto.auth.BankIdUserInfoResponse;
import no.ntnu.sparestibackend.model.Category;
import no.ntnu.sparestibackend.model.User;
import no.ntnu.sparestibackend.repository.UserRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.Base64;
import java.util.HashSet;

/**
 * Service class for authentication related operations.
 */
@Service
@RequiredArgsConstructor
public class AuthService {
    private final TokenService tokenService;
    private final UserRepository userRepository;
    private final UserPreferencesService userPreferencesService;
    private final CategoryService categoryService;

    private final Logger logger = LoggerFactory.getLogger(AuthService.class);
    private final String SIGNICAT_URL = "https://spare-sti.sandbox.signicat.com";

    @Value("${signicat.client_id}")
    private String CLIENT_ID;

    @Value("${backend.url}")
    private String REDIRECT_URL;

    /**
     * Generates the BankID authentication URL.
     *
     * @param response HttpServletResponse object
     * @return BankID authentication URL
     * @throws IOException if an error occurs
     */
    public String createBankIdAuthenticationURL(HttpServletResponse response) throws IOException {
        String RESPONSE_TYPE = "code";
        String SCOPE = "openid+profile+email";
        String PROMPT = "login";
        String ACR_VALUES = "idp:nbid+nbid_idp:BID";

        return SIGNICAT_URL + "/auth/open/connect/authorize?client_id=" + CLIENT_ID
                + "&response_type=" + RESPONSE_TYPE
                + "&scope=" + SCOPE
                + "&prompt=" + PROMPT
                + "&acr_values=" + ACR_VALUES
                + "&redirect_uri=" + REDIRECT_URL + "/auth/bank-id/callback";
    }

    /**
     * Performs login or registration of a user using BankID authentication.
     *
     * @param code Authorization code received from BankID authentication
     * @return Authentication token
     * @throws JsonProcessingException if an error occurs during JSON processing
     */
    public String loginOrRegisterUserFromBankIdAuthentication(String code)
            throws JsonProcessingException {
        logger.info("Received request to login or register user from BankID authentication");
        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        String auth = "sandbox-combative-shoe-333" + ":"
                + "5R7xQQQo2ZdsEGp0ZzDcC1SkiAxSVkWMKugNiH3UkMJ1zxDI";
        String encodedAuth = Base64.getEncoder().encodeToString(auth.getBytes(StandardCharsets.UTF_8));
        String authHeader = "Basic " + encodedAuth;

        WebClient webClient = WebClient.create();
        String tokenResponse = webClient.post()
                .uri(SIGNICAT_URL + "/auth/open/connect/token")
                .header("Content-Type", "application/x-www-form-urlencoded")
                .header("Authorization", authHeader)
                .bodyValue("grant_type=authorization_code&code=" + code + "&redirect_uri=" + REDIRECT_URL + "/auth/bank-id/callback")
                .retrieve()
                .bodyToMono(String.class).block();

        BankIdTokenResponse bankIdTokenResponse = objectMapper.readValue(tokenResponse, BankIdTokenResponse.class);
        logger.info("Received token response from BankID authentication: " + tokenResponse);

        String userInfoResponse = webClient.post()
                .uri(SIGNICAT_URL + "/auth/open/connect/userinfo")
                .header("Accept", "application/json")
                .header("Authorization", "Bearer " + bankIdTokenResponse.accessToken())
                .retrieve()
                .bodyToMono(String.class).block();

        logger.info("Received user info response from BankID authentication: " + userInfoResponse);

        // Create user with the data from the userInfoResponse, given_name, family_name, idp_id
        BankIdUserInfoResponse bankIdUserInfoResponse = objectMapper.readValue(userInfoResponse, BankIdUserInfoResponse.class);
        User user = this.userRepository.findByIdpId(bankIdUserInfoResponse.idpId());

        // Create user if it does not exist
        if (user == null) {
            logger.info("User does not exist, creating new user with idp_id: {}", bankIdUserInfoResponse.idpId());
            User newUser = User.builder()
                    .idpId(bankIdUserInfoResponse.idpId())
                    .firstName(bankIdUserInfoResponse.givenName())
                    .lastName(bankIdUserInfoResponse.familyName())
                    .build();
            user = this.userRepository.save(newUser);
            HashSet<Category> categoriesToSaveOn = new HashSet<>(this.categoryService.populateNewUserWithDefaultCategories(user));
            this.userPreferencesService.createUserPreferenceForNewUser(user, categoriesToSaveOn);
        }

        logger.info("User successfully logged in or registered from BankID authentication");
        return this.tokenService.generateToken(user.getUserId());
    }
}

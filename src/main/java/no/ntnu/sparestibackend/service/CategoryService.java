package no.ntnu.sparestibackend.service;

import lombok.RequiredArgsConstructor;
import no.ntnu.sparestibackend.dto.CategoryDTO;
import no.ntnu.sparestibackend.model.Category;
import no.ntnu.sparestibackend.model.User;
import no.ntnu.sparestibackend.repository.CategoryRepository;
import no.ntnu.sparestibackend.utils.CategoryDefaults;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import java.util.ArrayList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class CategoryService {
    private final CategoryRepository categoryRepository;
    private final UserService userService;
    private final UserPreferencesService userPreferencesService;


    /**
     * Creates a new category for a user
     *
     * @param userId
     * @param categoryDTO
     * @return the created category
     */
    public Category createCategory(Long userId, CategoryDTO categoryDTO) {
        User user = userService.getUserByUserId(userId);

        if (categoryRepository.existsByUserAndName(user, categoryDTO.name())) {
            throw new IllegalArgumentException("Category with name " + categoryDTO.name() + " already exists");
        }

        Category newCategory = new Category();
        newCategory.setUser(user);
        newCategory.setName(categoryDTO.name());
        newCategory.setImg(categoryDTO.img());

        return categoryRepository.save(newCategory);
    }


    /**
     * Gets all categories for a user
     *
     * @param userId
     * @return the categories
     */
    public List<Category> getMyCategories(Long userId) {
        return categoryRepository.findCategoriesByUserUserId(userId);
    }

    public List<Category> populateNewUserWithDefaultCategories(User user) {
        System.out.println("Populating new user with default categories");
        List<Category> defaultCategories = List.of(CategoryDefaults.getDefaultCategories(user));
        List<Category> newCategories = new ArrayList<>();
        for (Category defaultCategory : defaultCategories) {
            Category newCategory = new Category();
            newCategory.setUser(user);
            newCategory.setName(defaultCategory.getName());
            newCategory.setImg(defaultCategory.getImg());
            newCategories.add(newCategory);
        }
        categoryRepository.saveAll(newCategories);
        return newCategories;
    }

    /**
     * Deletes a category
     *
     * @param categoryId The ID of the category to delete
     * @param userId     The ID of the user
     */
    public void deleteCategory(Long categoryId, Long userId) {
        userPreferencesService.deleteCategoryFromUserPreferences(userId, categoryId);
        if (!categoryRepository.existsByCategoryIdAndUserUserId(categoryId, userId)) {
            throw new ResponseStatusException(HttpStatus.FORBIDDEN, "Can not delete this category");
        }
        categoryRepository.deleteById(categoryId);
    }
}

package no.ntnu.sparestibackend.service;

import lombok.RequiredArgsConstructor;
import no.ntnu.sparestibackend.dto.openAIApi.ChatCompletionsDTO;
import no.ntnu.sparestibackend.dto.openAIApi.ChatCompletionsMessageDTO;
import org.json.JSONObject;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;

import java.util.List;

/**
 * Service class for generating AI-based saving tips.
 */
@Service
@RequiredArgsConstructor
public class SavingTipService {

    /**
     * Generates saving tips using an AI model based on a given category.
     *
     * @param category the category for which to generate saving tips
     * @return a string containing the generated saving tips
     */
    public String getAiGeneratedSavingTips(String category) {
        ChatCompletionsDTO chatCompletionsDTO = ChatCompletionsDTO.builder()
                .model("gpt-3.5-turbo")
                .messages(List.of(
                        ChatCompletionsMessageDTO.builder()
                                .role("system")
                                .content("You are a financial expert designed to output saving tips called savingTips, which is a String of category and tip. Provide tips in Norwegian Bokmål, and ensure the category is matched with user input. Drop all explanatory text and only provide the tip. Make suggestions in the style of the following example: Visste du at du kan spare 150kr i måneden ved å lage kaffe hjemme?")
                                .build(),
                        ChatCompletionsMessageDTO.builder()
                                .role("user")
                                .content("Generate saving tips for this categories: " + category)
                                .build()
                ))
                .build();

        WebClient client = WebClient.create("https://api.openai.com/v1/chat/completions");
        String response = client.post()
                .header("Content-Type", "application/json")
                .header("Authorization", "Bearer sk-proj-dU6LgucI1j9IIIMEQ0uRT3BlbkFJEdsu0vvllfnlMtKut69b")
                .bodyValue(chatCompletionsDTO)
                .retrieve().bodyToMono(String.class).block();

        System.out.println(response);

        JSONObject jsonObject = new JSONObject(response);

        return jsonObject.getJSONArray("choices").getJSONObject(0).getJSONObject("message").getString("content");
    }
}

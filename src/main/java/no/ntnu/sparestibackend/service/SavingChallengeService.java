package no.ntnu.sparestibackend.service;

import jakarta.persistence.EntityManager;
import lombok.RequiredArgsConstructor;
import no.ntnu.sparestibackend.dto.SavingChallengeDTO;
import no.ntnu.sparestibackend.dto.openAIApi.ChatCompletionsDTO;
import no.ntnu.sparestibackend.dto.openAIApi.ChatCompletionsFormatDTO;
import no.ntnu.sparestibackend.dto.openAIApi.ChatCompletionsMessageDTO;
import no.ntnu.sparestibackend.dto.savingchallenges.SavingChallengeSuggestionDTO;
import no.ntnu.sparestibackend.mapper.SavingChallengeMapper;
import no.ntnu.sparestibackend.model.*;
import no.ntnu.sparestibackend.repository.SavingChallengeRepository;
import no.ntnu.sparestibackend.repository.SavingGoalRepository;
import no.ntnu.sparestibackend.utils.DifficultyGenerator;
import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;
import org.springframework.web.server.ResponseStatusException;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Service class for managing saving challenges.
 * Provides CRUD operations on {@link SavingChallenge} entities.
 */
@Service
@RequiredArgsConstructor
public class SavingChallengeService {
    private final Logger logger = LoggerFactory.getLogger(SavingChallengeService.class);

    private final EntityManager entityManager;
    private final SavingChallengeRepository savingChallengeRepository;
    private final SavingGoalRepository savingGoalRepository;
    private final UserPreferencesService userPreferencesService;
    private final TransactionService transactionService;
    private final BankAccountService bankAccountService;
    private final SavingChallengeMapper savingChallengeMapper;


    /**
     * Saves a new saving challenge for a user.
     *
     * @param userId             the ID of the user to save the challenge for
     * @param savingChallengeDTO the saving challenge data transfer object
     * @return the saved saving challenge data transfer object
     */
    public SavingChallengeDTO saveSavingChallenge(Long userId, SavingChallengeDTO savingChallengeDTO) {
        logger.info("Attempting to save a new saving challenge for user ID: {}", userId);
        User user = entityManager.find(User.class, userId);
        Category category = entityManager.find(Category.class, savingChallengeDTO.categoryId());
        if (user == null) {
            logger.error("No user found with ID: {}", userId);
            throw new RuntimeException("User not found");
        }
        SavingChallenge savingChallenge = SavingChallenge.builder()
                .description(savingChallengeDTO.description())
                .currentSpending(savingChallengeDTO.currentSpending())
                .targetSpending(savingChallengeDTO.targetSpending())
                .startDate(savingChallengeDTO.startDate())
                .endDate(savingChallengeDTO.endDate())
                .status(savingChallengeDTO.status())
                .difficulty(savingChallengeDTO.difficulty())
                .category(category)
                .user(user)
                .build();

        if (savingChallengeDTO.savingGoalId() != null) {
            SavingGoal savingGoal = entityManager.find(SavingGoal.class, savingChallengeDTO.savingGoalId());
            savingChallenge.setSavingGoal(savingGoal);
        }
        SavingChallenge savedChallenge = savingChallengeRepository.save(savingChallenge);
        logger.info("Saving challenge successfully saved with ID: {}", savedChallenge.getSavingChallengeId());
        return savingChallengeMapper.toSavingChallengeDTO(savedChallenge);
    }

    /**
     * Retrieves all saving challenges for a user.
     *
     * @param userId the ID of the user
     * @return a list of saving challenges for the user
     */
    public List<SavingChallengeDTO> getAllSavingChallengesForUser(Long userId) {
        logger.info("Retrieving all saving challenges for user ID: {}", userId);
        List<SavingChallenge> savingChallenges = savingChallengeRepository.findByUserUserId(userId);
        List<SavingChallengeDTO> savingChallengeDTOS = new ArrayList<>();
        for (SavingChallenge savingChallenge : savingChallenges) {
            List<Transaction> transactions = transactionService.getTransactionsByUserIdCategoryIdAndDateInterval(savingChallenge.getCategory().getCategoryId(), savingChallenge.getStartDate(), savingChallenge.getEndDate());
            double spentSoFar = Math.abs(transactions.stream().mapToDouble(Transaction::getSum).sum());
            savingChallengeDTOS.add(savingChallengeMapper.toSavingChallengeDTOWithSpentSoFar(savingChallenge, spentSoFar));
        }
        return savingChallengeDTOS;
    }


    /**
     * Generates a list of saving challenge suggestions for a user based on their preferences.
     *
     * @param userId              the ID of the user to generate suggestions for
     * @param numberOfSuggestions the number of suggestions to generate
     * @return a list of saving challenge suggestions
     */
    public List<SavingChallengeSuggestionDTO> generateSavingChallenges(Long userId, int numberOfSuggestions) {
        logger.info("Generating saving challenge suggestions for user ID: {}", userId);
        BankAccount checkingAccount = this.bankAccountService.getCheckingAccountByUserId(userId);
        if (checkingAccount == null) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "You do not have a checking account");
        }

        UserPreferences userPreferences = this.userPreferencesService.getUserPreferencesByUserId(userId);
        if (userPreferences == null) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "You do not have any user preferences");
        }
        if (userPreferences.getSavingCategories().isEmpty()) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "You do not have any categories you want to save on. Please add some categories to your user preferences");
        }

        List<Category> categoriesToSuggestOn = new ArrayList<>();
        while (categoriesToSuggestOn.size() < numberOfSuggestions) {
            Category category = userPreferences.getSavingCategories().stream().toList().get((int) (Math.random() * userPreferences.getSavingCategories().size()));
            if (!categoriesToSuggestOn.contains(category)) {
                categoriesToSuggestOn.add(category);
            }
        }

        DifficultyGenerator difficultyGenerator = new DifficultyGenerator(userPreferences, numberOfSuggestions);
        List<SavingChallengeSuggestionDTO> savingChallengeSuggestionDTOS = new ArrayList<>();
        for (Category category : categoriesToSuggestOn) {
            List<Transaction> transactionsForCategory = transactionService.getTransactionsByBankAccountNumberAndCategoriesForLast28Days(checkingAccount.getAccountNumber(), category.getCategoryId());
            if (transactionsForCategory.isEmpty()) {
                continue;
            }
            double totalSpending = Math.abs(transactionsForCategory.stream().mapToDouble(Transaction::getSum).sum());
            double averageSpending = totalSpending / transactionsForCategory.size() / 4;

            SavingChallengeSuggestionDTO savingChallengeSuggestionDTO = SavingChallengeSuggestionDTO.builder()
                    .category(category)
                    .currentSpending((int) (averageSpending * 100) / 100.0)
                    .difficulty(difficultyGenerator.getNextDifficulty())
                    .build();
            savingChallengeSuggestionDTOS.add(savingChallengeSuggestionDTO);
        }

        HashMap<String, String> categoryDescriptions = getAIGeneratedDescription(savingChallengeSuggestionDTOS);
        List<SavingChallengeSuggestionDTO> newSavingChallengeSuggestionDTOS = new ArrayList<>();
        for (SavingChallengeSuggestionDTO suggestion : savingChallengeSuggestionDTOS) {
            newSavingChallengeSuggestionDTOS.add(SavingChallengeSuggestionDTO.builder()
                    .category(suggestion.category())
                    .currentSpending(suggestion.currentSpending())
                    .difficulty(suggestion.difficulty())
                    .description(categoryDescriptions.get(suggestion.category().getName()))
                    .build());
        }
        logger.info("Generated {} suggestions for user ID: {}", savingChallengeSuggestionDTOS.size(), userId);
        return newSavingChallengeSuggestionDTOS;
    }

    /**
     * Generates AI descriptions for the provided saving challenge suggestions.
     *
     * @param savingChallengeSuggestionDTOS the saving challenge suggestions to describe
     * @return a map of category names to descriptions
     */
    private HashMap<String, String> getAIGeneratedDescription(List<SavingChallengeSuggestionDTO> savingChallengeSuggestionDTOS) {
        ChatCompletionsDTO chatCompletionsDTO = ChatCompletionsDTO.builder()
                .model("gpt-3.5-turbo")
                .responseFormat(ChatCompletionsFormatDTO.builder()
                        .type("json_object")
                        .build())
                .messages(List.of(
                        ChatCompletionsMessageDTO.builder()
                                .role("system")
                                .content("You are a financial expert designed to output descriptions as a JSON object called savingChallenges that is an array of category and description. Give the descriptions in norwegian bokmål and category should be the same as the user input. An example of a description is 'Vi ser du har brukt 100 kr på mat i gjennomsnitt hver uke den siste måneden. Prøv å lage mat hjemme i stedet for å spise ute for å spare penger.'")
                                .build(),
                        ChatCompletionsMessageDTO.builder()
                                .role("user")
                                .content("Create 3 different saving challenge descriptions for these savingChallenges: " + savingChallengeSuggestionDTOS.stream().map(savingChallengeSuggestionDTO -> String.format("{ category: %s, currentAverageWeeklySpending: %.2f }", savingChallengeSuggestionDTO.category(), savingChallengeSuggestionDTO.currentSpending())).toList())
                                .build()
                ))
                .build();
        WebClient client = WebClient.create("https://api.openai.com/v1/chat/completions");
        String response = client.post()
                .header("Content-Type", "application/json")
                .header("Authorization", "Bearer sk-proj-dU6LgucI1j9IIIMEQ0uRT3BlbkFJEdsu0vvllfnlMtKut69b")
                .bodyValue(chatCompletionsDTO)
                .retrieve().bodyToMono(String.class).block();

        JSONObject jsonObject = new JSONObject(response);
        String savingChallenges = jsonObject.getJSONArray("choices").getJSONObject(0).getJSONObject("message").getString("content");
        JSONObject savingChallengesObject = new JSONObject(savingChallenges);
        JSONArray savingChallengesArray = savingChallengesObject.getJSONArray("savingChallenges");

        HashMap<String, String> categoryDescriptions = new HashMap<>();
        for (int i = 0; i < savingChallengesArray.length(); i++) {
            JSONObject savingChallenge = savingChallengesArray.getJSONObject(i);
            String category = savingChallenge.getString("category");
            String description = savingChallenge.getString("description");
            categoryDescriptions.put(category, description);
        }

        return categoryDescriptions;
    }


    /**
     * Retrieves a {@link SavingChallenge} entity by its ID.
     *
     * @param id the ID of the saving challenge
     * @return the saving challenge found
     * @throws RuntimeException if no saving challenge is found with the provided ID
     */
    public SavingChallengeDTO getSavingChallengeById(Long id) {
        logger.info("Attempting to retrieve saving challenge with ID: {}", id);
        SavingChallenge savingChallenge = savingChallengeRepository.findById(id).orElseThrow(() -> {
            logger.error("No saving challenge found with ID: {}", id);
            throw new RuntimeException("Challenge not found");
        });
        List<Transaction> transactions = transactionService.getTransactionsByUserIdCategoryIdAndDateInterval(savingChallenge.getCategory().getCategoryId(), savingChallenge.getStartDate(), savingChallenge.getEndDate());
        double spentSoFar = Math.abs(transactions.stream().mapToDouble(Transaction::getSum).sum());
        return SavingChallengeDTO.builder()
                .savingChallengeId(savingChallenge.getSavingChallengeId())
                .description(savingChallenge.getDescription())
                .currentSpending(savingChallenge.getCurrentSpending())
                .targetSpending(savingChallenge.getTargetSpending())
                .startDate(savingChallenge.getStartDate())
                .endDate(savingChallenge.getEndDate())
                .status(savingChallenge.getStatus())
                .difficulty(savingChallenge.getDifficulty())
                .category(savingChallenge.getCategory())
                .savingGoalId(savingChallenge.getSavingGoal() != null ? savingChallenge.getSavingGoal()
                        .getSavingGoalId() : null)
                .spentSoFar(spentSoFar)
                .build();
    }

    /**
     * Updates a {@link SavingChallenge} based on provided data transfer object.
     *
     * @param id                 the ID of the saving challenge to update
     * @param savingChallengeDTO the DTO containing changes
     * @return the updated saving challenge
     * @throws RuntimeException if no saving challenge is found with the provided ID
     */
    public SavingChallengeDTO updateSavingChallenge(Long id, SavingChallengeDTO savingChallengeDTO) {
        logger.info("Updating saving challenge with ID: {}", id);
        SavingChallenge existingChallenge = savingChallengeRepository.findById(id)
                .orElseThrow(() -> new RuntimeException("Challenge not found"));

        if (savingChallengeDTO.description() != null) {
            existingChallenge.setDescription(savingChallengeDTO.description());
        }
        if (savingChallengeDTO.currentSpending() != null) {
            existingChallenge.setCurrentSpending(savingChallengeDTO.currentSpending());
        }
        if (savingChallengeDTO.targetSpending() != null) {
            existingChallenge.setTargetSpending(savingChallengeDTO.targetSpending());
        }
        if (savingChallengeDTO.startDate() != null) {
            existingChallenge.setStartDate(savingChallengeDTO.startDate());
        }
        if (savingChallengeDTO.endDate() != null) {
            existingChallenge.setEndDate(savingChallengeDTO.endDate());
        }
        if (savingChallengeDTO.status() != null) {
            existingChallenge.setStatus(savingChallengeDTO.status());
        }
        if (savingChallengeDTO.difficulty() != null) {
            existingChallenge.setDifficulty(savingChallengeDTO.difficulty());
        }
        if (savingChallengeDTO.savingGoalId() != null) {
            SavingGoal savingGoal = entityManager.find(SavingGoal.class, savingChallengeDTO.savingGoalId());
            existingChallenge.setSavingGoal(savingGoal);
        }
        logger.info("Saving challenge with ID: {} updated successfully", id);
        return savingChallengeMapper.toSavingChallengeDTO(savingChallengeRepository.save(existingChallenge));
    }

    /**
     * Deletes a {@link SavingChallenge} from the database.
     *
     * @param id the ID of the saving challenge to delete
     */
    public void deleteSavingChallenge(Long id) {
        logger.info("Deleting saving challenge with ID: {}", id);
        savingChallengeRepository.deleteById(id);
        logger.info("Saving challenge with ID: {} deleted successfully", id);
    }


    /**
     * Links a saving challenge to a saving goal by updating the saving challenge's associated saving goal.
     *
     * @param challengeId The ID of the saving challenge to update.
     * @param goalId      The ID of the saving goal to link to the challenge.
     * @return The updated SavingChallenge with the linked SavingGoal.
     * @throws RuntimeException if either the challenge or the goal is not found.
     */
    public SavingChallenge linkChallengeToGoal(Long challengeId, Long goalId) {
        logger.info("Linking saving challenge ID: {} to saving goal ID: {}", challengeId, goalId);
        SavingChallenge challenge = savingChallengeRepository.findById(challengeId)
                .orElseThrow(() -> new RuntimeException("Challenge not found"));
        SavingGoal goal = savingGoalRepository.findById(goalId)
                .orElseThrow(() -> new RuntimeException("Goal not found"));
        challenge.setSavingGoal(goal);
        SavingChallenge updatedChallenge = savingChallengeRepository.save(challenge);
        logger.info("Saving challenge ID: {} linked to goal ID: {}", challengeId, goalId);
        return updatedChallenge;
    }

    /**
     * Calculates the length of the longest success rate of challenges for a user.
     *
     * @param userId The users unique id.
     * @return The length of the longest success rate.
     */
    public int calculateLongestSuccessStreak(Long userId) {
        List<SavingChallenge> challenges = savingChallengeRepository.findByUserUserIdOrderByEndDateAsc(userId);
        int longestStreak = 0;
        int currentStreak = 0;

        for (SavingChallenge challenge : challenges) {
            if (challenge.getStatus() == Status.COMPLETED) {
                currentStreak++;
                longestStreak = Math.max(longestStreak, currentStreak);
            } else if (challenge.getStatus() == Status.FAILED) {
                currentStreak = 0;
            }
        }

        return longestStreak;
    }

    /**
     * Counts the amount of challenges based on difficulty and status.
     *
     * @param userId     The user's unique id.
     * @param difficulty The difficulty of the challenge to be counted.
     * @param status     Status of the challenge.
     * @return Amount of challenges that meet the requirements.
     */
    public int getCompletedChallengesCount(Long userId, Difficulty difficulty, Status status) {
        List<SavingChallenge> challenges = savingChallengeRepository.findSavingChallengesByDifficultyAndUserUserIdAndStatus(difficulty, userId, status);
        return challenges.size();
    }
}

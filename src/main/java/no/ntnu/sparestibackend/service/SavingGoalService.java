package no.ntnu.sparestibackend.service;

import lombok.RequiredArgsConstructor;
import no.ntnu.sparestibackend.controller.TransactionController;
import no.ntnu.sparestibackend.dto.SavingGoalDTO;
import no.ntnu.sparestibackend.model.SavingGoal;
import no.ntnu.sparestibackend.model.Status;
import no.ntnu.sparestibackend.model.Transaction;
import no.ntnu.sparestibackend.model.User;
import no.ntnu.sparestibackend.repository.SavingGoalRepository;
import no.ntnu.sparestibackend.repository.TransactionRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

/**
 * Provides business logic for managing saving goals.
 * This service handles interactions with the {@link SavingGoalRepository} to perform
 * CRUD operations on saving goals and integrates with {@link UserService} to manage user
 * information related to saving goals.
 */
@Service
@RequiredArgsConstructor
public class SavingGoalService {
    private final Logger logger = LoggerFactory.getLogger(TransactionController.class);

    private final SavingGoalRepository savingGoalRepository;
    private final UserService userService;
    private final TransactionRepository transactionRepository;

    /**
     * Creates a new saving goal using the provided user ID and goal details from the DTO.
     * It associates the saving goal with a user and persists it to the database.
     *
     * @param userId        The ID of the user to whom the saving goal will be associated.
     * @param savingGoalDTO The data transfer object containing details about the saving goal
     *                      such as title and deadline.
     * @return The newly created and saved {@link SavingGoal}, including any automatically
     * generated fields like the unique identifier.
     */
    public SavingGoal createSavingGoal(long userId, SavingGoalDTO savingGoalDTO) {
        logger.info("Creating saving goal for user ID: {}", userId);
        User user = userService.getUserByUserId(userId);
        if (user == null) {
            logger.error("User not found with ID: {}", userId);
            return null;
        }
        Status status = savingGoalDTO.status() != null ? savingGoalDTO.status() : Status.ACTIVE;

        SavingGoal savingGoal = SavingGoal.builder()
                .user(user)
                .title(savingGoalDTO.title())
                .goal(savingGoalDTO.goal())
                .deadline(savingGoalDTO.deadline())
                .status(status)
                .build();
        SavingGoal savedGoal = savingGoalRepository.save(savingGoal);
        logger.info("Saving goal created with ID: {}", savedGoal.getSavingGoalId());
        return savedGoal;
    }

    /**
     * Calculates the total amount saved towards a specific saving goal.
     *
     * @param savingGoalId The ID of the saving goal to calculate savings for.
     * @return The total amount saved so far for the specified saving goal.
     */
    public double getTotalSavedForSavingGoal(long savingGoalId) {
        logger.info("Calculating total saved for saving goal ID: {}", savingGoalId);
        List<Transaction> transactions = transactionRepository.findBySavingGoal_SavingGoalId(savingGoalId);
        double total = transactions.stream().mapToDouble(Transaction::getSum).sum();
        logger.info("Total saved for saving goal ID {} is {}", savingGoalId, total);
        return total;
    }

    /**
     * Updates an existing saving goal using the provided user ID, goal ID, and goal details from the DTO.
     * The update is performed only if the goal exists and belongs to the user specified by the user ID.
     *
     * @param userId        The ID of the user attempting to update the saving goal.
     * @param savingGoalId  The ID of the saving goal to be updated.
     * @param savingGoalDTO The data transfer object containing the new details of the saving goal.
     * @return The updated {@link SavingGoal} object if the update is successful; otherwise, returns null.
     */
    public SavingGoal updateSavingGoal(long userId, Long savingGoalId, SavingGoalDTO savingGoalDTO) {
        logger.info("Updating saving goal ID: {} for user ID: {}", savingGoalId, userId);
        Optional<SavingGoal> existingGoal = savingGoalRepository.findById(savingGoalId);
        if (existingGoal.isPresent() && existingGoal.get().getUser().getUserId() == userId) {
            SavingGoal savingGoal = existingGoal.get();

            if (savingGoalDTO.deadline() != null) {
                savingGoal.setTitle(savingGoalDTO.title());
            }
            savingGoal.setGoal(savingGoalDTO.goal());
            if (savingGoalDTO.deadline() != null) {
                savingGoal.setDeadline(savingGoalDTO.deadline());
            }
            if (savingGoalDTO.status() != null) {
                savingGoal.setStatus(savingGoalDTO.status());
            }
            SavingGoal updatedGoal = savingGoalRepository.save(savingGoal);
            logger.info("Saving goal ID: {} updated successfully", savingGoalId);
            return updatedGoal;
        }
        logger.error("Failed to update or goal not found/owned by user ID: {}", userId);
        return null;
    }

    /**
     * Deletes a saving goal identified by the savingGoalId, but only if the goal exists and belongs to the
     * user identified by userId.
     *
     * @param userId       The ID of the user who owns the saving goal.
     * @param savingGoalId The ID of the saving goal to delete.
     * @return true if the saving goal is successfully deleted; false if the goal does not exist or does not belong to the user.
     */
    public boolean deleteSavingGoal(long userId, long savingGoalId) {
        logger.info("Attempting to delete saving goal ID: {} for user ID: {}", savingGoalId, userId);
        Optional<SavingGoal> existingGoal = savingGoalRepository.findById(savingGoalId);
        if (existingGoal.isPresent() && existingGoal.get().getUser().getUserId() == userId) {
            savingGoalRepository.deleteById(savingGoalId);
            logger.info("Saving goal ID: {} deleted successfully", savingGoalId);
            return true;
        }
        logger.error("Failed to delete or goal not found/owned by user ID: {}", userId);
        return false;
    }

    /**
     * Retrieves all saving goals for a specific user based on the user's ID. This method
     * ensures that only the saving goals belonging to the provided user ID are fetched.
     *
     * @param userId The ID of the user whose saving goals are to be retrieved.
     * @return A list of {@link SavingGoal} objects representing all saving goals associated with the user.
     */
    public List<SavingGoal> getAllSavingGoalsForUser(long userId) {
        logger.info("Retrieving all saving goals for user ID: {}", userId);
        return savingGoalRepository.findSavingGoalsByUserUserId(userId);
    }

    /**
     * Retrieves all saving goals for a specific user based on the user's ID. This method
     * ensures that only the saving goals belonging to the provided user ID are fetched.
     *
     * @param userId The ID of the user whose saving goals are to be retrieved.
     * @return A list of {@link SavingGoal} objects representing all saving goals associated with the user.
     */
    public List<SavingGoal> getAllSavingGoalsForUser(long userId, Status status) {
        return savingGoalRepository.findSavingGoalsByUserUserIdAndStatus(userId, status);
    }

    /**
     * Calculates the total amount saved across all saving goals for a specified user.
     * This method retrieves all saving goals associated with the user and sums up all the
     * transactions linked to these goals to determine the total saved amount.
     *
     * @param userId The ID of the user whose total savings across all goals is to be calculated.
     * @return The sum of all amounts saved in transactions linked to the user's saving goals.
     */
    public double getTotalSavedForAllSavingGoals(long userId) {
        List<SavingGoal> savingGoals = getAllSavingGoalsForUser(userId);
        double total = savingGoals.stream()
                .mapToDouble(goal -> getTotalSavedForSavingGoal(goal.getSavingGoalId()))
                .sum();
        logger.info("Total saved across all saving goals for user ID: {} is {}", userId, total);
        return total;
    }
}

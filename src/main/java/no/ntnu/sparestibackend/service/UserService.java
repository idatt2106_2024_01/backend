package no.ntnu.sparestibackend.service;

import lombok.RequiredArgsConstructor;
import no.ntnu.sparestibackend.dto.UserBadgeDTO;
import no.ntnu.sparestibackend.dto.UserInfoDTO;
import no.ntnu.sparestibackend.mapper.UserMapper;
import no.ntnu.sparestibackend.model.User;
import no.ntnu.sparestibackend.model.UserBadge;
import no.ntnu.sparestibackend.repository.UserBadgeRepository;
import no.ntnu.sparestibackend.repository.UserRepository;
import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * Provides business logic related to user operations.
 */
@Service
@RequiredArgsConstructor
public class UserService {
    private final UserRepository userRepository;
    private final UserMapper userMapper;
    private final UserBadgeRepository userBadgeRepository;

    /**
     * Retrieves a user by their unique identifier.
     *
     * @param userId The ID of the user to retrieve.
     * @return The User object if found; otherwise, null.
     */
    public User getUserByUserId(Long userId) {
        return userRepository.findByUserId(userId);
    }

    /**
     * Converts a User entity to a UserInfoDTO.
     *
     * @param userId The ID of the user whose information is to be converted to DTO.
     * @return A UserInfoDTO containing the user's data.
     */
    public UserInfoDTO getUserInfoDTOByUserId(Long userId) {
        User user = userRepository.findByUserId(userId);
        return userMapper.userToUserInfoDTO(user);
    }

    /**
     * Updates a user's information based on the provided UserInfoDTO.
     * This method first retrieves the existing User from the database, then maps the DTO
     * to the User entity, and finally saves the updated entity.
     *
     * @param userId      The ID of the user to update.
     * @param userInfoDTO The DTO containing updated user information.
     * @return The updated User object, or null if no user was found for the given ID.
     */
    public User updateUser(Long userId, UserInfoDTO userInfoDTO) {
        User user = userRepository.findByUserId(userId);
        if (user != null) {
            if (userInfoDTO.firstName() != null) {
                user.setFirstName(userInfoDTO.firstName());
            }
            if (userInfoDTO.lastName() != null) {
                user.setLastName(userInfoDTO.lastName());
            }
            if (userInfoDTO.email() != null && !userInfoDTO.email().isEmpty()) {
                user.setEmail(userInfoDTO.email());
            }
            user.setProfileAvatar(userInfoDTO.profileAvatar());
            user.setNewsletterConsent(userInfoDTO.newsletterConsent());
            user.setOnboarded(userInfoDTO.isOnboarded());
            return userRepository.save(user);
        }
        return null;
    }

    /**
     * Retrieves a list of all badges for a specific user, indicating which have been achieved.
     *
     * @param userId The unique identifier of the user whose badges are to be retrieved.
     * @return A list of {@link UserBadgeDTO} representing all badges for the user, including both achieved and not achieved badges.
     */
    public List<UserBadgeDTO> getAllBadges(Long userId) {
        List<UserBadge> userBadges = Optional.ofNullable(
                userBadgeRepository.findByUser_UserId(userId)
        ).orElse(Collections.emptyList());

        return userBadges.stream()
                .map(ub -> new UserBadgeDTO(
                        ub.getBadge().getBadgeId(),
                        ub.getBadge().getName(),
                        ub.getBadge().getDescription(),
                        ub.isAchieved(),
                        ub.getDateAchieved()))
                .collect(Collectors.toList());
    }

    /**
     * Retrieves a list of all badges achieved by a specific user.
     * This method filters the badges to return only those that have been achieved.
     *
     * @param userId The unique identifier of the user whose achieved badges are to be retrieved.
     * @return A list of {@link UserBadgeDTO} representing each badge the user has achieved.
     */
    public List<UserBadgeDTO> getAchievedBadges(Long userId) {
        return getAllBadges(userId).stream()
                .filter(UserBadgeDTO::isAchieved)
                .collect(Collectors.toList());
    }

    /**
     * Saves or updates a user in the database.
     *
     * @param user The {@link User} entity to be saved or updated.
     */
    public void saveUser(User user) {
        userRepository.save(user);
    }

    /**
     * Updates the newsletter consent for a user.
     *
     * @param userId       the ID of the user to update
     * @param emailConsent whether the user consents to receive newsletters
     */
    public void updateNewsletterConsent(Long userId, boolean emailConsent) {
        User user = getUserByUserId(userId);
        if (user == null) {
            throw new RuntimeException("User not found");
        }
        user.setNewsletterConsent(emailConsent);
        saveUser(user);
    }

    /**
     * Updates the profile avatar for a specific user.
     *
     * @param userId        The unique identifier of the user whose profile avatar is to be updated.
     * @param profileAvatar The URL or identifier of the new profile avatar.
     */
    public void updateProfileAvatar(Long userId, String profileAvatar) {
        User user = getUserByUserId(userId);
        if (user == null) {
            throw new RuntimeException("User not found");
        }
        user.setProfileAvatar(profileAvatar);
        saveUser(user);
    }
}

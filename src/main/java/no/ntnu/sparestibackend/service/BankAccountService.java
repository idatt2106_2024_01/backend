package no.ntnu.sparestibackend.service;

import jakarta.persistence.EntityManager;
import lombok.RequiredArgsConstructor;
import no.ntnu.sparestibackend.dto.BankAccountDTO;
import no.ntnu.sparestibackend.model.AccountType;
import no.ntnu.sparestibackend.model.BankAccount;
import no.ntnu.sparestibackend.model.User;
import no.ntnu.sparestibackend.repository.BankAccountRepository;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

/**
 * Service handling bank account operations including creation, retrieval, and updates.
 */
@RequiredArgsConstructor
@Service
public class BankAccountService {
    private final EntityManager entityManager;
    private final BankAccountRepository bankAccountRepository;
    private final TransactionService transactionService;

    /**
     * Adds a new bank account to the database.
     *
     * @param userId         the ID of the user creating the account
     * @param bankAccountDTO the details of the new bank account
     * @return the created bank account data transfer object
     * @throws ResponseStatusException if the bank account already exists or user is not found
     */
    public BankAccountDTO integrateWithBankAccount(Long userId, BankAccountDTO bankAccountDTO) {
        BankAccount existingBankAccount = bankAccountRepository.findByAccountNumber(bankAccountDTO.accountNumber());
        if (existingBankAccount != null) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, "Bank account already exists");
        }
        User user = this.entityManager.find(User.class, userId);
        if (user == null) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "User not found");
        }
        BankAccount bankAccount = bankAccountRepository.save(BankAccount.builder()
                .accountNumber(bankAccountDTO.accountNumber())
                .name(bankAccountDTO.name())
                .accountType(bankAccountDTO.accountType())
                .user(user)
                .build());

        if (bankAccount.getAccountType() == AccountType.CHECKING) {
            transactionService.populateBankAccountWithTransactions(bankAccount);
        }

        return BankAccountDTO.builder()
                .accountNumber(bankAccount.getAccountNumber())
                .name(bankAccount.getName())
                .accountType(bankAccount.getAccountType())
                .build();
    }

    /**
     * Retrieves a bank account by its account number.
     *
     * @param accountNumber the unique account number
     * @return the bank account or null if not found
     */
    public BankAccount getBankAccountByAccountNumber(Long accountNumber) {
        return bankAccountRepository.findByAccountNumber(accountNumber);
    }

    /**
     * Retrieves a checking account for a specific user by their ID.
     *
     * @param userId the ID of the user
     * @return the user's checking account or null if not found
     */
    public BankAccount getCheckingAccountByUserId(Long userId) {
        return bankAccountRepository.findByUserUserIdAndAccountType(userId, AccountType.CHECKING);
    }

    /**
     * Updates the details of an existing bank account.
     *
     * @param accountNumber  the account number of the bank account to update
     * @param bankAccountDTO the new details to update the account with
     * @return the updated bank account data transfer object
     * @throws ResponseStatusException if the bank account is not found
     */
    public BankAccountDTO updateBankAccount(Long accountNumber, BankAccountDTO bankAccountDTO) {
        BankAccount existingBankAccount = getBankAccountByAccountNumber(accountNumber);
        if (existingBankAccount == null) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Bank account not found");
        }

        existingBankAccount.setName(bankAccountDTO.name());
        if (bankAccountDTO.accountType() != null) {
            existingBankAccount.setAccountType(bankAccountDTO.accountType());
        }
        bankAccountRepository.save(existingBankAccount);
        return BankAccountDTO.builder()
                .accountNumber(existingBankAccount.getAccountNumber())
                .name(existingBankAccount.getName())
                .accountType(existingBankAccount.getAccountType())
                .build();
    }
}
